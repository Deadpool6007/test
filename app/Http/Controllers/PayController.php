<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use Omnipay\Omnipay;
//use App\Payment;
use App\Models\Payment;
use App\Models\User;

class PayController extends Controller
{
  
    public $gateway;
  
    public function __construct()
    {
        $this->gateway = Omnipay::create('PayPal_Rest');
        $this->gateway->setClientId(env('PAYPAL_CLIENT_ID'));
        $this->gateway->setSecret(env('PAYPAL_CLIENT_SECRET'));
        $this->gateway->setTestMode(true); //set it to 'false' when go live
    }
  
    public function index()
    {
        return view('pay');
    }
  
    public function charge(Request $request)
    {
        $response = $this->gateway->purchase(array(
            'amount' => $request->input('amount'),
            'items' => array(
                array(
                    'name' => 'Course Subscription',
                    'price' => $request->input('amount'),
                    'description' => 'Get access to premium courses.',
                    'quantity' => 1
                ),
            ),
            'currency' => env('PAYPAL_CURRENCY'),
            'returnUrl' => url('paymentsucc'),
            'cancelUrl' => url('paymenterror'),
        ))->send();
        
        if($request->input('submit'))
        {
            try {
                $response = $this->gateway->purchase(array(
                    'amount' => $request->input('amount'),
                    'currency' => env('PAYPAL_CURRENCY'),
                    'returnUrl' => url('paymentsucc'),
                    'cancelUrl' => url('paymenterror'),
                ))->send();
           
                if ($response->isRedirect()) {
                    $response->redirect(); // this will automatically forward the customer
                } else {
                    // not successful
                    return $response->getMessage();
                }
            } catch(Exception $e) {
                return $e->getMessage();
            }
        }
    }
  
    public function payment_success(Request $request)
    {
        // Once the transaction has been approved, we need to complete it.
        if ($request->input('paymentId') && $request->input('PayerID'))
        {
            $transaction = $this->gateway->completePurchase(array(
                'payer_id'             => $request->input('PayerID'),
                'transactionReference' => $request->input('paymentId'),
            ));
            $response = $transaction->send();
          
            if ($response->isSuccessful())
            {
                // The customer has successfully paid.
                $arr_body = $response->getData();
          
                // Insert transaction data into the database
                $isPaymentExist = Payment::where('payment_id', $arr_body['id'])->first();
          
                if(!$isPaymentExist)
                {
                    $payment = new Payment;
                    $payment->payment_id = $arr_body['id'];
                    $payment->uid = '2';
                    $payment->payer_id = $arr_body['payer']['payer_info']['payer_id'];
                    $payment->payer_email = $arr_body['payer']['payer_info']['email'];
                    $payment->amount = $arr_body['transactions'][0]['amount']['total'];
                    $payment->currency = env('PAYPAL_CURRENCY');
                  
                    $payment->payment_status = $arr_body['state'];
                    $payment->save();

                    if($payment)
                    {
                    $id='2';
                    $data=User::find($id);
                    $data->validity= "30";
                    $data->subscribedate=date('Y-m-d'); 
                    $data->save();
                    }
                }
          
                return "Payment is successful. Your transaction id is: ". $arr_body['id'];
            } else {
                return $response->getMessage();
            }
        } else {
            return 'Transaction is declined';
        }
    }
  
    public function payment_error()
    {
        return 'User is canceled the payment.';
    }
  
}