<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\User;
use App\models\Payment;
use App\models\category;
use App\models\chat;
use App\models\expert;
use App\models\expertchat;
use DB;
use Auth;
use Session;
class UserController extends Controller
{
  public function login()
  {
      return view('user.login');
  }

  public function userdashboard()
  {
    if(Auth::guard('user')->check())
      {   
          $id=Auth::guard('user')->user()->id;
          $user=User::find($id);
          return view('user.userdashboard',compact('user'));
  
       //  return $user; 
      } else{
    
          return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
        // return "in wrong crendentails";
      }
  }

  public function register(Request $request)
  {

      $data=DB::table('users')->where('email','=',$request['email'])->count();

   // return $data;

     if($data>0)
     {

      $user_data = array(
          'email' => $request->get('email'),
          'password' => $request->get('password')
      );
      if(Auth::guard('user')->attempt($user_data))
      {   
         
           return redirect('userdashboard');
  
       //  return $user; 
      } else{
    
          return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
        // return "in wrong crendentails";
      }
     }else
     {
        // return "in register form ";
          $User=new User;
               $User->email=request('email');
           $User->username=request('username');
           $User->password=bcrypt(request('password'));
             $User->save();
   if($User){
       //return redirect('login');
       return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
   }
   else{
       return "failed";
   }
 }
  }

public function resetpassword(request $request)
{

  return view('user.reset-password');

}
public function resetpass(request $request)
{

  
  $data=DB::table('users')->where('email','=',$request['email'])->count();

   if($data>0)
     {
      
      $user=DB::table('users')->where('email','=',$request['email'])->update([

          'password'=>bcrypt(request('password')),
      ]);
  //     $User=new User;
  //     $User->email=request('email');
  //   $User->password=bcrypt(request('password'));
  //   $User->save();
     return redirect('login');
     }
       //  return $user; 
       else{
    
          return redirect('resetpassword');
        // return "in wrong crendentails";
      }

     
    }
    public function askquestion()
    {
      if(Auth::guard('user')->check())
    {   
      $id=Auth::guard('user')->user()->id;
      $user=User::find($id);
          $category=DB::table('categories')->get();
         return view('user.categories',compact('category','user'));

     //  return $user; 
    } else{
  
        return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
      // return "in wrong crendentails";
    }
    }

    public function postquestion(Request $request)
    {
   
      if(Auth::guard('user')->check())
      {   
        
        $id=Auth::guard('user')->user()->id;
        $user=User::find($id);

        $post=new chat;
        $post->category_name=request('category_name');
        $post->question=request('question');
        $post->user_id=$id;
    
      $post->save();

      if($post)
      {
        return redirect('userdashboard');

      }else{
        return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
      }
       //  return $user; 
      } else{
    
          return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
        // return "in wrong crendentails";
      }
    }

public function chathistory()
{
  if(Auth::guard('user')->check())
  {   
      $id=Auth::guard('user')->user()->id;
     
      $user=User::find($id);
      $chat=chat::join('experts','experts.id','=','chats.expert_id')->select('experts.*','chats.*')->where('chats.user_id','=',$id)->get();
     //  return  $chat;
       return view('user.Usertable',compact('chat'));

   //  return $user; 
  } else{

      return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
    // return "in wrong crendentails";
  }
}

public function chartExpert($id)
{
   
  if(Auth::guard('user')->check())
  {   
      
      $uid=Auth::guard('user')->user()->id;
        
     
      
      $chats=chat::find($id);
       $expertchat=expertchat::join('chats','chats.id','=','expertchats.chatid')->where('expertchats.chatid','=',$id)->get();

     
       return view('user.chartExpert',compact('expertchat','chats'));
  } else{

      return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
    // return "in wrong crendentails";
  }
}
public function postque($id,request $request)
{
  
  

  if(Auth::guard('user')->check())
  {
     
     
      $uid=Auth::guard('user')->user()->id;
      $post=new expertchat;
     
      $post->quest=$request->question; 
      $post->uid=$uid; 
      $post->chatid=$id; 
      $post->save();
      return redirect()->back();
  } else{

      return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
    // return "in wrong crendentails";
  }
}

public function logout() {
  if (Auth::guard('user')->check()) {
      Session::flush();
      Auth::logout();
      return redirect('login');
  } else {
      return redirect('userdashboard');
      }
  }
  public function update(Request $request)
  {
    
      if(Auth::guard('user')->check()){
          $aid = Auth::guard('user')->user()->id;
        //  $User=expert::find($aid);
         $user=DB::table('users')->where('id','=',$aid)->update(
           [
            'address'=>request('address'),
           'city'=>request('city'),
           'state'=>request('state'),
          'country'=>request('country'),
            'zipcode'=>request('zipcode'),
            'phone'=>request('phone'),
           ]
           );
      //  $User->firstname=request('firstname');
      // $User->lastname=request('lastname');
      // $User->address=request('address');
      // $User->city=request('city');
      // $User->state=request('state');
      // $User->country=request('country');
      // $User->zipcode=request('zipcode');
      // $User->phone=request('phone');
     
      // $User->password=bcrypt(request('password'));
      // $User->save();
     
         
          return redirect('userdashboard');
   
  }
}
//    public function register(Request $request)
//    {
//      $id='2';
//  //$data=DB::table('payments')->select('subscribedate')->where('user_id','=',$id)->get();

//    $data=User::find($id);

//    $count= $data->pid;
//    $result=payment::find($count);
//    $date=$result->subscribedate;
//     // $to=date('Y-m-d');
    
//      $to=\Carbon\Carbon::createFromFormat('Y-m-d','2021-8-20');
    
//      $ans= $to->diffInDays($date);
//       $comp='7';
//      //return $ans;
//      if($ans>$comp)
//      {
//         return view('pay');
//      }else
//      {
//          return "your subcription is still valid";
//      }

//     //  return response()->json($count); 
//       // return $data=$payment->payer_email;
//     //    if($data= $user->email==$payment->payer_email)
//     //    {
//     //    return $data;
//     //    }
//     //    else{


//     //     return "logic failed";
//     //    }

//     }

   

// //    public function login(Request $request)
// //    {
// //     $user_data = array(
// //         'email' => $request->get('email'),
// //         'password' => $request->get('password')
// //     );
// //     if(Auth::guard('user')->attempt($user_data))
// //     {   
// //         $id=Auth::guard('user')->user()->id;
// //         $user=User::find($id);
// //        return view('dashboard',compact('user'));

// //        // return $user; 
// //     }
// //     else{
// //         return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
// //     }

// //    }
   
// //    public function update(Request $request)
// //    {
// //     if(Auth::guard('user')->check()){
// //         $aid = Auth::guard('user')->user()->id;
// //         $User=User::find($aid);

// //      $User->firstname=request('firstname');
// //     $User->lastname=request('lastname');
// //     $User->address=request('address');
// //     $User->city=request('city');
// //     $User->state=request('state');
// //     $User->country=request('country');
// //     $User->zipcode=request('zipcode');
// //     $User->phone=request('phone');
   
  
// //     $User->save();
// //     if($User){
// //         $aid = Auth::guard('user')->user()->id;
// //         $user=User::find($aid);
// //         return view('home',compact('user'));

// //     }
// //     else{
// //         return "failed";
// //     }

// //    }
// // }
// // public function login1()
// // {
// //     if(Auth::guard('user')->check())
// //     {
// //     return redirect('home');
// //     }
// //     else{
// //         return view('login');
// //     }
// // }
  
}
