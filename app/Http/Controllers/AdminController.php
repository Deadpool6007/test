<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\User;
use App\models\Payment;
use App\models\expert;
use App\models\expertchat;
use App\models\chat;
use App\models\admin;
use App\models\category;

use DB;
use Auth;
use Session;

class AdminController extends Controller
{
    public function login()
    {
    return view('admin.adminlogin');
    }
    public function adminlogin(Request $request)
    {
        
        $user_data = array(
            'email' => $request->get('email'),
            'password' => $request->get('password')
        );
        if(Auth::guard('admin')->attempt($user_data))
        {   
            $id=Auth::guard('admin')->user()->id;
           
            
             return redirect('admindash');
    
         //  return $user; 
        } else{
      
            return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
          // return "in wrong crendentails";
        }
    }

    public function dashboard()
    {
        if(Auth::guard('admin')->check())
        {   
            $id=Auth::guard('admin')->user()->id;
            $user=admin::find($id);
          //  return$user;
           return view('admin.Admindashboard',compact('user'));
            // return $user;
         
        } else{
      
            return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
          // return "in wrong crendentails";
        }
    }
public function viewuser()
{
    
    if(Auth::guard('admin')->check())
    {   
        $id=Auth::guard('admin')->user()->id;
        $admin=admin::find($id);
      //  return$user;
      $user=User::all();
       return view('admin.Viewuser',compact('user','admin'));
        // return $user;
     
    } else{
  
        return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
      // return "in wrong crendentails";
    }

   
}
public function expertview()
{
    if(Auth::guard('admin')->check())
    {   
        $id=Auth::guard('admin')->user()->id;
        $admin=admin::find($id);
      //  return$user;
      $expert=expert::all();
    return view('admin.Expertview',compact('expert','admin'));
        // return $user;
     
    } else{
  
        return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
      // return "in wrong crendentails";
    }
}
public function viewprofile($id)
{
    if(Auth::guard('admin')->check())
    {   

        
        // $id=Auth::guard('admin')->user()->id;
        $expert=expert::find($id);
      // return$user;
 
     return view('admin.Addprofile',compact('expert'));
        // return $user;
     
    } else{
  
        return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
      // return "in wrong crendentails";
    }
}
public function assignexpert()
{
    if(Auth::guard('admin')->check())
    {   
      $id=Auth::guard('admin')->user()->id;
      $admin=admin::find($id);
       $chat=chat::join('users','users.id','=','chats.user_id')->select('users.*','chats.*')->where('chats.expert_id','=','0')->get();
     //  return $chat;
       $expert=expert::all();
   // return $expert;
     return view('admin.Assignexperet',compact('chat','expert','admin'));
        // return $user;
     
    } else{
  
        return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
      // return "in wrong crendentails";
    }
}

public function paymentreport()
{
    if(Auth::guard('admin')->check())
    {   
      $id=Auth::guard('admin')->user()->id;
      $admin=admin::find($id);

        $payment=payment::join('users','users.id','=','payments.uid')->select('users.*','payments.*')->get();

     return view('admin.Paymentreport',compact('payment','admin'));
        // return $user;
     
    } else{
  
        return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
      // return "in wrong crendentails";
    }

}
public function Addcategory()
{
    if(Auth::guard('admin')->check())
    {   
      $id=Auth::guard('admin')->user()->id;
      $admin=admin::find($id);
     return view('admin.Addcatagory',compact('admin'));
        // return $user;
     
    } else{
  
        return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
      // return "in wrong crendentails";
    }
    
}
public function addcat( Request $request )
{
    if(Auth::guard('admin')->check())
    {   

        $category=new category;
      $category->category_name=$request->category_name;
      $category->save();
     
        return redirect()->back();
        // return $user;
     
    } else{
  
        return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
      // return "in wrong crendentails";
    }
    
}
public function userdelete($id)
{
  if(Auth::guard('admin')->check())
    {   

        $user=user::find($id)->delete();
     
        return redirect()->back();
        // return $user;
     
    } else{
  
        return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
      // return "in wrong crendentails";
    }

}
public function deleteexpert($id)
{
  if(Auth::guard('admin')->check())
    {   

        $expert=expert::find($id)->delete();
     
        return redirect()->back();
        // return $user;
     
    } else{
  
        return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
      
    }

}
public function logout() {
  if (Auth::guard('admin')->check()) {
      Session::flush();
      Auth::logout();
      return redirect('adminlogin');
  } else {
      return redirect('admindash');
      }
  }
}
