<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\User;
use App\models\Payment;
use App\models\expert;
use App\models\expertchat;
use App\models\chat;
use DB;
use Auth;
use Session;
class ExpertController extends Controller
{
    public function expertlogin()
    {
        return view('expert.expertlogin');
    }
    public function login(Request  $request)
    {
        $user_data = array(
            'email' => $request->get('email'),
            'password' => $request->get('password')
        );
        if(Auth::guard('expert')->attempt($user_data))
        {   
            $id=Auth::guard('expert')->user()->id;
           
             return redirect('expertdash');
    
         //  return $user; 
        } else{
      
            return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
          // return "in wrong crendentails";
        }
    }

    public function expertdash()
    {
        if(Auth::guard('expert')->check())
        {   
            $id=Auth::guard('expert')->user()->id;
            $user=expert::find($id);
           return view('expert.ExpertDashboard',compact('user'));
            // return $user;
         
        } else{
      
            return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
          // return "in wrong crendentails";
        }
    }
    public function register(Request $request)
    {
        //return $request;
        $User=new expert;
    
        $User->firstname=request('firstname');
       $User->lastname=request('lastname');
     
       $User->city=request('city');
       $User->state=request('state');
       $User->country=request('country');
       $User->zipcode=request('zipcode');
       $User->phone=request('phone');
       $User->email=request('email');
       $User->password=bcrypt(request('password'));
       $User->save();
       if($User){
        return redirect('expertlogin');
       }else{
        return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
       }

    }
    public function expertupdate(Request $request)
    {
        if(Auth::guard('expert')->check()){
            $aid = Auth::guard('expert')->user()->id;
            $User=expert::find($aid);
    
         $User->firstname=request('firstname');
        $User->lastname=request('lastname');
        $User->address=request('address');
        $User->city=request('city');
        $User->state=request('state');
        $User->country=request('country');
        $User->zipcode=request('zipcode');
        $User->phone=request('phone');
       
        $User->password=bcrypt(request('password'));
        $User->save();
        if($User){
           
            return redirect('expertdash');
        }
        else{
            return "failed";
        }
    }

    }
    public function logout() {
        if (Auth::guard('expert')->check()) {
            Session::flush();
            Auth::logout();
            return redirect('expertlogin');
        } else {
            return redirect('expertdash');
            }
        }
        public function expertreg()
        {
            return view('expert.expertreg');
        }
        public function assignuser()
        {
            if(Auth::guard('expert')->check())
            {   
                $id=Auth::guard('expert')->user()->id;
             $chat=chat::
            join('experts','chats.expert_id','=','experts.id')
            ->join('users','users.id','=','chats.user_id')
            ->select('users.*','chats.*')
             ->where('chats.expert_id','=',$id)->get();
             // return $chat;
              return view('expert.Usertable',compact('chat'));
           
             
            } else{
          
                return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
              // return "in wrong crendentails";
            }
        }

 public function chartexpert(){
           
  if(Auth::guard('expert')->check())
  {   
      $id=Auth::guard('expert')->user()->id;
     
      $expert=expert::find($id);
      $chat=chat::join('users','users.id','=','chats.user_id')->select('users.*','chats.*')->where('chats.expert_id','=',$id)->get();
      //  return  $chat;
       return view('expert.Usertable',compact('chat'));

//    //  return $user; 
   } else{

     return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
    // return "in wrong crendentails";
   }
    }
    public function chathistory($id)
{
   
  if(Auth::guard('expert')->check())
  {   
      $uid=$id;
      
      $eid=Auth::guard('expert')->user()->id;
    
      $exper=expert::find($eid);
     
      $chats=chat::find($id);
      

       $expertchat=expertchat::join('chats','chats.id','=','expertchats.chatid')->where('expertchats.chatid','=',$uid)->get();

   //    return $expertchat;
     
       return view('expert.chartExp',compact('expertchat','chats'));
  } else{

      return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
    // return "in wrong crendentails";
  }
}

public function postans($id,Request $request )
{
 

    if(Auth::guard('expert')->check())
  {
     
     
      $eid=Auth::guard('expert')->user()->id;
    
      $post=new expertchat;
     
      $post->answer=$request->answer; 
      $post->eid=$eid; 
      $post->chatid=$id; 
      $post->save();
      return redirect()->back();
  } else{

      return redirect()->back()->with('flash_message_error', 'Wrong Login Details');
    // return "in wrong crendentails";
  }
}

}
