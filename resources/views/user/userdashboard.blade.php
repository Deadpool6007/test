<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keyword" content="asist, answer, solutions">
    <meta name="description" content="we asist you on your each feet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="css/userdash.css">
       <style>
        body{
          font-size:15px!important;
          font-weight:500;
        }
       </style>
</head>



<body>
   
    <!-- navbar -->
     <nav>
        <div class="u-name">Logged in as {{$user->email}}
         <a href="userlogout">&nbsp;Log out</a>
         <span>
           <a href="askque">Formular una pregunta</a> &nbsp;
           <a href="chathistory">Mis preguntas</a> &nbsp;
           <a href="userdashboard">Mi cuenta</a>  &nbsp;
           <a href="contactus">Contacto</a> &nbsp;

        </span>
        </div>
    </nav>
      


    <div class="container">
      <div class="user-header pt-lg-2 pt-md-2 pt-2">
          <img src="images/asistley.png" class="img-fluid d-inline asist-logo">
           <strong>Legal</strong>
           <p class="yellow">Pregunte sobre Derecho y obtenga respuestas</p>
       </div>
    </div>     <!--contianer-->      
   

    <div class="container">
      <h1 class="heading">Mi cuenta</h1>
       <div class="user-dash">
       <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-12 col-12">
          <div class="box">
                <b>Buscar en archivos de respuestas</b>
                <input type="text">
                <select>
                 <option>Cualquier categoría</option>    
                 <option>Cualquier categoría</option>    
                 <option>Cualquier categoría</option>    
                 <option>Cualquier categoría</option>    
                 <option>Cualquier categoría</option>    
                 <option>Cualquier categoría</option>    
                 <option>Cualquier categoría</option>    
               </select>
            <a href="#"><button class="btn">Buscar</button></a>
          </div>   <!--box-->

          <!-- 2nd box -->

          <div class="box2">
            <b>Datos de la cuenta</b>
            <ul>
            <li><a href="userdashboard"><b>Perfil de la cuenta</b></a></li>
            <li><a href="chathistory"><b>historial de gráficos</a></li>
            <li><a href="expertreg"><b>Convertirse en un experto</b></a></li>
            </ul>
          </div>
          </div>   <!--col-lg-3-->

          
           
          <div class="col-lg-9 col-md-9 col-sm-9 col-12">
            <h4 class="mt-0 mb-lg-3 mb-md-3 mb-2 blue">Perfil de la cuenta</h4>
            <p style="color:#1b92ab; font-size:20px";>Información de la cuenta</p>
          <form method="post" action="userupdate">
            @csrf

    <div class="form-group row">
     <label for="staticEmail" class="col-lg-4 col-md-4 col-4 col-form-label">Nombre de usuario</label>
      <div class="col-lg-8 col-md-8 col-12">
       <p>{{$user->username}}</p>
     </div>
    </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-lg-4 col-md-4 col-4 col-form-label"><b>Dirección de correo electrónico</b></label>
    <div class="col-lg-8 col-md-8 col-8">
    <p>{{$user->email}} <a href="#" class="d-inline">Cambiar e-mail</a>
</p>
    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-lg-4 col-md-4 col-4 col-form-label"><b>Contraseña</b></label>
    <div class="col-lg-8 col-md-8 col-12">
    <p> <a href="" class="d-inline">	Editar contraseñae</a>
</p>
    </div>
  </div>

  <h4 class="blue">Configuración de la cuenta</h4>
  <div class="form-group row">
    <label for="inputPassword" class="col-lg-4 col-md-4 col-4 col-form-label"><b>Mantener iniciada la sesión</b></label>
    <div class="col-lg-8 col-md-8 col-12">
    <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
        <label class="custom-control-label" for="customRadioInline1">Si</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
        <label class="custom-control-label" for="customRadioInline2">No</label>
      </div>
    <!-- </p> -->
    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-lg-4 col-md-4 col-4 col-form-label">Zona horaria</label>
    <div class="col-lg-8 col-md-8 col-8">
      <div class="col-lg-8 col-md-8 col-8">
          <select class="browser-default custom-select"  class="form-control">
               <option selected>-- Derecho Civil</option>
               <option value="State one">-- Derecho Civil</option>
               <option value="State two">-- Derecho Civil</option>
               <option value="State three">-- Derecho Civil</option>
            </select>
        </div>
    </div>
  </div>

  <h4 class="blue">Información de contacto</h4>
  <div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-4">
       Dirección 1:
    </div>     <!--col-l4-->
    <div class="col-lg-8 col-sm-8 col-md-8 col-8">
       <input type="text"name="address" value="{{$user->address}}">
    </div>       <!--col-lg-8-->

    <div class="col-lg-4 col-md-4 col-sm-4 col-4">
        Ciudad: 
    </div>     <!--col-l4-->
    <div class="col-lg-8 col-sm-8 col-md-8 col-8">
       <input type="text" name="city" value="{{$user->city}}">
    </div>       <!--col-lg-8-->
  </div>     <!--row-->
   
     <!--row-->

  <div class="row">
  <div class="col-lg-4 col-md-4 col-sm-4 col-4">
        Estado (O bien, país:)
    </div>     <!--col-l4-->
    <div class="col-lg-8 col-sm-8 col-md-8 col-8">
       <!-- <input type="text"> -->
       <select name="country">
          <option>{{$user->country}}</option>
          <option>Alabama</option>
          <option>Alaska</option>
          <option>Hawai</option>
       </select>
    </div>       <!--col-lg-8-->
  </div>     <!--row-->

   <!--row-->

  <div class="row">
    <div class="col-lg-4 col-md-4 col-sm-4 col-4">
       Código postal:
    </div>     <!--col-l4-->
    <div class="col-lg-8 col-sm-8 col-md-8 col-8">
       <input type="text" name="zipcode" value="{{$user->zipcode}}">
    </div>       <!--col-lg-8-->

    <div class="col-lg-4 col-md-4 col-sm-4 col-4">
        Teléfono: 
    </div>     <!--col-l4-->
    <div class="col-lg-8 col-sm-8 col-md-8 col-8">
       <input type="text" name="phone" value="{{$user->phone}}">
       <button type="submit" class="btn2">Guardar configuración</button>
    </div>       <!--col-lg-8-->
  </div>     <!--row-->
  <a href="#" class="forget">Descartar cambios</a>
         </div>    <!--col-lg-9-->
        </div>    <!--row--> 
       </div>    <!--user-dash-->
    </div>    <!--container-->

    <footer>
       <hr></hr>
       <p>
         <a href="#">Convertirse en un experto</a> | 
         <a href="#">Términos de servicio</a>  |
         <a href="#">Privacidad</a>  |
         <a href="#">Acerca de nosotros</a>  |
         <a href="#">Impressum</a>  
          <div class="color" style="color:#000">© 2003-2021 JustAnswer LLC</div>
       </p>

    </footer>

   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   
</body>
</html