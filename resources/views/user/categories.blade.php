<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keyword" content="asist, answer, solutions">
    <meta name="description" content="we asist you on your each feet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css"> 
     <link rel="stylesheet" href="css/categories.css">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
     <link rel="stylesheet" href="css/owl.carousel.min.css"> 
</head>
<body>
     <nav>
         <a href="#" class="name">Sesión iniciada como <b>{{$user->email}}</b></a>
         <div class="top-menu"><a href="userlogout">cerrar sesión</a>&nbsp;|&nbsp;
            <a href="userdashboard">Mi cuenta</a>&nbsp;|&nbsp;
            <a href="contactus">Contacto</a>
         </div>
     </nav>

   <div class="categories">
       <div class="container-fluid">
          <div class="row">
             <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="header-sec">
                 <img src="images/asistley.png" class="pt-2"><span>Legal</span>
                 <div class="Pregunte">Pregunte sobre Derecho y obtenga respuestas</div>
                 <a href="#">Preguntas a Expertos </a> 
                 <img src="images/arrow.png" class="arrow"><span class="abogados">Abogados Online</span>
               </div>
              </div>    <!--col-lg-6-->
             <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                 <h6>Cómo funciona JustAnswer:</h6>
                 <div class="box">
                    <ul> 
                        <li><img src="images/question.png" class="question"><a href="#" >Preguntar a un Experto</a></li>
                        <li><img src="images/asist.png" class="asist"><a href="#" >Preguntar a un Experto</a></li>
                        <li><img src="images/satisfied.png" class="satisfied"><a href="#" >Preguntar a un Experto</a></li>
                   </ul>
                  </div>
             </div>    <!--col-lg-6-->
          </div>
          <div class="row jusity-content-center">
             <div class="col-lg-10 col-md-10 col-12">
       <div class="text mt-lg-0 mt-md-0 mt-3">
         <h5>¿Tiene usted una pregunta sobre Leyes? Pregunte a un Abogado en línea ahora.</h5>
         <p class="text-left">Las leyes pueden ser un poco confusas cuando hay que enfrentarse a una cuestión legal. Es importante tener una persona experta en leyes a su lado que pueda ayudarle a entender los procesos y explicarle cómo funciona el sistema. Los abogados en JustAnswer.es ofrecen rápidas respuestas en línea las 24 horas del día a cualquier pregunta legal sobre el Derecho español para usted. Si está empezando un negocio, está en trámites de divorcio o cualquier situación que requiera la respuesta de un abogado profesional y serio, permita que un abogado de verdad le ayude. Obtenga respuestas online ahora.</p>
       </div>    <!--text-->
</div>
</div>
     </div>      <!--contaiern-->



  <!-- categories page -->
    <div class="subcategories">
      <select>
        <option>ver por subcategorías</option> 
        <option></option>  
        <option></option>  
        <option></option>  
        <option></option>  
        <option></option>  
        <option></option>  
        <option></option>  
        <option></option>  
      </select> 
    </div>      <!--subcate-->

     <!-- main-panel -->
     <div class="img-panel">
       <div class="container-fluid">
        <div class="bg-img position-relative">

<div class="row">
   <div class="col-lg-7 col-md-7 col-8">
      
   <p class="mb-lg-3 mb-md-3 mb-2 pb-0">Pregunte a un Abogado en línea ahora</p>
              <div class="text-effect">
              <textarea class="form-control form-control-sm"  placeholder=" Escriba su pregunta sobre Legal aquí..."></textarea>
             </div>
             <h5>5 Abogados están aceptando preguntas ahora</h5>
            <a href="#"><img src="images/continue_off.gif" class="continue"></a>
</div>     <!--col-lg-7-->

<div class="col-lg-5 col-md-5 col-4">
<div class="ribbon">
               <p>JustAnswer ofrece miles de Abogados preparados para responder a su pregunta ahora.</p>
             </div>    <!--ribbon-->
</div>      <!--col-5-->
</div>    <!--row-->              
          </div>     <!--bg-img-->
        </div>
       </div>
      </div>     <!--main-panel-->
   </div>  <!--categories-->


   <!-- owl carouesrl -->
   <div class="container">
         <div class="row">
            <!----row start here---->
            <!---slider one start herer--->
            <div class="col-lg-6 col-md-6 col-12">
               <div id="owl-demo" class="owl-carousel owl-theme">
                  <div class="item">
                     <div class=" border-gery p-4 clearfix">
                        <div class="row">
                           <div class="col-lg-9 col-md-9 col-12">
                              <p class="text-left text-dark "><span class="text-blue">Excelente. Me respondió en muy corto tiempo<br><br></span>
                               <span class="text">dbkjad e respondió en muy corto tiempo. Respuesta correcta, precisa y con todo lo que necesitaba saber. Fueron varios puntos que tenía en duda y me respondió todos.Muchas gracias!
                               <br><br>
                                <span class="name"> Elina </span>
                              </p>
                           </div>
                           <div class="col-lg-3 col-md-3 col-12">
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <h6 class="text-dark mt-5">10/10/2020</h6>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class=" border-gery p-4 clearfix">
                        <div class="row">
                           <div class="col-lg-9 col-md-9 col-12">
                           <p class="text-left text-dark "><span class="text-blue">Excelente. Me respondió en muy corto tiempo<br><br></span>
                               <span class="text">evbdfjk respondió en muy corto tiempo. Respuesta correcta, precisa y con todo lo que necesitaba saber. Fueron varios puntos que tenía en duda y me respondió todos.Muchas gracias!
                               <br><br>
                                <span class="name"> Elina Jogn</span>
                              </p>
                           </div>
                           <div class="col-lg-3 col-md-3 col-12">
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <h6 class="text-dark mt-5">20/10/2020</h6>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class=" border-gery p-4 clearfix">
                        <div class="row">
                           <div class="col-lg-9 col-md-9 col-12">
                           <p class="text-left text-dark "><span class="text-blue">Excelente. Me respondió en muy corto tiempo<br><br></span>
                               <span class="text">e respondió en muy corto tiempo. Respuesta correcta, precisa y con todo lo que necesitaba saber. Fueron varios puntos que tenía en duda y me respondió todos.Muchas gracias!
                               <br><br>
                                <span class="name"> Elina Dldfh </span>
                              </p>
                           </div>
                           <div class="col-lg-3 col-md-3 col-12">
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <!-- <h6 class="text-dark mt-5">Heading three here</h6> -->
                              <h6 class="text-dark mt-5">12/10/2020</h6>

                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="item">
                     <div class=" border-gery p-4 clearfix">
                        <div class="row">
                           <div class="col-lg-9 col-md-9 col-12">
                           <p class="text-left text-dark "><span class="text-blue">1 Excelente. Me respondió en muy corto tiempo<br><br></span>
                               <span class="text">e respondió en muy corto tiempo. Respuesta correcta, precisa y con todo lo que necesitaba saber. Fueron varios puntos que tenía en duda y me respondió todos.Muchas gracias!
                               <br><br>
                                <span class="name"> Elina Eline</span>
                              </p>
                              <!-- <p class="text-left text-dark "> 3 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p> -->
                           </div>
                           <div class="col-lg-3 col-md-3 col-12">
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <i class="fa fa-star" aria-hidden="true"></i>
                              <!-- <h6 class="text-dark mt-5">Heading four here</h6> -->
                              <h6 class="text-dark mt-5">16/10/2020</h6>

                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!---slider one End herer--->



            <!-- 2nd carousel -->
            <div class="col-lg-6 col-md-6 col-12">
               <!---slider two start herer--->
               <div id="owl-demo2" class="owl-carousel owl-theme">
                     <div class="item">
                     <div class=" border-gery p-4 clearfix">
                        <div class="row">
                           <div class="col-lg-3 col-md-3 col-12"><img src="images/img-1.jpg" class="img-fluid person-img"></p>
                           </div>      <!--col-lg-3-->
                           <div class="col-lg-9 col-md-9 col-12">
                              <p class="text-left text-dark "><b>Fernando Cantalapiedra</b><br>Abogado <br>Clientes satisfechos: 17</p>
                           </div>    <!--col-lg-9-->
                           <br><br><br>
                           <div class="text2">Experto en derecho y leyes Españolas relacionadas con cualquier rama del derecho. Formo parte del Ilustre Colegio de Aboado de A Coruña, número: 4541.</div>
                        </div>    <!--row-->                    
                     </div>       <!--boder-grey-->
                  </div>     <!--item-->
                  <div class="item">
                     <div class=" border-gery p-4 clearfix">
                      <div class="row">
                           <div class="col-lg-3 col-md-3 col-12"><img src="images/img-2.jpg" class="img-fluid person-img"></p>
                           </div>      <!--col-lg-3-->
                           <div class="col-lg-9 col-md-9 col-12">
                              <p class="text-left text-dark "><b>JoseM<br>Abogado</b><br>Clientes satisfechos: 6016</p>
                           </div>    <!--col-lg-9-->
                           <br><br><br>
                           <div class="text2">José M. Abogado, con diez años de experiencia resolviendo dudas legales en Justanswer. English spoken.</div>
                        </div>    <!--row--> 
                     </div>    <!--boder-grey-->
                  </div>         <!--item-->
                  <div class="item">
                     <div class=" border-gery p-4 clearfix">
                      <div class="row">
                           <div class="col-lg-3 col-md-3 col-12"><img src="images/img-3.jpg" class="img-fluid person-img"></p>
                           </div>      <!--col-lg-3-->
                           <div class="col-lg-9 col-md-9 col-12">
                              <p class="text-left text-dark "><b>JoseM<br>Abogado</b><br>Clientes satisfechos: 6016</p>
                           </div>    <!--col-lg-9-->
                           <br><br><br>
                           <div class="text2">José M. Abogado, con diez años de experiencia resolviendo dudas legales en Justanswer. English spoken.</div>
                        </div>    <!--row--> 
                     </div>    <!--boder-grey-->
                  </div>         <!--item-->
                  <div class="item">
                     <div class=" border-gery p-4 clearfix">
                      <div class="row">
                           <div class="col-lg-3 col-md-3 col-12"><img src="images/img-1.jpg" class="img-fluid person-img"></p>
                           </div>      <!--col-lg-3-->
                           <div class="col-lg-9 col-md-9 col-12">
                              <p class="text-left text-dark "><b>JoseM<br>Abogado</b><br>Clientes satisfechos: 6016</p>
                           </div>    <!--col-lg-9-->
                           <br><br><br>
                           <div class="text2">José M. Abogado, con diez años de experiencia resolviendo dudas legales en Justanswer. English spoken.</div>
                        </div>    <!--row--> 
                     </div>    <!--boder-grey-->
                  </div>         <!--item-->
                  <div class="item">
                     <div class=" border-gery p-4 clearfix">
                      <div class="row">
                           <div class="col-lg-3 col-md-3 col-12"><img src="images/img-4.png" class="img-fluid person-img"></p>
                           </div>      <!--col-lg-3-->
                           <div class="col-lg-9 col-md-9 col-12">
                              <p class="text-left text-dark "><b>DiegoAbogado</b><br>Licenciatura <br>Clientes satisfechos: 746</p>
                           </div>    <!--col-lg-9-->
                           <br><br><br>
                           <div class="text2">José M. Abogado, con diez años de experiencia resolviendo dudas legales en Justanswer. English spoken.</div>
                        </div>    <!--row--> 
                     </div>
                  </div>
                  <div class="item">
                     <div class=" border-gery p-4 clearfix">
                       <div class="row">
                           <div class="col-lg-3 col-md-3 col-12"><img src="images/img-5.jpg" class="img-fluid person-img"></p>
                           </div>      <!--col-lg-3-->
                           <div class="col-lg-9 col-md-9 col-12"><b>Jesús</b> <br>Licenciatura/Derecho<br>Clientes satisfechos: 52</p>
                           </div>    <!--col-lg-9-->
                           <br><br><br>
                           <div class="text2">Abogada desde el año 2002 especialista en derecho de familia, accidentes de tráfico, derecho penal, sucesiones, derecho laboral, contratos, extranjería y derecho en general.</div>
                        </div>    <!--row--> 
                     </div>
                  </div>
               </div>
            </div>
            <!---slider two end herer--->
         </div>
         <!----row end here---->
      </div>   <!--conatainer--> 
      <!-- owl end here -->

<!-- after carousel section -->

<div class="legal-section">
   <div class="container-fluid">
     <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-5 col-5">
        <div class="box">
          <p data-aos="fade-down" data-aos-duration="500">Artículos recientes sobre <a href="#">Legal</a><br><span><a href="#">Acoso policial</a></span></p>
        </div>   <!--box-->
      </div>        <!--col-lg-4-->


      
      <!-- seo question set -->
      <div class="col-lg-7 col-md-7 col-sm-7 col-7">
        <div class="table-box">
           <ul>
             <div class="text show-more-height">
            <li class="header">
              <h4 class="link">Preguntas sobre Legal</h4>
              <div class="date h-date">Fecha de  envio</div>
             </li> 
              <!-- 1st table data --> 
              <li class="table-item">
              <h4 class="link"><a href="#">Hola buenas tardes, tengo una problema en elche alicante, si</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Al novio de mi hija no le dan el visado para venir a españa,</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Buenas tardes, Mi duda es la siguiente, mis padres me</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Sobre un herencia de la casa de mi abuelo. Asistente:</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Quiero saber si puedo pedir un permiso de recidencia aqui en</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Hola, yo he tenido una custodia exclusiva de mi hijo, a lo</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Hola Buenas Tardes Estoy Cobrando La Renda Garantida Y Me</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Para inscribir una empresa S.L en el registro mercantil,</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Hola. Me llamo Carlos. Quisiera preguntar un tema</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">El notario ha avisado que ya tiene las últimas voluntades y</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Buenos días Quisiera saber cómo puedo reclamar el</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Hola mi pregunta es que me llmaron de bolsa para ocupar el</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Tengo consultas relativas a obtención de visa para trabajo</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Buenos días, me ha llegado del juzgado una demanda que pone</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Si compro un credito a un fondo buitre y el deudor ejercen</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">En una liquidación de gananciales, los vienes privativos o</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Hola buenas tardes,me e ido hoy del trabajo sin avisar, soy</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Hola llevo separado legalmente con sentencia desde el año</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Hola, Quería hacerte una pregunta en su momento firme un</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Si necesito trabajar no tengo papeles y tengo una hija</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Mi padre fue practicante en Guinea Ecuatorial en la epoca de</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Hola en julio dejé de vivir en un piso, ni la inmobiliaría</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Se trata de un cobro indebido de 3500 euros hace 19 años, en</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Buenos días,. Te cuento mi caso a ver si me puedes ayudar.</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">He recibido una denuncia por mala praxis, cuando no era</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Reclamé un importe de un seguro que iba con un préstamo d l</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Reclamé un importe de un seguro que iba con un préstamo d l</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Hola, tengo un adosado desde el año 2002 con una hipoteca a</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Voy ha solicitar la pensión de incapacidad permanente por</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Buenas noches,estoy separada legalmente hace años, que</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             <li class="table-item">
              <h4 class="link"><a href="#">Cumplí 65 años el 4 de junio. Hice tramites con la Seguridad</a></h4>
              <div class="date">10/10/2019</div>
             </li>
             </div>

       <div class="show-more">
           <div class="navigate">
              <!-- <a href="#">10 siguientes ></a> -->
              show more >
            </div>
       </div>
            </ul>
        </div>  
          
      </div>   <!--table-box-->

      <div class="social-section">
        <div class="social-btn">
        <button class="btn"><img src="images/twittear.png" class="t-icon"><span class="t-text">Twittear</span></button>
      </div>        <!--socialb-btn-->
      <div class="social-btn">
        <button class="btn"><img src="images/twittear.png" class="t-icon"><span class="t-text">Twittear</span></button>
      </div>        <!--socialb-btn-->
      <div class="social-btn">
        <button class="btn"><img src="images/facebook.png" class="t-icon"><span class="t-text">Compartir</span></button>
      </div>        <!--socialb-btn-->
      </div>    <!--social-section-->
    </div>    <!--col-lg-7-->
   </div>     <!--row-->
</div>        <!--container-->

<!--footer-section-->
<div class="f-section">
    <div class="links">
    <img src="images/norton_secured.png">

    <p>
    <a href="#">Cómo funciona JustAnswer</a>
    <a href="#">Registrarse</a> 
    <a href="#">Iniciar sesión</a> 
    <a href="#">Convertirse en un experto</a>
    <a href="#">Contacto</a>
    <a href="#">Sitemap</a>
    <a href="#">Acerca de nosotros</a><br>
    <a href="#">Nuestro grupo</a>
    <a href="#">Términos de servicio</a>
    <a href="#">Privacidad</a>
    <a href="#">Programa de Afiliados</a>
    <a href="#">Recomendaciones de expertos</a>
    <a href="#">Impressum</a><br>
    <a href="#">Categorías relacionadas</a>
    <a href="#">Cómo funciona JustAnswer</a><br>
      <span>© 2003-2021 Asistley LLC</span>
    </p>
    </div>     <!--links-->
<div>       <!--f-section-->
 


   <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
   <script src="js/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"> </script>
   <script src="js/owl.carousel.min.js"></script>
   <script>
      $(".show-more").click(function () {
        if($(".text").hasClass("show-more-height")) {
            $(this).text("(Show Less)");
        } else {
            $(this).text("(Show More)");
        }

         $(".text").toggleClass("show-more-height");
      });
   </script>

   
      <!---owl slider one code--->
      <script type="text/javascript">
         $(document).ready(function() {
          
           $("#owl-demo").owlCarousel({
             navigation : true,
             items : 1, 
             loop:true,
                nav:true,
             animateOut: 'fadeOut',
                animateIn: 'fadeIn',
           });
            $( ".owl-prev").html('Anterior');
            $( ".owl-next").html('Sigulente');
         });
      </script>


      <!---owl slider two code--->
      <script type="text/javascript">
         $(document).ready(function() {
          
           $("#owl-demo2").owlCarousel({
             navigation : true,
             items : 1, 
             loop:true,
                nav:true,
             animateOut: 'fadeOut',
                animateIn: 'fadeIn',
           });
            $( ".owl-prev").html('Anterior');
            $( ".owl-next").html('Sigulente');
          
         });
      </script>


<script>
  AOS.init();
</script>
</html>