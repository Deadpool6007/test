<!DOCTYPE html>
<html>
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->

<head>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Oswald:500" rel="stylesheet">
<script>!function(e){"undefined"==typeof module?this.charming=e:module.exports=e}(function(e,n){"use strict";n=n||{};var t=n.tagName||"span",o=null!=n.classPrefix?n.classPrefix:"char",r=1,a=function(e){for(var n=e.parentNode,a=e.nodeValue,c=a.length,l=-1;++l<c;){var d=document.createElement(t);o&&(d.className=o+r,r++),d.appendChild(document.createTextNode(a[l])),n.insertBefore(d,e)}n.removeChild(e)};return function c(e){for(var n=[].slice.call(e.childNodes),t=n.length,o=-1;++o<t;)c(n[o]);e.nodeType===Node.TEXT_NODE&&a(e)}(e),e});
</script>


    <meta charset="utf-8" />
    <link rel="preload" as="script" href="js/cookie.js" />
    <link rel="dns-prefetch" href="http://ww2.justanswer.es/" />
    <link rel="dns-prefetch" href="http://ww2-secure.justanswer.es/" />
    <link rel="dns-prefetch" href="http://www.googleadservices.com/" />
    <link rel="dns-prefetch" href="http://www.google-analytics.com/" />
    <link rel="dns-prefetch" href="http://googleads.g.doubleclick.net/" />
    <meta name="verify-v1" content="onq/Jmry4CF5JcNYaSAF/wroRZDDuc3sl/jR76/BDYI=" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Pregunte a un experto y obtenga respuestas ahora</title>
     <script type="text/javascript">
    var JA = window.JA || {};
    JA.i = window.JA.i || {};
    JA.i.siteArea = JA.i.siteArea || "LandingPages";
    JA.i.user = JA.i.user || {};
    if (typeof JA.i.user.isGuest === "undefined") {
        JA.i.user.isGuest = true;
    }
    JA.i.subscription = JA.i.subscription || {};
    if (typeof JA.i.subscription.numOfActiveSubscriptions === "undefined") {
        JA.i.subscription.numOfActiveSubscriptions = 0;
        JA.i.expertNameSingular = "";
    }
    JA.i.platform = "Node FEAPI"
    JA.i.baseDomain = "justanswer.es";
    </script>
    <script type="text/javascript">
    var mainVariable = "JA";
    var jsVariables = {
        "i": {
            "partner": {
                "ID": 9,
                "Name": "JustAnswer Spain"
            },
            "pageViewName": "Home"
        }
    };
    window[mai
    nVariable] = window[mainVariable] || new Object();
    var objectKeys = Object.keys(jsVariables);
    for (var i = 0; i < objectKeys.length; i++) {
        window[mainVariable][objectKeys[i]] = jsVariables[objectKeys[i]];
    }
    </script> 
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@justanswer" />
    <meta property="twitter:url" content="https://www.justanswer.es/" />
    <meta property="twitter:title" content="I got expert answers at JustAnswer. Try it yourself!" />
    <meta property="twitter:description"
        content="Want to talk with a licensed doctor, lawyer, vet, mechanic, or other expert? JustAnswer makes it easy. It’s faster than an in-person visit and more reliable than searching the web. Try it!" />
    <meta property="twitter:image" content="//ww2.justanswer.es/static/fe/th-page-hp/homepage-og.jpg" />

    <meta property="fb:app_id" content="1439148066368316" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.justanswer.es/" />
    <meta property="og:image" content="//ww2.justanswer.es/static/fe/th-page-hp/homepage-og.jpg" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:title" content="Habla con Expertos en minutos, 24/7" />
    <meta property="og:description"
        content="Harvard lawyers, UCLA doctors & more are on call, ready to answer your questions. Ask away!" />
    <meta property="og:site_name" content="JustAnswer" />
    <meta property="og:locale" content="es_ES" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- <meta name="copyright" content="&copy;2003-2021 JustAnswer LLC" /> -->
    <meta name="description"
        content="Haga una pregunta y obtenga una respuesta a su pregunta de un experto profesional de JustAnswer, el sitio web líder de preguntas y respuestas. ¡Pregunte a un experto ahora!" />
    <meta id="meta_title" name="title" content="Pregunte a un experto y obtenga respuestas ahora" />
    <meta id="meta_keywords" name="keywords"
        content="Pregunta a Expertos, Pregunte y obtenga respuestas, preguntas y respuestas, pregutnas online, hacer preguntas online, preguntas a expertos, JustAnswer, Just Answer, JustAnswer.es" />
    <meta id="meta_robots" name="ROBOTS" content="index, follow" />
    <link rel="canonical" href="index.html" />
    <link rel="shortcut icon" type="image/x-icon" href="https://my-secure.justanswer.com/favicon.ico" />
    <link rel="apple-touch-icon" href="../secure.justanswer.com/img/ja-boxlogo.png" />
    <link href="https://plus.google.com/+justanswer" rel="publisher" />
    <link rel="stylesheet" href="css/index.css">
</head>

<body>
    <img src="images/logo.png" width="0" height="0">
    <div id="hp-page" class="th-page-hp_es" data-state-mobile-qbox="condensed" data-state-mobile-subnav="closed">
        <div class="th-cookie-banner desktop" data-component="cookie-banner">
            <div class="cookie-banner-text">Utilizamos cookies para mejorar su experiencia en nuestro sitio web. Al
                continuar utilizando este sitio, usted acepta el uso de cookies tal y como se describe en nuestra <a
                    href='http://ww2.justanswer.es/es/cookie-notice' class='cookies-privacy-link' target='_blank'
                    rel='nofollow noopener noreferrer' data-element='cookie-notice-link'>política de cookies</a>, a no
                ser que las haya desactivado.</div>
            <div class="cookie-banner-close-btn" data-element="close-button"></div>
        </div>
        <div class="th-cookie-banner mobile" data-component="cookie-banner">
            <div class="cookie-banner-text">Al continuar utilizando este sitio, usted acepta el uso de cookies en su
                dispositivo tal y como se describe en nuestra <a href='http://ww2.justanswer.es/es/cookie-notice'
                    class='cookies-privacy-link' rel='nofollow' data-element='cookie-notice-link'>política de
                    cookies</a>, a no ser que las haya desactivado.</div>
            <div class="cookie-banner-close-btn" data-element="close-button"></div>
        </div>
        <div class="th-hp-spotlight">
            <div class="spotlight">
                <div class="spotlight-inner">
                    <div id="spotlight-photos" class="carousel">
                        <div class="spotlight-slide general" data-category-name="general">
                            <h1 class="category-title"><span class="title-text shifted">Médicos, abogados,<span
                                        class="group"> veterinarios, 24/7</span></span></h1>
                            <div class="bg-photo"
                                style="background-image: url(images/bg-1.jpg)"
                                data-style="background-image: url(images/bg-2.jpg)">
                            </div>
                        </div>
                        <div class="spotlight-slide lawyers" data-category-name="lawyers">
                            <p class="category-title"><span class="title-text shifted">Abogados de familia<span
                                        class="group"> por una tarifa plana</span></span></p>
                            <div class="bg-photo"
                                data-style="background-image: url(images/bg-3.jpg)">
                            </div>
                        </div>
                        <div class="spotlight-slide mechanics" data-category-name="mechanics">
                            <p class="category-title"><span class="title-text shifted">Mecánicos preparados<span
                                        class="group"> para sacarle de un apuro</span></span></p>
                            <div class="bg-photo"
                                data-style="background-image: url(images/bg-4.jpg)">
                            </div>
                        </div>
                        <div class="spotlight-slide doctors" data-category-name="doctors">
                            <p class="category-title"><span class="title-text shifted">Médicos disponibles a todas
                                    horas,<span class="group"> sin tener que esperar</span></span></p>
                            <div class="bg-photo"
                                data-style="background-image: url(images/bg-5.jpg)">
                            </div>
                        </div>
                        <div class="spotlight-slide vets" data-category-name="vets">
                            <p class="category-title"><span class="title-text shifted">Contacte con un veterinario,<span
                                        class="group"> incluso en mitad de la noche</span></span></p>
                            <div class="bg-photo"
                                data-style="background-image: url(images/bg-6.jpg)">
                            </div>
                        </div>
                        <div class="spotlight-slide home" data-category-name="home">
                            <p class="category-title"><span class="title-text shifted">El fontanero que le ayudará<span
                                        class="group"> a desatascar las tuberías</span></span></p>
                            <div class="bg-photo"
                                data-style="background-image: url(images/bg-7.jpg)">
                            </div>
                        </div>
                        <div class="spotlight-slide tech" data-category-name="tech">
                            <p class="category-title"><span class="title-text shifted">Su informático cuando<span
                                        class="group"> se estropea el disco duro</span></span></p>
                            <div class="bg-photo"
                                data-style="background-image: url(//ww2.justanswer.es/static/fe/th-hp-spotlight/bg-tech_optimized.jpg)">
                            </div>
                        </div>
                        <div class="spotlight-slide more" data-category-name="more">
                            <p class="category-title"><span class="title-text shifted">Su experto en antigüedades<span
                                        class="group"> dispuesto a asesorarle</span></span></p>
                            <div class="bg-photo"
                                data-style="background-image: url(//ww2.justanswer.es/static/fe/th-hp-spotlight/bg-more_optimized.jpg)">
                            </div>
                        </div>
                    </div>
                    <div class="spotlight-content">
                        <div class="utility">
                            <div class="utility-inner">
                                <div class="logo-cell">
                                    <div class="th-hp-logo" data-component="th-hp-logo">
                                        <div class="logo"><a class="logo__link" href="index.html"
                                                data-element="link"><img class="logo__image"
                                                    src="images/logo.png"
                                                    alt="JustAnswer" data-element="image"></a></div>
                                    </div>
                                </div>
                                <ul class="utility-nav" data-component="utility">
                                    <li><a href="https://secure.justanswer.es/login.aspx" target="_blank" rel="nofollow"
                                            data-element="login-link">Iniciar sesión</a></li>
                                    <li><a href="help/contact-us.html" target="_blank" rel="nofollow"
                                            data-element="contact-us-link">Contacto</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="header">
                            <div class="th-hp-qbox">
                                <div class="qbox" data-component="question-box">
                                    <form method="post" action="https://my-secure.justanswer.es/myhome/">
                                        <div class="input"><textarea id="question-input" class="input-field"
                                                name="question" maxlength="5000"
                                                data-element="question-input"></textarea><input
                                                id="question-category-key" type="hidden" name="categoryKey">
                                            <div id="question-placeholder" class="placeholder">
                                                <div id="question-placeholder-label" class="placeholder-label">
                                                    <span>Escriba su pregunta </span>
                                                    <div class="placeholder-category general active"> </div>
                                                    <div class="placeholder-category lawyers">legal</div>
                                                    <div class="placeholder-category mechanics">de coche</div>
                                                    <div class="placeholder-category doctors">de salud</div>
                                                    <div class="placeholder-category vets">sobre mascotas</div>
                                                    <div class="placeholder-category home">sobre reparaciones</div>
                                                    <div class="placeholder-category tech">de informática y electrónica
                                                    </div>
                                                    <div class="placeholder-category more"> </div><span> aquí...</span>
                                                </div><span id="qbox-pipe" class="placeholder-pipe"></span>
                                            </div>
                                            <div class="error ">Escriba su pregunta aquí para continuar...</div>
                                        </div>
                                        <div class="submit">
                                            <div class="submit-button ja-button" data-element="submit-button"><button
                                                    class="button button-orange">Continuar</button></div>
                                            <div id="online-now" class="online">
                                                <div class="online-category-container general active"><span
                                                        class="online-count">3</span>
                                                    <div class="online-category"> Expertos </div><span>están en línea
                                                        ahora</span>
                                                </div>
                                                <div class="online-category-container lawyers"><span
                                                        class="online-count">3</span>
                                                    <div class="online-category"> abogados </div><span>están en línea
                                                        ahora</span>
                                                </div>
                                                <div class="online-category-container mechanics"><span
                                                        class="online-count">3</span>
                                                    <div class="online-category"> mecánicos </div><span>están en línea
                                                        ahora</span>
                                                </div>
                                                <div class="online-category-container doctors"><span
                                                        class="online-count">3</span>
                                                    <div class="online-category"> médicos </div><span>están en línea
                                                        ahora</span>
                                                </div>
                                                <div class="online-category-container vets"><span
                                                        class="online-count">3</span>
                                                    <div class="online-category"> veterinarios </div><span>están en
                                                        línea ahora</span>
                                                </div>
                                                <div class="online-category-container home"><span
                                                        class="online-count">3</span>
                                                    <div class="online-category"> técnicos </div><span>están en línea
                                                        ahora</span>
                                                </div>
                                                <div class="online-category-container tech"><span
                                                        class="online-count">3</span>
                                                    <div class="online-category"> técnicos </div><span>están en línea
                                                        ahora</span>
                                                </div>
                                                <div class="online-category-container more"><span
                                                        class="online-count">3</span>
                                                    <div class="online-category"> Expertos </div><span>están en línea
                                                        ahora</span>
                                                </div>
                                            </div>
                                        </div>
                                    </form><i id="qbox-close" class="toggle-icon"><svg class="icon icon-arrow-down">
                                            <use xlink:href="#icon-arrow-down"></use>
                                        </svg></i>
                                </div>
                            </div>
                            <div class="extras shifted">
                                <p class="extra"><strong id="questions-answered-number"
                                        data-questions-count="16216105">15,216,105</strong> preguntas contestadas</p>
                                <p class="extra dollars"><strong>3 mil millones</strong> euros ahorrados</p>
                                <p class="extra"><strong>11,990</strong> expertos</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="nav-cell">
            <div class="th-hp-nav">
                <div id="category-nav" class="nav" data-component="category-nav"><a id="nav-lawyers"
                        class="category lawyers" href="legal-es/index.html" data-category-key="ABOGADOS001KEY"
                        data-element="navigation-lawyers">
                        <div class="menu-item"><i><svg class="icon icon-gavel">
                                    <use xlink:href="#icon-gavel"></use>
                                </svg></i>
                            <div class="label">Abogados</div>
                            <div class="sub-toggle"><span class="toggle-label">Categories</span><i
                                    class="toggle-icon"><svg class="icon icon-arrow-down">
                                        <use xlink:href="#icon-arrow-down"></use>
                                    </svg></i></div>
                        </div>
                    </a><a id="nav-mechanics" class="category mechanics" href="coche-es/index.html"
                        data-category-key="MECÁNICOS001KEY" data-element="navigation-mechanics">
                        <div class="menu-item"><i><svg class="icon icon-car">
                                    <use xlink:href="#icon-car"></use>
                                </svg></i>
                            <div class="label">Mecánicos</div>
                            <div class="sub-toggle"><span class="toggle-label">Categories</span><i
                                    class="toggle-icon"><svg class="icon icon-arrow-down">
                                        <use xlink:href="#icon-arrow-down"></use>
                                    </svg></i></div>
                        </div>
                    </a><a id="nav-doctors" class="category doctors" href="medicina-es/index.html"
                        data-category-key="MÉDICOS001KEY" data-element="navigation-doctors">
                        <div class="menu-item"><i><svg class="icon icon-stethoscope">
                                    <use xlink:href="#icon-stethoscope"></use>
                                </svg></i>
                            <div class="label">Médicos</div>
                            <div class="sub-toggle"><span class="toggle-label">Categories</span><i
                                    class="toggle-icon"><svg class="icon icon-arrow-down">
                                        <use xlink:href="#icon-arrow-down"></use>
                                    </svg></i></div>
                        </div>
                    </a><a id="nav-vets" class="category vets" href="veterinaria/index.html"
                        data-category-key="VETERINARIOS002KEY" data-element="navigation-vets">
                        <div class="menu-item"><i><svg class="icon icon-paw">
                                    <use xlink:href="#icon-paw"></use>
                                </svg></i>
                            <div class="label">Veterinarios</div>
                            <div class="sub-toggle"><span class="toggle-label">Categories</span><i
                                    class="toggle-icon"><svg class="icon icon-arrow-down">
                                        <use xlink:href="#icon-arrow-down"></use>
                                    </svg></i></div>
                        </div>
                    </a><a id="nav-home" class="category home" href="reformas-del-hogar/index.html"
                        data-category-key="TÉCNICOS001KEY" data-element="navigation-home">
                        <div class="menu-item"><i><svg class="icon icon-tools">
                                    <use xlink:href="#icon-tools"></use>
                                </svg></i>
                            <div class="label">Técnicos</div>
                            <div class="sub-toggle"><span class="toggle-label">Categories</span><i
                                    class="toggle-icon"><svg class="icon icon-arrow-down">
                                        <use xlink:href="#icon-arrow-down"></use>
                                    </svg></i></div>
                        </div>
                    </a><a id="nav-tech" class="category tech" href="informatica-es/index.html"
                        data-category-key="INFORMÁTICOS001KEY" data-element="navigation-tech">
                        <div class="menu-item"><i><svg class="icon icon-computer-notebook">
                                    <use xlink:href="#icon-computer-notebook"></use>
                                </svg></i>
                            <div class="label">Informática</div>
                            <div class="sub-toggle"><span class="toggle-label">Categories</span><i
                                    class="toggle-icon"><svg class="icon icon-arrow-down">
                                        <use xlink:href="#icon-arrow-down"></use>
                                    </svg></i></div>
                        </div>
                    </a><a id="nav-more" class="category more" href="general-es/index.html"
                        data-category-key="MÁS001KEY" data-element="navigation-more">
                        <div class="menu-item"><i><svg class="icon icon-more">
                                    <use xlink:href="#icon-more"></use>
                                </svg></i>
                            <div class="label">Más</div>
                            <div class="sub-toggle"><span class="toggle-label">Categories</span><i
                                    class="toggle-icon"><svg class="icon icon-arrow-down">
                                        <use xlink:href="#icon-arrow-down"></use>
                                    </svg></i></div>
                        </div>
                    </a></div>
                <div class="category-sub-nav">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
         </div> -->
        <div class="meet-experts-cell"> 
            <div class="th-hp-meet-experts" data-component="meet-experts">
                <h2 class="meet-experts-header section-header general">Le presentamos a los Expertos</h2>
                <p class="meet-experts-header section-header lawyers">Le presentamos a los abogados</p>
                <p class="meet-experts-header section-header mechanics">Le presentamos a los mecánicos</p>
                <p class="meet-experts-header section-header doctors">Le presentamos a los médicos y enfermeras</p>
                <p class="meet-experts-header section-header vets">Le presentamos a los veterinarios</p>
                <p class="meet-experts-header section-header home">Le presentamos a los técnicos</p>
                <p class="meet-experts-header section-header tech">Le presentamos a los Expertos en informática</p>
                <p class="meet-experts-header section-header more">
                <div></div>
                </p>
                <p class="intro general">
                <div></div>
                </p>
                <p class="intro lawyers">Especialistas en inmigración, abogados de divorcio, abogados criminalistas y
                    Expertos en todas las áreas del Derecho están a su disposición para responder cualquier pregunta,
                    por teléfono o por email en cuestión de minutos, y a un precio mucho menor de lo que cuesta una
                    consulta en persona. Los abogados pueden incluso preparar o revisar testamentos, multas y otros
                    documentos legales que necesite. </p>
                <p class="intro mechanics">Mecánicos especializados en todos los modelos y marcas de autos, barcos,
                    motocicletas y caravanas pueden ayudarle a diagnosticar problemas y asesorarle paso a paso para
                    solucionarlo, compartiendo esquemas de cableado, referencias de recambios o manuales de
                    instrucciones. Vuelva a poner su vehículo en marcha en cuestión de minutos, y por menos de lo que
                    cuesta un cambio de aceite.</p>
                <p class="intro doctors">Médicos de familia, pediatras, cardiólogos, oncólogos y muchos otros
                    especialistas están preparados para responder a su pregunta online o por teléfono en cuestión de
                    minutos, 24/7. Desde evaluar síntomas hasta revisar resultados de análisis y ofrecerle una segunda
                    opinión médica, puede acceder a profesionales de la salud de primer nivel siempre que lo necesite.
                </p>
                <p class="intro vets">Bien sea su mascota un chihuahua, un pastor alemán, un gato persa o una iguana,
                    veterinarios especializados en todo tipo de animales están a su disposición online 24/7, dispuestos
                    a darle respuestas personalizadas a su problema. Hable con un veterinario profesional por email o
                    teléfono sin el estrés de tener que meter a su animal en una jaula y buscar un método adecuado de
                    transporte.</p>
                <p class="intro home">Bien sea si está usted instalando un nuevo termostato, o necesita ayuda con un
                    interruptor, o tiene problemas con los grifos, puede contactar con uno de los electricistas y
                    fontaneros Expertos por email o teléfono 24/7. Resuelva su problema sin altos costes ni recargos,
                    incluso en fines de semana y festivos.</p>
                <p class="intro tech">Técnicos profesionales en Mac, PCs, iPhone y Android pueden ayudarle a recuperar
                    su contraseña, arreglar su red, recuperar su trabajo perdido o devolver su ordenador a la normalidad
                    tras una temida pantalla azul. Hable a cualquier hora y cualquier día con especialistas por email o
                    teléfono, que incluso pueden acceder a su dispositivo de manera remota y solucionar su problema.</p>
                <p class="intro more">
                <div></div>
                </p>
                <div class="experts">
                    <div class="cards">
                        <div class="cards-category cards-general active">
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="general-1">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-1"
                                                        data-src="images/medicine.jpg"
                                                        width="160" height="640" alt="Dr. Cabrera"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Dr. Cabrera</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Médico Adjunto</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">3,748</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Médico Adjunto</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Doctorado</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Ldo. en Medicina por la Universidad Complutense
                                                            de Madrid. Doctor en Medicina por la Universidad de Alcalá.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="general-2">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-2"
                                                        data-src="images/more.jpg"
                                                        width="160" height="640" alt="Ana M. Monsalve"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Ana M. Monsalve</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Abogada</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">2,662</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Abogada</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Posgrado</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Postgrado en derecho administrativo, estudios
                                                            en derecho internacional y extranjería.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="general-4">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-4"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/general.jpg"
                                                        width="160" height="640" alt="Mecánico Juan Ramón"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Mecánico Juan Ramón</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Jefe de Mantenimiento</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">578</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Jefe de Mantenimiento</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Ingeniero Metalúrgico</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">17 años laborando en una empresa de México.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card large-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert large white offline" data-position="general-3">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-3"
                                                    src="images/more.jpg"
                                                        width="160" height="640" alt="MV.William"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">MV.William</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Veterinario</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">317</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Veterinario</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura en Veterinaria</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Medicina de pequeños animales</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cards-category cards-lawyers ">
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="lawyers-1">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-1"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/law.jpg"
                                                        width="160" height="640" alt="Rafael Jofre"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Rafael Jofre</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Abogado de Chile</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">533</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Abogado de Chile</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Diplomatura en Derecho en Universidad de
                                                            Valparaíso</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Abogado con más de 20 años de experiencia en
                                                            materia civil, penal, laboral y familia.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="lawyers-4">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-4"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/law.jpg"
                                                        width="160" height="640" alt="Abogada María"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Abogada María</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Abogada</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">2,131</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Abogada</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Master en Derecho con despecho profesional
                                                            proprio.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="lawyers-3">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-3"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/law.jpg"
                                                        width="160" height="640" alt="Pablo Avila Camps"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Pablo Avila Camps</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Titular Avila Camps & Guerrero Kampf</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">871</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Titular Avila Camps & Guerrero Kampf</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Abogado con más de 20 años de experiencia en
                                                            materias laborales, previsionales, civiles y comerciales.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card large-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert large white offline" data-position="lawyers-2">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-2"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/law.jpg"
                                                        width="160" height="640" alt="Ana M. Monsalve"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Ana M. Monsalve</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Abogada</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">2,662</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Abogada</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Posgrado</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Postgrado en derecho administrativo, estudios
                                                            en derecho internacional y extranjería.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cards-category cards-mechanics ">
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="mechanics-3">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-3"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/mechanic.jpg"
                                                        width="160" height="640" alt="Mecánico Eduardo"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Mecánico Eduardo</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Jefe de Taller</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">32,560</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Jefe de Taller</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Bachillerato</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Mecánico con 30 años de experiencia en la
                                                            solución de problemas del automóvil</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="mechanics-4">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-4"
                                                        src="images/more.jpg"
                                                        width="160" height="640" alt="Chad Farhat"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Chad Farhat</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Técnico con certificado ASE</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">1,922</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Técnico con certificado ASE</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura en Tecnología Industrial</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Certificado ASE</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="mechanics-1">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-1"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/mechanic.jpg"
                                                        width="160" height="640" alt="Lou P."></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Lou P.</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Técnico Principal</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">3,779</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Técnico Principal</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Técnico de Mercedes</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card large-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert large white offline" data-position="mechanics-2">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-2"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/mechanic.jpg"
                                                        width="160" height="640" alt="Mecánico Juan Ramón"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Mecánico Juan Ramón</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Jefe de Mantenimiento</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">578</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Jefe de Mantenimiento</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Ingeniero Metalúrgico</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">17 años laborando en una empresa de México.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cards-category cards-doctors ">
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="doctors-3">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-3"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/medicine.jpg"
                                                        width="160" height="640" alt="Dr. Cabrera"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Dr. Cabrera</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Médico Adjunto</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">3,748</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Médico Adjunto</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Doctorado</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Ldo. en Medicina por la Universidad Complutense
                                                            de Madrid. Doctor en Medicina por la Universidad de Alcalá.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="doctors-4">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-4"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/medicine.jpg"
                                                        width="160" height="640" alt="Dr. K"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Dr. K</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Ginecólogo y obstetra certificado</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">3,446</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Ginecólogo y obstetra certificado</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Grado en medicina</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Doctor en medicina</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="doctors-1">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-1"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/medicine.jpg"
                                                        width="160" height="640" alt="Dr. David_Murillo"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Dr. David_Murillo</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Médico General</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">5,527</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Médico General</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Licencia en Medicina con experiencia en todas
                                                            las areas de medicina.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card large-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert large white offline" data-position="doctors-2">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-2"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/medicine.jpg"
                                                        width="160" height="640" alt="Dra. Débora Palma"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Dra. Débora Palma</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Cirujano General</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">2,584</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Cirujano General</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Postdoctorado</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Médico graduada con especialidad en Cirugía
                                                            General</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cards-category cards-vets ">
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="vets-2">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-2"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/vet.jpg"
                                                        width="160" height="640" alt="Dr. Deb"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Dr. Deb</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Veterinaria</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">18</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Veterinaria</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Máster en Etologia clínica aplicada</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Licenciada en veterinaria por la UAB
                                                            (Barcelona, España) especializada en animales de compañía y
                                                            en problemas de conducta</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="vets-3">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-3"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/vet.jpg"
                                                        width="160" height="640" alt="Rebecca"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Rebecca</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Veterinaria de perros</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">9,844</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Veterinaria de perros</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Doctorado en Medicina Veterinaria</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Doctorado</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="vets-4">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-4"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/vet.jpg"
                                                        width="160" height="640" alt="Dr. Scott Nimmo"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Dr. Scott Nimmo</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Veterinario</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">11,738</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Veterinario</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciado en Medicina y Cirugía Verterinaria
                                                        </p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Licenciatura</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card large-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert large white offline" data-position="vets-1">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-1"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/vet.jpg"
                                                        width="160" height="640" alt="MV.William"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">MV.William</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Veterinario</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">317</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Veterinario</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura en Veterinaria</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Medicina de pequeños animales</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cards-category cards-home ">
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="home-2">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-2"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/technicians.jpg"
                                                        width="160" height="640" alt="Rick"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Rick</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Supervisor HVAC</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">19,875</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Supervisor HVAC</p>
                                                    </div>
                                                    <div></div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Supervisor en construcción, fontanero principal
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="home-3">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-3"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/technicians.jpg"
                                                        width="160" height="640" alt="Joel Swenson"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Joel Swenson</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Propietario de Scott's Appliance Repair</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">2,218</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Propietario de Scott's Appliance Repair</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">GED</p>
                                                    </div>
                                                    <div></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="home-4">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-4"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/technicians.jpg"
                                                        width="160" height="640" alt="Kelly"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Kelly</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Técnico de electrodomésticos</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">19,489</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Técnico de electrodomésticos</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Bachillerato</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">30+ años de experiencia</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card large-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert large white offline" data-position="home-1">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-1"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/technicians.jpg"
                                                        width="160" height="640" alt="Ing. Navas"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Ing. Navas</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Soporte Técnico</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">1,212</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Soporte Técnico</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Especializado en HW, SW e Infraestructura de
                                                            Redes.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cards-category cards-tech ">
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="tech-2">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-2"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/computer.jpg"
                                                        width="160" height="640" alt="Viet"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Viet</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Técnico en computación</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">35,673</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Técnico en computación</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura en Informática</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Certificados Microsoft MCP y CompTIA A+</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="tech-3">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-3"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/computer.jpg"
                                                        width="160" height="640" alt="Lorenz Vauck"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Lorenz Vauck</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Experto en computación</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">714</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Experto en computación</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Ingenieria Eléctrica</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Certificados DOS, Linx, Mac OS, iOS, Android,
                                                            Window</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="tech-4">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-4"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/computer.jpg"
                                                        width="160" height="640" alt="IT Miro"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">IT Miro</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Informático</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">3,098</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Informático</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura en Informatico</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Microsoft Certified Professional</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card large-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert large white offline" data-position="tech-1">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-1"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/computer.jpg"
                                                        width="160" height="640" alt="Ing. Navas"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Ing. Navas</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Soporte Técnico</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">1,212</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Soporte Técnico</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Especializado en HW, SW e Infraestructura de
                                                            Redes.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cards-category cards-more ">
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="more-2">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-2"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/more.jpg"
                                                        width="160" height="640" alt="Rafael Jofre"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Rafael Jofre</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Abogado de Chile</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">533</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Abogado de Chile</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Diplomatura en Derecho en Universidad de
                                                            Valparaíso</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Abogado con más de 20 años de experiencia en
                                                            materia civil, penal, laboral y familia.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="more-3">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-3"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/more.jpg"
                                                        width="160" height="640" alt="Ing. Navas"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Ing. Navas</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Soporte Técnico</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">1,212</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Soporte Técnico</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Especializado en HW, SW e Infraestructura de
                                                            Redes.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card small-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert small white offline" data-position="more-4">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-4"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/more.jpg"
                                                        width="160" height="640" alt="MV.William"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">MV.William</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Veterinario</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">317</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Veterinario</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Licenciatura en Veterinaria</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Medicina de pequeños animales</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card large-card" data-element="expert-card">
                                <div class="th-hp-dynamic-expert">
                                    <div class="expert large white offline" data-position="more-1">
                                        <div class="expert-body">
                                            <div class="header-bg"></div>
                                            <div class="portrait">
                                                <div class="photo"><img class="expert-photo-sprite position-1"
                                                        data-src="//ww2.justanswer.es/static/fe/th-page-hp/es_sprites/more.jpg"
                                                        width="160" height="640" alt="Dr. Cabrera"></div>
                                                <div class="status">online</div>
                                            </div>
                                            <div class="content">
                                                <div class="primary">
                                                    <p class="name">Dr. Cabrera</p>
                                                    <div class="rating"><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i><i class="star"><svg>
                                                                <use xlink:href="#icon-star-filled"></use>
                                                            </svg></i></div>
                                                    <p class="title">Médico Adjunto</p>
                                                </div>
                                                <div class="secondary">
                                                    <p class="customers"><span class="number">3,748</span> clientes
                                                        satisfechos</p>
                                                    <div class="verified"><i class="verified-icon"><svg>
                                                                <use xlink:href="#icon-check-circle"></use>
                                                            </svg></i>
                                                        <p class="verified-label">Verificado</p>
                                                    </div>
                                                </div>
                                                <div class="credentials">
                                                    <div class="cred">
                                                        <p class="label">Cargo actual:</p>
                                                        <p class="value">Médico Adjunto</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Título más alto:</p>
                                                        <p class="value">Doctorado</p>
                                                    </div>
                                                    <div class="cred">
                                                        <p class="label">Especialidad:</p>
                                                        <p class="value">Ldo. en Medicina por la Universidad Complutense
                                                            de Madrid. Doctor en Medicina por la Universidad de Alcalá.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="supporting">
                        <div class="quote-cell">
                            <div class="quotes-category quotes-general active">
                                <blockquote class="quote" data-position="general-1">
                                    <div>
                                        <p>"Atender las dudas de los pacientes a través de JustAnswer, sin importar la
                                            distancia a la que se encuentren y de manera inmediata, se ha convertido en
                                            parte de mi práctica clínica cotidiana. Verdaderamente me siento parte de
                                            JustAnswer, y todo el que me conoce sabe que JustAnswer forma parte de mi
                                            vida diaria."</p>
                                        <p class="cite">– <b>Dr. Cabrera</b>, Médico Adjunto<br>Experto en JustAnswer
                                            desde 2011</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="general-2">
                                    <div>
                                        <p>"Me gusta trabajar en JustAnswer porque amo mi carrera, y puedo ejercerla en
                                            cualquier parte del mundo que me encuentre, JustAnswer ha sido un medio para
                                            ayudar a muchas personas que vienen desorientadas buscando una verdadera
                                            ayuda profesional. He conseguido mucha gente que es agradecida, es algo que
                                            me sorprende, porque siempre pensé que la mayoría de las personas
                                            desconfiaban de este medio. Trabajar en JA me hace crecer cada día más como
                                            persona, además me brinda la posibilidad de tener contacto con personas de
                                            muchos lugares. JustAnswer es una empresa estable, vengo trabajando hace
                                            varios años, sintiéndome muy orgullosa de ser parte de esta familia. No me
                                            siento presionada para hacer el trabajo, lo hago solo porque me gusta y a
                                            pesar de las dificultades que pasan en la vida, siempre estoy dispuesta para
                                            dar lo mejor de mi a mis clientes. No hay días o noches sábados o domingos
                                            que diga no atiendo mis clientes..."</p>
                                        <p class="cite">– <b>Ana M. Monsalve</b>, Abogada<br>Experto en JustAnswer desde
                                            2011</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="general-4">
                                    <div>
                                        <p>"Me gusta trabajar en JustAnswer porque me da la oportunidad de ayudar y
                                            prestar mis servicios a los clientes con calidad, honestidad y buena empatia
                                            con ellos. Reafirmar mis conocimientos y ponerlos en buena obra para el
                                            cliente, así gana el y gano yo. Sumando una sinergia de trabajo. además me
                                            ha sido de agrado servir y atender clientes hasta Africa y otros lugares del
                                            mundo, llenándome de satisfacción de que el cliente este contento y que se
                                            sienta bien servido y atendido en JustAnswer, en tiempo y respuesta."</p>
                                        <p class="cite">– <b>Mecánico Juan Ramón</b>, Jefe de Mantenimiento<br>Experto
                                            en JustAnswer desde 2012</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote active" data-position="general-3">
                                    <div>
                                        <p>"JustAnswer ha sido para mí un canal inmejorable para desarrollar mi pasión
                                            por la medicina veterinaria y al mismo tiempo tener la satisfacción de poder
                                            ayudar a las personas a cuidar y conocer a sus mascotas, sin importar la
                                            distancia y el horario que nos separan. He tenido que aprender a evaluar los
                                            pacientes de una manera diferente, tomando en cuenta las limitaciones de una
                                            consulta a distancia, pero a su vez sacándole provecho a las virtudes de
                                            esta plataforma en internet que me permite una interacción muy cómoda y
                                            práctica con mis clientes, donde estos pueden leer las respuestas y luego
                                            volver en caso de necesitar aclarar dudas o para informar sobre la evolución
                                            de los pacientes, lo que se traduce en una una mayor efectividad en la
                                            resolución de problemas, siempre aclarando que no se trata de sustituir la
                                            asistencia médica presencial, pero si de ofrecer asistencia en situaciones
                                            que requieren una orientación rápida y oportuna, cuando no se cuenta con
                                            acceso a un profesional en su localidad…"</p>
                                        <p class="cite">– <b>MV.William</b>, Veterinario<br>Experto en JustAnswer desde
                                            2011</p>
                                    </div>
                                </blockquote>
                            </div>
                            <div class="quotes-category quotes-lawyers">
                                <blockquote class="quote" data-position="lawyers-1">
                                    <div>
                                        <p>"He notado que este servicio es utilizado en su mayor parte por personas que
                                            se encuentran acongojadas por problemas que no están capacitadas para
                                            resolver por sí solas y recurren a nosotros para obtener ayuda. Al atender a
                                            estas personas, mediante nuestras respuestas proporcionamos a esas personas
                                            una respuesta técnica que aclare la situación que se trate o que los oriente
                                            respecto al camino a seguir sin que hayan debido desplazarse ni invertir
                                            mucho de su tiempo. Queda la sensación de haber ayudado a estas personas, de
                                            haber hecho su día un poco menos difícil. Es por ello que me gusta trabajar
                                            en JustAnswer."</p>
                                        <p class="cite">– <b>Rafael Jofre</b>, Abogado de Chile<br>Experto en JustAnswer
                                            desde 2009</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="lawyers-4">
                                    <div>
                                        <p>"Me gusta trabajar en JustAnswer porque ayudo a la gente con mis
                                            conocimientos. Es realmente gratificante cuando un cliente te da las gracias
                                            por el trabajo realizado."</p>
                                        <p class="cite">– <b>Abogada María</b>, Abogada<br>Experto en JustAnswer desde
                                            2010</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="lawyers-3">
                                    <div>
                                        <p>"Me gusta trabajar con Justanswer, debido a que es una manera efectiva de
                                            darle no sólo soluciones legales a la gente, en mi caso, sino ayuda humana
                                            efectiva en la mayoría de las veces. Siguiendo el compromiso adoptado el día
                                            en que me gradué como abogado intento siempre desde mis conocimientos y
                                            buena fe, darles a quienes acuden en mi ayuda aunque más no sea tranquilidad
                                            (a veces es imposible darles una solución legal más allá de la aceptación de
                                            la situación en que se encuentran) tan necesaria como es la que se puede
                                            transmitir cuando se escucha al preocupado; haciéndolo sentir que no está
                                            solo."</p>
                                        <p class="cite">– <b>Pablo Avila Camps</b>, Titular Avila Camps & Guerrero
                                            Kampf<br>Experto en JustAnswer desde 2012</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote active" data-position="lawyers-2">
                                    <div>
                                        <p>"Me gusta trabajar en JustAnswer porque amo mi carrera, y puedo ejercerla en
                                            cualquier parte del mundo que me encuentre, JustAnswer ha sido un medio para
                                            ayudar a muchas personas que vienen desorientadas buscando una verdadera
                                            ayuda profesional. He conseguido mucha gente que es agradecida, es algo que
                                            me sorprende, porque siempre pensé que la mayoría de las personas
                                            desconfiaban de este medio. Trabajar en JA me hace crecer cada día más como
                                            persona, además me brinda la posibilidad de tener contacto con personas de
                                            muchos lugares. JustAnswer es una empresa estable, vengo trabajando hace
                                            varios años, sintiéndome muy orgullosa de ser parte de esta familia. No me
                                            siento presionada para hacer el trabajo, lo hago solo porque me gusta y a
                                            pesar de las dificultades que pasan en la vida, siempre estoy dispuesta para
                                            dar lo mejor de mi a mis clientes. No hay días o noches sábados o domingos
                                            que diga no atiendo mis clientes..."</p>
                                        <p class="cite">– <b>Ana M. Monsalve</b>, Abogada<br>Experto en JustAnswer desde
                                            2011</p>
                                    </div>
                                </blockquote>
                            </div>
                            <div class="quotes-category quotes-mechanics">
                                <blockquote class="quote" data-position="mechanics-3">
                                    <div>
                                        <p>"Desde que comencé mi participación en JustAnswer allá por abril del 2011
                                            siento lo mismo que puedo sentir en este tiempo cada vez que los clientes
                                            valoran positivamente mis respuestas, y es una satisfacción muy especial. En
                                            mecánica uno depende mucho de lo que puede observar al revisar un coche
                                            utilizando no solo los sentidos sino una buena cantidad de herramientas y
                                            asimismo se requiere mucho oficio para dar un diagnóstico correcto, por lo
                                            que interpretar las preguntas planteados por los clientes y poder ayudarlos
                                            a encontrar la solución que venían a buscar con solo la comunicación textual
                                            o telefónica, eso es algo que todavía sigue siendo apasionante para mí y es
                                            el desafío constante que me mueve a seguir en JustAnswer, con el aditamento
                                            incluso de ganar un dinero extra."</p>
                                        <p class="cite">– <b>Mecánico Eduardo</b>, Jefe de Taller<br>Experto en
                                            JustAnswer desde 2011</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="mechanics-4">
                                    <div>
                                        <p>"Trabajar en JustAnswer ha sido una experiencia que cambió mi vida, llego a
                                            hacer lo que amo, proporcionando ayuda experta a las personas con sus
                                            problemas de automóviles en todo el mundo."</p>
                                        <p class="cite">– <b>Chad Farhat</b>, Técnico con certificado ASE<br>Experto en
                                            JustAnswer desde 2010</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="mechanics-1">
                                    <div>
                                        <p>"Me encanta trabajar en JustAnswer y no puedo nombrar una sola cosa que más
                                            disfruto: la flexibilidad, la satisfacción de un cliente diciendo que fui
                                            muy útil, los ingresos extra para mi familia, el trabajo en equipo con otros
                                            expertos en el sitio. Realmente es una gran plataforma y tengo suerte de ser
                                            parte de ella. Me encanta ayudar a la gente en JustAnswer por la sencilla
                                            razón de que en el mundo de automóviles hay tanta desinformación en la web
                                            que cuesta a los clientes cientos, si no miles de dólares, y yo puedo
                                            proporcionarles la información adecuada de mi experiencia real y guiarles en
                                            una dirección correcta, hasta solucionar el problema, y eso no tiene
                                            precio."</p>
                                        <p class="cite">– <b>Lou P.</b>, Técnico Principal<br>Experto en JustAnswer
                                            desde 2009</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote active" data-position="mechanics-2">
                                    <div>
                                        <p>"Me gusta trabajar en JustAnswer porque me da la oportunidad de ayudar y
                                            prestar mis servicios a los clientes con calidad, honestidad y buena empatia
                                            con ellos. Reafirmar mis conocimientos y ponerlos en buena obra para el
                                            cliente, así gana el y gano yo. Sumando una sinergia de trabajo. además me
                                            ha sido de agrado servir y atender clientes hasta Africa y otros lugares del
                                            mundo, llenándome de satisfacción de que el cliente este contento y que se
                                            sienta bien servido y atendido en JustAnswer, en tiempo y respuesta."</p>
                                        <p class="cite">– <b>Mecánico Juan Ramón</b>, Jefe de Mantenimiento<br>Experto
                                            en JustAnswer desde 2012</p>
                                    </div>
                                </blockquote>
                            </div>
                            <div class="quotes-category quotes-doctors">
                                <blockquote class="quote" data-position="doctors-3">
                                    <div>
                                        <p>"Atender las dudas de los pacientes a través de JustAnswer, sin importar la
                                            distancia a la que se encuentren y de manera inmediata, se ha convertido en
                                            parte de mi práctica clínica cotidiana. Verdaderamente me siento parte de
                                            JustAnswer, y todo el que me conoce sabe que JustAnswer forma parte de mi
                                            vida diaria."</p>
                                        <p class="cite">– <b>Dr. Cabrera</b>, Médico Adjunto<br>Experto en JustAnswer
                                            desde 2011</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="doctors-4">
                                    <div>
                                        <p>"Me encanta trabajar con las personas en JustAnswer porque puedo dedicar
                                            mucho tiempo a cada uno y llegar al fondo de la pregunta."</p>
                                        <p class="cite">– <b>Dr. K</b>, Ginecólogo y obstetra certificado<br>Experto en
                                            JustAnswer desde 2012</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="doctors-1">
                                    <div>
                                        <p>"Me encanta participar en JustAnswer ya que me permite ayudar e interactuar
                                            con personas de todas partes del mundo que en muchas de las ocasiones no
                                            pueden acudir a una consulta médica presencial, además me da la oportunidad
                                            de conocer sobre sus costumbres y sobre todo ver cómo cambian las patologías
                                            más comunes de una región a otra por lo que así me mantengo siempre fresco
                                            en los conocimientos de un gran abanico de las mismas que normalmente en mi
                                            país no son tan frecuentes dándome ese extra de motivación para seguir
                                            creciendo en mi práctica médica."</p>
                                        <p class="cite">– <b>Dr. David_Murillo</b>, Médico General<br>Experto en
                                            JustAnswer desde 2009</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote active" data-position="doctors-2">
                                    <div>
                                        <p>"Me encanta trabajar en Justanswer. Es una página que me permite ayudar a los
                                            clientes en sus problemas médicos de forma directa, segura y privada. El
                                            formato facilita la interacción constante y en tiempo real con el cliente de
                                            forma tal que se logra dar una opinión o un consejo médico oportunamente. Lo
                                            que más me ha sorprendido y fascinado siempre de Justanswer es poder crear
                                            una relación de confianza con los clientes a pesar de la distancia y la
                                            falta de contacto físico. Esto solamente es posible gracias al formato tan
                                            amigable y fluido con que Justanswer ha creado la página. En conclusión,
                                            estoy sumamanete contenta de trabajar en Justanswer porque me brinda la
                                            oportunidad de ayudar a muchas personas con sus problemas o dudas médicas de
                                            forma directa rompiendo la barrera de la distancia y creando un vínculo de
                                            confianza."</p>
                                        <p class="cite">– <b>Dra. Débora Palma</b>, Cirujano General<br>Experto en
                                            JustAnswer desde 2012</p>
                                    </div>
                                </blockquote>
                            </div>
                            <div class="quotes-category quotes-vets">
                                <blockquote class="quote" data-position="vets-2">
                                    <div>
                                        <p>"Trabajo con JustAnswer porque me gusta ayudar a que los propietarios tengan
                                            acceso a la opinión de un buen veterinario, aunque sea a deshoras o a mucha
                                            distancia. Al principio tenía mis dudas, pero jamás he tenido un problema
                                            con los pagos, y la atención al experto es impecable: rápida, atenta y
                                            eficaz."</p>
                                        <p class="cite">– <b>Dr. Deb</b>, Veterinaria<br>Experto en JustAnswer desde
                                            2011</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="vets-3">
                                    <div>
                                        <p>"Cuando las personas preocupadas por sus mascotas vienen a JustAnswer, pienso
                                            en lo mucho que mi perro Ilsa significa para mí. Entonces le doy mi mejor
                                            respuesta, diciendo a los dueños preocupados lo que haría en su situación."
                                        </p>
                                        <p class="cite">– <b>Rebecca</b>, Veterinaria de perros<br>Experto en JustAnswer
                                            desde 2008</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="vets-4">
                                    <div>
                                        <p>"Como veterinario dedicado a los animales pequeños, he pasado toda mi carrera
                                            ayudando a las personas con problemas de salud de su mascota y estoy muy
                                            contento de la oportunidad brinda JustAnswer para continuar haciéndolo
                                            online. Pero no solo eso, al investigar los problemas de la gente me pongo
                                            al corriente de los últimos avances en mi campo."</p>
                                        <p class="cite">– <b>Dr. Scott Nimmo</b>, Veterinario<br>Experto en JustAnswer
                                            desde 2008</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote active" data-position="vets-1">
                                    <div>
                                        <p>"JustAnswer ha sido para mí un canal inmejorable para desarrollar mi pasión
                                            por la medicina veterinaria y al mismo tiempo tener la satisfacción de poder
                                            ayudar a las personas a cuidar y conocer a sus mascotas, sin importar la
                                            distancia y el horario que nos separan. He tenido que aprender a evaluar los
                                            pacientes de una manera diferente, tomando en cuenta las limitaciones de una
                                            consulta a distancia, pero a su vez sacándole provecho a las virtudes de
                                            esta plataforma en internet que me permite una interacción muy cómoda y
                                            práctica con mis clientes, donde estos pueden leer las respuestas y luego
                                            volver en caso de necesitar aclarar dudas o para informar sobre la evolución
                                            de los pacientes, lo que se traduce en una una mayor efectividad en la
                                            resolución de problemas, siempre aclarando que no se trata de sustituir la
                                            asistencia médica presencial, pero si de ofrecer asistencia en situaciones
                                            que requieren una orientación rápida y oportuna, cuando no se cuenta con
                                            acceso a un profesional en su localidad…"</p>
                                        <p class="cite">– <b>MV.William</b>, Veterinario<br>Experto en JustAnswer desde
                                            2011</p>
                                    </div>
                                </blockquote>
                            </div>
                            <div class="quotes-category quotes-home">
                                <blockquote class="quote" data-position="home-2">
                                    <div>
                                        <p>"Realmente disfruto ayudar a la gente, especialmente a aquellos que estan en
                                            un apuro, como con aquel hombre que no tenía calefacción a 30 grados bajo
                                            cero y el técnico más cercano estaba a 200 millas en avión."</p>
                                        <p class="cite">– <b>Rick</b>, Supervisor HVAC<br>Experto en JustAnswer desde
                                            2005</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="home-3">
                                    <div>
                                        <p>"Como experto en la categoría de electrodomésticos, ayudo a los clientes a
                                            ahorrar dinero en servicios innecesarios y excesivamente caros, lo que para
                                            mí hace que ser un miembro de la comunidad JustAnswer sea una experiencia
                                            muy gratificante."</p>
                                        <p class="cite">– <b>Joel Swenson</b>, Propietario de Scott's Appliance
                                            Repair<br>Experto en JustAnswer desde 2015</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="home-4">
                                    <div>
                                        <p>"Disfruto de la libertad y los ingresos que ofrece trabajar en JA. Sobre todo
                                            me gusta saber que mi ayuda proporciona a los clientes una solución
                                            alternativa a una costosa reparación de electrodomésticos y les permite
                                            aprender y resolver un problema que probablemente no serían capaz de
                                            resolver por su cuenta!"</p>
                                        <p class="cite">– <b>Kelly</b>, Técnico de electrodomésticos<br>Experto en
                                            JustAnswer desde 2008</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote active" data-position="home-1">
                                    <div>
                                        <p>"Es gratificante el poder solucionar problemas y compartir el sentimiento de
                                            liberación cuando un clientes logra resolver su situación, ¿cuantos nos
                                            hemos hallado en problemas y no encontrar salida? ¿cuantas veces hemos
                                            buscado ayuda y darnos cuenta que los expertos y la ayuda profesional es muy
                                            cara?, JustAnswer me permite poder asistir a Clientes que en su
                                            desesperación llegaron a nosotros y ver como darles la mejor experiencia y
                                            solución a un costo muy bajo y al mismo tiempo hacerlos parte del proceso es
                                            algo inapreciable!"</p>
                                        <p class="cite">– <b>Ing. Navas</b>, Soporte Técnico<br>Experto en JustAnswer
                                            desde 2011</p>
                                    </div>
                                </blockquote>
                            </div>
                            <div class="quotes-category quotes-tech">
                                <blockquote class="quote" data-position="tech-2">
                                    <div>
                                        <p>"Para mí los ordenadores y la tecnología son fáciles y JustAnswer me da la
                                            oportunidad de hacerlo más fácil para los demás. Me hace muy feliz conseguir
                                            que el ordenador de alguien vuelva a funcionar."</p>
                                        <p class="cite">– <b>Viet</b>, Técnico en computación<br>Experto en JustAnswer
                                            desde 2010</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="tech-3">
                                    <div>
                                        <p>"Es muy gratificante ayudar a los clientes a solucionar sus problemas
                                            informáticos y sobrepasar sus expectativas. Aparte del desafío para mí (que
                                            me gusta mucho), me encanta ver las sonrisas de los clientes cuando
                                            terminamos la sesión que comenzó con una cara triste. JustAnswer es
                                            beneficioso para todos."</p>
                                        <p class="cite">– <b>Lorenz Vauck</b>, Experto en computación<br>Experto en
                                            JustAnswer desde 2014</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="tech-4">
                                    <div>
                                        <p>"El saber que mi conocimiento y experiencia puede ayudar a otros con sus
                                            problemas de la computadora me llena todos los días. Con JustAnswer puedo
                                            transformar la frustración en satisfacción, y estoy seguro de que mis
                                            clientes lo aprecian mucho!"</p>
                                        <p class="cite">– <b>IT Miro</b>, Informático<br>Experto en JustAnswer desde
                                            2010</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote active" data-position="tech-1">
                                    <div>
                                        <p>"Es gratificante el poder solucionar problemas y compartir el sentimiento de
                                            liberación cuando un clientes logra resolver su situación, ¿cuantos nos
                                            hemos hallado en problemas y no encontrar salida? ¿cuantas veces hemos
                                            buscado ayuda y darnos cuenta que los expertos y la ayuda profesional es muy
                                            cara?, JustAnswer me permite poder asistir a Clientes que en su
                                            desesperación llegaron a nosotros y ver como darles la mejor experiencia y
                                            solución a un costo muy bajo y al mismo tiempo hacerlos parte del proceso es
                                            algo inapreciable!"</p>
                                        <p class="cite">– <b>Ing. Navas</b>, Soporte Técnico<br>Experto en JustAnswer
                                            desde 2011</p>
                                    </div>
                                </blockquote>
                            </div>
                            <div class="quotes-category quotes-more">
                                <blockquote class="quote" data-position="more-2">
                                    <div>
                                        <p>"He notado que este servicio es utilizado en su mayor parte por personas que
                                            se encuentran acongojadas por problemas que no están capacitadas para
                                            resolver por sí solas y recurren a nosotros para obtener ayuda. Al atender a
                                            estas personas, mediante nuestras respuestas proporcionamos a esas personas
                                            una respuesta técnica que aclare la situación que se trate o que los oriente
                                            respecto al camino a seguir sin que hayan debido desplazarse ni invertir
                                            mucho de su tiempo. Queda la sensación de haber ayudado a estas personas, de
                                            haber hecho su día un poco menos difícil. Es por ello que me gusta trabajar
                                            en JustAnswer."</p>
                                        <p class="cite">– <b>Rafael Jofre</b>, Abogado de Chile<br>Experto en JustAnswer
                                            desde 2009</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="more-3">
                                    <div>
                                        <p>"Es gratificante el poder solucionar problemas y compartir el sentimiento de
                                            liberación cuando un clientes logra resolver su situación, ¿cuantos nos
                                            hemos hallado en problemas y no encontrar salida? ¿cuantas veces hemos
                                            buscado ayuda y darnos cuenta que los expertos y la ayuda profesional es muy
                                            cara?, JustAnswer me permite poder asistir a Clientes que en su
                                            desesperación llegaron a nosotros y ver como darles la mejor experiencia y
                                            solución a un costo muy bajo y al mismo tiempo hacerlos parte del proceso es
                                            algo inapreciable!"</p>
                                        <p class="cite">– <b>Ing. Navas</b>, Soporte Técnico<br>Experto en JustAnswer
                                            desde 2011</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote" data-position="more-4">
                                    <div>
                                        <p>"JustAnswer ha sido para mí un canal inmejorable para desarrollar mi pasión
                                            por la medicina veterinaria y al mismo tiempo tener la satisfacción de poder
                                            ayudar a las personas a cuidar y conocer a sus mascotas, sin importar la
                                            distancia y el horario que nos separan. He tenido que aprender a evaluar los
                                            pacientes de una manera diferente, tomando en cuenta las limitaciones de una
                                            consulta a distancia, pero a su vez sacándole provecho a las virtudes de
                                            esta plataforma en internet que me permite una interacción muy cómoda y
                                            práctica con mis clientes, donde estos pueden leer las respuestas y luego
                                            volver en caso de necesitar aclarar dudas o para informar sobre la evolución
                                            de los pacientes, lo que se traduce en una una mayor efectividad en la
                                            resolución de problemas, siempre aclarando que no se trata de sustituir la
                                            asistencia médica presencial, pero si de ofrecer asistencia en situaciones
                                            que requieren una orientación rápida y oportuna, cuando no se cuenta con
                                            acceso a un profesional en su localidad…"</p>
                                        <p class="cite">– <b>MV.William</b>, Veterinario<br>Experto en JustAnswer desde
                                            2011</p>
                                    </div>
                                </blockquote>
                                <blockquote class="quote active" data-position="more-1">
                                    <div>
                                        <p>"Atender las dudas de los pacientes a través de JustAnswer, sin importar la
                                            distancia a la que se encuentren y de manera inmediata, se ha convertido en
                                            parte de mi práctica clínica cotidiana. Verdaderamente me siento parte de
                                            JustAnswer, y todo el que me conoce sabe que JustAnswer forma parte de mi
                                            vida diaria."</p>
                                        <p class="cite">– <b>Dr. Cabrera</b>, Médico Adjunto<br>Experto en JustAnswer
                                            desde 2011</p>
                                    </div>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hiw-cell">
            <div class="th-hp-hiw" data-state-show-step="1" data-component="how-it-works">
                <p class="section-header">Cómo funciona <span class="header-group">(y por qué los suscriptores confían
                        en nosotros)</span></p>
                <div class="hiw-content">
                    <div class="copy">
                        <div class="steps"><span class="position-indicator"></span>
                            <div id="hiw-step1" class="step step1" data-element="hiw-step1">
                                <h2 class="step-title">Pregunte a cualquier hora</h2>
                                <p class="step-info">Los suscriptores pueden pedir ayuda a los más de 12.000 Expertos
                                    acreditados en la plataforma, desde médicos hasta abogados, informáticos, mecánicos
                                    y otras especialidades.</p>
                            </div>
                            <div id="hiw-step2" class="step step2" data-element="hiw-step2">
                                <h2 class="step-title">Un Experto responderá en minutos</h2>
                                <p class="step-info">En cuanto haga su pregunta, le pondremos en contacto con el Experto
                                    más indicado para su caso. Podrá escribirle, enviarle documentos y hablar con él
                                    hasta resolver todas sus dudas.</p>
                            </div>
                            <div id="hiw-step3" class="step step3" data-element="hiw-step3">
                                <p class="step-title">Ahorre tiempo y dinero</p>
                                <p class="step-info">Evite desplazamientos, largas esperas o gastos cuantiosos en
                                    consultas. Con su membresía podrá acceder a servicios profesionales sin límites y
                                    además ahorrará tiempo y dinero.</p>
                            </div>
                        </div>
                    </div>
                    <div class="visuals">
                        <div class="phone step1"></div>
                        <div class="phone step2"></div>
                        <div class="phone step3"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="th-hp-guarantee">
            <p class="section-header">Un plan lleno de soluciones <span class="header-group">
                    <div></div>
                </span></p>
            <p class="intro">¿Busca soluciones? Está en el sitio adecuado. Con su suscripción, puede preguntar a
                Expertos sin límite y pedir segundas opiniones gratis.</p>
            <div class="guarantees">
                <div class="item"><i><svg class="icon icon-chat-double">
                            <use xlink:href="#icon-chat-double"></use>
                        </svg></i>
                    <p class="label">Interactúe sin límite con el Experto</p>
                    <p class="info">Haga las preguntas que considere necesarias hasta que consiga la respuesta a su
                        problema.</p>
                </div>
                <div class="item"><i><svg class="icon icon-chat-checkmark">
                            <use xlink:href="#icon-chat-checkmark"></use>
                        </svg></i>
                    <p class="label">Segundas opiniones gratuitas</p>
                    <p class="info">Si quiere una segunda opinión después de recibir una respuesta, puede pedirla sin
                        gasto añadido.</p>
                </div>
                <div class="item"><i><svg class="icon icon-24-hours">
                            <use xlink:href="#icon-24-hours"></use>
                        </svg></i>
                    <p class="label">Respuestas en minutos - 24/7</p>
                    <p class="info">Pregunte a cualquier hora, incluso festivos. ¡Estamos siempre abiertos!</p>
                </div>
            </div>
        </div>
        <div class="th-hp-testimonials">
            <div class="testimonials">
                <div class="content">
                    <p class="section-header">Opiniones de nuestros clientes <span class="group">
                            <div></div>
                        </span></p>
                    <div id="testimonials" class="testimonial-list">
                        <div class="testimonial">
                            <div class="testimonial-content">
                                <p class="title">Una atención impecable</p>
                                <blockquote class="quote">
                                    <p>"Un servicio rápido, eficaz y altamente reconfortante saber que dispones de la
                                        ayuda que uno necesita para cualquier inquietud con profesionales dispuestos a
                                        ayudarte en todas las dudas que uno tenga,altamente aconsejable."</p>
                                </blockquote>
                            </div>
                            <div class="cite"><span class="person"><b>Sergio Liefde Luiers.</b></span>
                                <div class="rated"><span class="label">Valoración:</span>
                                    <div class="rating"><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-content">
                                <p class="title">Excelente y muy confiable</p>
                                <blockquote class="quote">
                                    <p>" Me encanta!!! Siempre que tengo alguna duda solo tengo que hacer mi pregunta y
                                        los Drs. son muy amables y rápidos en contestar. Además sus respuestas son muy
                                        acertadas. Gracias por estar a solo un click para recibir ayuda!"</p>
                                </blockquote>
                            </div>
                            <div class="cite"><span class="person"><b>Martha</b></span>
                                <div class="rated"><span class="label">Valoración:</span>
                                    <div class="rating"><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-content">
                                <p class="title">Es como tener un médico en casa</p>
                                <blockquote class="quote">
                                    <p>"Respuestas concretas, rápidas y con un vocabulario entendible para todos,
                                        dependiendo de la consulta hasta recibes material para determinados tratamientos
                                        como en el caso de mí madre, rehabilitación después de una operación de cadera.
                                        Realmente lo recomiendo, también consulté por una problema de salud de mi esposo
                                        y obtuve una respuesta certera y puntual a tal punto que ha llegado el momento
                                        de visitar a su médico y le recomendó exactamente lo que aquí me habían
                                        anticipado en tan solo unos minutos. Realmente lo recomiendo. Saludos."</p>
                                </blockquote>
                            </div>
                            <div class="cite"><span class="person"><b>Silvia</b></span>
                                <div class="rated"><span class="label">Valoración:</span>
                                    <div class="rating"><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-content">
                                <p class="title">JustAnswer siempre me ayuda!</p>
                                <blockquote class="quote">
                                    <p>"Estoy muy satisfecha con el servicio que brinda JustAnswer por medio de sus
                                        expertos. Yo lo recomiendo porque me dan buenas respuestas, recomendaciones y
                                        consejos para estar mejor."</p>
                                </blockquote>
                            </div>
                            <div class="cite"><span class="person"><b>Ambar 2000</b></span>
                                <div class="rated"><span class="label">Valoración:</span>
                                    <div class="rating"><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-content">
                                <p class="title">Veterinario muy profesional y eficiente</p>
                                <blockquote class="quote">
                                    <p>"Las recomendaciones del veterinario fueron muy profesionales y eficientes, y
                                        verdaderamente me ayudaron en un momento en que me encontraba en un auténtico
                                        dilema."</p>
                                </blockquote>
                            </div>
                            <div class="cite"><span class="person"><b>ACR</b></span>
                                <div class="rated"><span class="label">Valoración:</span>
                                    <div class="rating"><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-content">
                                <p class="title">Tengo un problema legal...</p>
                                <blockquote class="quote">
                                    <p>"Tengo un problema legal bastante sencillo de resolver, pero no habitual. Hace
                                        mucho estoy buscando un abogado que me lo soluciones, y me han dado respuestas
                                        inútiles y hasta absurdas. Finalmente, ustedes me han puesto en contacto con un
                                        profesional que está capacitado para resolverlo. Muchas gracias."</p>
                                </blockquote>
                            </div>
                            <div class="cite"><span class="person"><b>Cliente de JustAnswer</b></span>
                                <div class="rated"><span class="label">Valoración:</span>
                                    <div class="rating"><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial">
                            <div class="testimonial-content">
                                <p class="title">Excelente!!</p>
                                <blockquote class="quote">
                                    <p>"Me dieron respuesta clara y rápida a mi pregunta."</p>
                                </blockquote>
                            </div>
                            <div class="cite"><span class="person"><b>Lazaro Morales</b></span>
                                <div class="rated"><span class="label">Valoración:</span>
                                    <div class="rating"><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i><i class="star"><svg>
                                                <use xlink:href="#icon-star-filled"></use>
                                            </svg></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="th-hp-recent-questions" data-component="recent-questions">
            <div class="recent-questions">
                <h2 class="section-header">Preguntas y respuestas recientes</h2>
                <div class="questions"><span id="recent-questions-prev" class="arrow arrow-up"
                        data-element="recent-questions-prev"><i class="arrow-icon"><svg>
                                <use xlink:href="#icon-arrow-up"></use>
                            </svg></i></span>
                    <div id="recent-questions" class="list">
                        <div class="question">
                            <div class="question-inner">
                                <div class="question-side side front">
                                    <div class="content" data-element="question-side">
                                        <p class="question-text">E sido autónomo durante 20 años actualmente soy
                                            pensionista desde hace 1 año y medio tengo una incapacidad permanente total,
                                            mi pensión es de 609 euros. Hacienda me solicita ahora el pago de el cuarto
                                            trimestre de los últimos 4años porque no está de acuerdo con mis
                                            liquidaciones de esos años.me lo a solicitado un año después de estar fuera
                                            del trabajo por ser pensionista después de un año. Me puede hacer una
                                            paralela un año después de haberme jubilado<br />Asistente del experto: ¿Qué
                                            tipo de pensión cobras?<br />Cliente: Incapacidad permanente total 609 euros
                                            al mes<br />Asistente del experto: ¿Hay algúna otra cosa que quieras que
                                            sepa el abogado?<br />Cliente: Si los documentos muchos de ellos ya no los
                                            tengo</p><span class="question-age">hace 2 horas</span>
                                    </div>
                                    <div class="actions"><button class="button recent-questions-button"
                                            data-element="preview-answer-btn">Ver respuesta</button>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-outline"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                                <div class="answer-side side back">
                                    <div class="content" data-element="answer-side">
                                        <p class="question-text">A usted por su confianza. A su disposici&oacute;n para
                                            lo que precise. Le agradecer&iacute;a su valoraci&oacute;n positiva al salir
                                            del chat.</p><span class="question-age">hace 2 horas</span>
                                    </div>
                                    <div class="actions"><a class="button"
                                            href="legal-es/hgllu-sido-aut-nomo-durante-20-a-os-actualmente-soy-pensionista.html"
                                            data-element="view-question-btn">Leer más</a>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-outline"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="question">
                            <div class="question-inner">
                                <div class="question-side side front">
                                    <div class="content" data-element="question-side">
                                        <p class="question-text">SI<br />Asistente del experto: ¿Desde cuándo ocurren
                                            los síntomas que te preocupan?<br />Cliente: HACE UN MES<br />Asistente del
                                            experto: ¿Tomas alguna medicación de forma regular?<br />Cliente: Si,
                                            corazon, tension, parkinson, sintrom, etc....<br />Asistente del experto:
                                            ¿Hay algún otro detalle que crees que el médico debería saber?<br />Cliente:
                                            no</p><span class="question-age">hace 2 horas</span>
                                    </div>
                                    <div class="actions"><button class="button recent-questions-button"
                                            data-element="preview-answer-btn">Ver respuesta</button>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                                <div class="answer-side side back">
                                    <div class="content" data-element="answer-side">
                                        <p class="question-text">Si está sirviendo mi consulta por favor valórenme con
                                            cinco estrellas me puedes ir preguntando todo lo que desee</p><span
                                            class="question-age">hace 2 horas</span>
                                    </div>
                                    <div class="actions"><a class="button"
                                            href="urologia/hgllm-si-asistente-del-experto-desde-cu-ndo-ocurren-los-s-ntomas.html"
                                            data-element="view-question-btn">Leer más</a>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="question">
                            <div class="question-inner">
                                <div class="question-side side front">
                                    <div class="content" data-element="question-side">
                                        <p class="question-text">si, necesito ayuda para saber si mi esposa que acaba de
                                            darse de baja como autónoma tiene derecho a desempleo<br />Asistente del
                                            experto: ¿Has iniciado algún trámite oficial?<br />Cliente: se solicitó vía
                                            conversación telefono<br />Asistente del experto: ¿Hay algúna otra cosa que
                                            quieras que sepa el abogado?<br />Cliente: ica con el fremap y directamente
                                            me dijeron que no, que ya había agotado la prestación, pero según tengo
                                            entendido las prestaciones extraordinarias del covid no contaban como
                                            desempleo</p><span class="question-age">hace 2 horas</span>
                                    </div>
                                    <div class="actions"><button class="button recent-questions-button"
                                            data-element="preview-answer-btn">Ver respuesta</button>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                                <div class="answer-side side back">
                                    <div class="content" data-element="answer-side">
                                        <p class="question-text">Buenos d&iacute;as. Mi nombre es Juan Jesus
                                            S&aacute;nchez Ferrer, abogado colegiado del ICAM. Estoy leyendo su consulta
                                            y preparando la respuesta. Puede que la redacci&oacute;n tarde unos minutos,
                                            le ruego sea paciente ya que estamos preparando la m</p><span
                                            class="question-age">hace 2 horas</span>
                                    </div>
                                    <div class="actions"><a class="button"
                                            href="derecho-laboral/hgll5-si-necesito-ayuda-para-saber-si-mi-esposa-que-acaba-de.html"
                                            data-element="view-question-btn">Leer más</a>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="question">
                            <div class="question-inner">
                                <div class="question-side side front">
                                    <div class="content" data-element="question-side">
                                        <p class="question-text">Buenas tardes Quería saber qué debo hacer para
                                            contratar el servicio de un abogado y presentar un recurso contencioso ante
                                            el ministerio de universidades por la demora (19 meses) en la homologación
                                            de mi título extranjero de médico. Vivo en la provincia de Lleida,Cataluña.
                                            Que coste tiene el servicio? Gracias un saludo.</p><span
                                            class="question-age">hace 3 horas</span>
                                    </div>
                                    <div class="actions"><button class="button recent-questions-button"
                                            data-element="preview-answer-btn">Ver respuesta</button>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                                <div class="answer-side side back">
                                    <div class="content" data-element="answer-side">
                                        <p class="question-text">Le he ofrecido el servicio premium minimo, sin
                                            compromiso alguno, para el caso de que quiera que se le facilite un
                                            presupuesto. Saludos. </p><span class="question-age">hace 3 horas</span>
                                    </div>
                                    <div class="actions"><a class="button"
                                            href="derecho-civil/hgliy-buenas-tardes-quer-a-saber-qu-debo-hacer-para-contratar-el.html"
                                            data-element="view-question-btn">Leer más</a>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="question">
                            <div class="question-inner">
                                <div class="question-side side front">
                                    <div class="content" data-element="question-side">
                                        <p class="question-text">Hola buenos dias, si tengo una duda sobre una lesion
                                            que tengo<br />Asistente del experto: ¿Desde cuándo ocurren los síntomas que
                                            te preocupan?<br />Cliente: desde hace 4 o 5 dias y se han
                                            agravado<br />Asistente del experto: ¿Tomas alguna medicación de forma
                                            regular?<br />Cliente: no, solamente esta noche he tomado
                                            paracetamol<br />Asistente del experto: ¿Hay algún otro detalle que crees
                                            que el médico debería saber?<br />Cliente: tengo foto, creo que se trata de
                                            hemorroide externa pero me interesaria la valoracion para el tratamiento,
                                            tambien estuve dos dias muy deshidratado cuando comenzo la lesion</p><span
                                            class="question-age">hace 3 horas</span>
                                    </div>
                                    <div class="actions"><button class="button recent-questions-button"
                                            data-element="preview-answer-btn">Ver respuesta</button>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                                <div class="answer-side side back">
                                    <div class="content" data-element="answer-side">
                                        <p class="question-text">Me envia la foto</p><span class="question-age">hace 3
                                            horas</span>
                                    </div>
                                    <div class="actions"><a class="button"
                                            href="medicina-es/hglhl-hola-buenos-dias-si-tengo-una-duda-sobre-una-lesion-que.html"
                                            data-element="view-question-btn">Leer más</a>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="question">
                            <div class="question-inner">
                                <div class="question-side side front">
                                    <div class="content" data-element="question-side">
                                        <p class="question-text">Buenas noches mi bebé de 2 años tiene
                                            fiebre<br />Asistente del experto: ¿Desde cuándo ocurren estos
                                            síntomas?<br />Cliente: Ayer por la tarde<br />Asistente del experto: ¿Hay
                                            vómitos o mareos?<br />Cliente: No<br />Asistente del experto: ¿Hay algún
                                            otro detalle que crees que el médico debería saber?<br />Cliente: Solo tiene
                                            fiebre, le di Motrin 7ml, anda de buen humor, tiene apetito, solo la fiebre
                                            y poquito escurrimiento nasal, ayer hizo una evaluación no era líquida pero
                                            si más aguada de lo normal, después de esa evaluación no ha hecho más no
                                            tiene diarrea.</p><span class="question-age">hace 3 horas</span>
                                    </div>
                                    <div class="actions"><button class="button recent-questions-button"
                                            data-element="preview-answer-btn">Ver respuesta</button>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                                <div class="answer-side side back">
                                    <div class="content" data-element="answer-side">
                                        <p class="question-text">:) no olvide la valoraci&oacute;n!</p><span
                                            class="question-age">hace 3 horas</span>
                                    </div>
                                    <div class="actions"><a class="button"
                                            href="pediatria/hgle3-buenas-noches-mi-beb-de-a-os-tiene-fiebre-asistente-del.html"
                                            data-element="view-question-btn">Leer más</a>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="question">
                            <div class="question-inner">
                                <div class="question-side side front">
                                    <div class="content" data-element="question-side">
                                        <p class="question-text">Hola<br />Asistente del experto: ¡Hola! Dime, ¿en qué
                                            puedo ayudarte?<br />Cliente: Que tenido problemas con mi pareja y ella
                                            llamó a la policía, Y luego no ha declarado antes del juicio<br />Asistente
                                            del experto: ¿En qué lugar te encuentras? En cuestiones legales, siempre es
                                            importante saberlo, porque las normativas y leyes varían.<br />Cliente:
                                            Somos novios con un hijo<br />Asistente del experto: ¿Hay algúna otra cosa
                                            que quieras que sepa el abogado?<br />Cliente: No he declarado antes del
                                            juez Ella también no ha declarado</p><span class="question-age">hace 3
                                            horas</span>
                                    </div>
                                    <div class="actions"><button class="button recent-questions-button"
                                            data-element="preview-answer-btn">Ver respuesta</button>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                                <div class="answer-side side back">
                                    <div class="content" data-element="answer-side">
                                        <p class="question-text">Ella si debe presentarse pero acogerse a su derecho a
                                            no declarar.</p><span class="question-age">hace 3 horas</span>
                                    </div>
                                    <div class="actions"><a class="button"
                                            href="legal-es/hgld7-hola-asistente-del-experto-hola-dime-en-qu-puedo.html"
                                            data-element="view-question-btn">Leer más</a>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="question">
                            <div class="question-inner">
                                <div class="question-side side front">
                                    <div class="content" data-element="question-side">
                                        <p class="question-text">Disculpeme, una pregunta más. Si la carta de dimisión
                                            la presento el 6 de septiembre de 2021, y digo que dimito el 26 de
                                            septiembre, el último día en que estoy prestando servicio a la empresa es el
                                            26 de septiembre o el 25 de septiembre? Por otro lado, el preaviso no sería
                                            de 21 días, y no de 20 días en este caso?</p><span class="question-age">hace
                                            4 horas</span>
                                    </div>
                                    <div class="actions"><button class="button recent-questions-button"
                                            data-element="preview-answer-btn">Ver respuesta</button>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                                <div class="answer-side side back">
                                    <div class="content" data-element="answer-side">
                                        <p class="question-text">Recuerda valorar el servicio con las estrellas</p><span
                                            class="question-age">hace 4 horas</span>
                                    </div>
                                    <div class="actions"><a class="button"
                                            href="legal-es/hgl6x-disculpeme-una-pregunta-m-s-si-la-carta-de-dimisi-n-la.html"
                                            data-element="view-question-btn">Leer más</a>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="question">
                            <div class="question-inner">
                                <div class="question-side side front">
                                    <div class="content" data-element="question-side">
                                        <p class="question-text"><span style="vertical-align: inherit;"><span
                                                    style="vertical-align: inherit;">Good morning.In 2019 my friend
                                                    committed a robbery in a Madrid store, he went to the police station
                                                    and appeared in court the same day, and he did not contest anything
                                                    and they let him go, but he kept it in process. </span><span
                                                    style="vertical-align: inherit;">Now we want to go on a trip and you
                                                    have this problem, we are going to do a transfer in Madrid, can you
                                                    have problems because of the above or is it a matter of knowing how
                                                    the process progressed? </span><span
                                                    style="vertical-align: inherit;">Sincerely, Tiago ******* Assistant
                                                    to the expert: Is this the first accusation for this crime?
                                                </span><span style="vertical-align: inherit;">Client: yes Expert
                                                    assistant: Is there anything else you want the lawyer to know?
                                                </span><span style="vertical-align: inherit;">Customer:
                                                    no</span></span></br></p><span class="question-age">hace 4
                                            horas</span>
                                    </div>
                                    <div class="actions"><button class="button recent-questions-button"
                                            data-element="preview-answer-btn">Ver respuesta</button>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                                <div class="answer-side side back">
                                    <div class="content" data-element="answer-side">
                                        <p class="question-text">Cualquier duda me comenta en el chat por favor.</p>
                                        <span class="question-age">hace 4 horas</span>
                                    </div>
                                    <div class="actions"><a class="button"
                                            href="legal-es/hgl4c-buenos-d-as-en-el-a-o-2019-mi-amigo-cometi-un-robo-en-una.html"
                                            data-element="view-question-btn">Leer más</a>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="question">
                            <div class="question-inner">
                                <div class="question-side side front">
                                    <div class="content" data-element="question-side">
                                        <p class="question-text">Si,necesito una consulta sobre un tema de
                                            alquiler<br />Asistente del experto: ¿El alquiler lo pagas directamente al
                                            dueño o a través de un intermediario?<br />Cliente: Directamente al
                                            dueño<br />Asistente del experto: ¿Hay algúna otra cosa que quieras que sepa
                                            el abogado?<br />Cliente: Necesito un experto en contratos de alquileres y
                                            obligaciones</p><span class="question-age">hace 5 horas</span>
                                    </div>
                                    <div class="actions"><button class="button recent-questions-button"
                                            data-element="preview-answer-btn">Ver respuesta</button>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                                <div class="answer-side side back">
                                    <div class="content" data-element="answer-side">
                                        <p class="question-text">Gracias a usted por su confianza. A su
                                            disposici&oacute;n para lo que precise y le agradecer&iacute;a su valoracion
                                            positiva al salir del chat.</p><span class="question-age">hace 5
                                            horas</span>
                                    </div>
                                    <div class="actions"><a class="button"
                                            href="legal-es/hgkzh-si-necesito-una-consulta-sobre-un-tema-de-alquiler-asistente.html"
                                            data-element="view-question-btn">Leer más</a>
                                        <div class="rating"><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i><i class="star"><svg>
                                                    <use xlink:href="#icon-star-filled"></use>
                                                </svg></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><span id="recent-questions-down" class="arrow arrow-down"
                        data-element="recent-questions-next"><i class="arrow-icon"><svg>
                                <use xlink:href="#icon-arrow-down"></use>
                            </svg></i></span>
                </div>
            </div>
        </div>
        <footer class="de-footer_intl" role="complementary" data-component="footer">
            <div class="inner-footer">
                <div class="footer-left">
                    <div class="footer-left-inner">
                        <div class="footer-left-top">
                            <div class="list-box">
                                <h3 class="list-title" tabindex="0">Uso de JustAnswer</h3>
                                <ul class="list">
                                    <li class="maintab"><span class="ui-link" tabindex="0">Ver categorías</span><span
                                            class="drop-down-arrow"></span>
                                        <ul class="sublist">
                                            <li class="subitem"><a class="ui-link">Médicos Online</a></li>
                                            <li class="subitem"><a class="ui-link" >Abogados Online</a></li>
                                            <li class="subitem"><a class="ui-link" >Mecánicos Online</a></li>
                                            <li class="subitem"><a class="ui-link" >Veterinarios Online</a></li>
                                            <li class="subitem"><a class="ui-link" >Técnicos Online</a></li>
                                        </ul>
                                    </li>
                                    <li class="maintab"><span class="ui-link" tabindex="0">Otros países</span><span
                                            class="drop-down-arrow"></span>
                                        <ul class="sublist">
                                            <li class="subitem"><a class="ui-link" href="#" >JustAnswer Estados Unidos</a></li>
                                            <li class="subitem"><a class="ui-link" href="#">JustAnswer Reino Unido</a></li>
                                            <li class="subitem"><a class="ui-link"  href="#">JustAnswer Alemania</a></li>
                                            <li class="subitem"><a class="ui-link"  href="#" ></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="list-box">
                                <h3 class="list-title" tabindex="0">Atención al cliente</h3>
                                <ul class="list">
                                    <li><a href="#" >Pregunta frecuentes</a></li>
                                    <li><a href="login.html">Iniciar sesión</a></li>
                                    <li><a href="account/register.html">Registrarse</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="list-box">
                                <h3 class="list-title" tabindex="0">Acerca de JustAnswer</h3>
                                <ul class="list">
                                    <li><a href="#">Acerca de JustAnswer</a></li>
                                    <li><a href="#">Cómo funciona</a></li>
                                    <li><a href="#"">Sala de prensa</a></li>
                                </ul>
                            </div>
                            <div class="list-box">
                                <h3 class="list-title" tabindex="0">Únase a JustAnswer</h3>
                                <ul class="list">
                                    <li><a href="#">Empleo</a></li>
                                    <li><a href="#">Convertirse en Experto</a></li>
                                    <li><a href="#">Programa de Afiliados</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="footer-left-bottommost" role="contentinfo">
                            <p>© 2003-2021 JustAnswer LLC.</p><a href="#">Aviso de privacidad</a>
                            <a href="#">Términos de servicio</a><a href="#">Contacto</a>
                            <a href="#">Mapadel sitio</a>
                            <a href="#" >Impressum</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>

     <script src="js/in-ja.js"></script>
    <script src="js/in-ja1.js"></script>
    <script src="js/in-ja2.js"></script>
    <script src="js/in-ja3.js"></script>
    <script src="js/in-ja4.js"></script>
    <script src="js/in-ja5.js"></script>
    <script src="js/in-ja6.js"></script>
    <script src="js/in-ja7.js"></script>
    <script src="js/in-ja8.js"></script>
    <script src="js/in-ja9.js"></script>
    <script src="js/in-ja11.js"></script>
    <script src="js/in-ja12.js"></script>
    <script src="js/in-ja13.js"></script>
    <script src="js/in-ja14.js"></script>
    <script src="js/in-ja15.js"></script>
    <script src="js/in-ja16.js"></script>
    <script src="js/in-ja17.js"></script>
    <script src="js/in-ja18.js"></script>
    <script src="js/in-ja19.js"></script> 

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "name": "JustAnswer",
        "alternateName": "Just Answer"
    }
    </script>
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "JustAnswer",
        "url": "https://www.justanswer.es/",
        "legalName": "JustAnswer LLC",
        "logo": "http://www.justanswer.com/img/logos/logo-homepage-trim.png",
        "sameAs": ["https://www.facebook.com/Justansweres/", "https://twitter.com/JustAnswer_es",
            "https://plus.google.com/+justanswer", "https://es.wikipedia.org/wiki/JustAnswer",
            "http://www.pinterest.com/JustAnswer/", "https://www.youtube.com/user/justanswer",
            "https://www.linkedin.com/company/justanswer"
        ],
        "contactPoint": [{
            "@type": "ContactPoint",
            "telephone": "informacion@justanswer.es",
            "contactType": "Atención al cliente"
        }]
    }
    </script>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script src="js/cookies.js"></script>
    <script>
    require("@justanswer/th-page-hp_es")(document.querySelector(".th-page-hp_es"))
    </script>
    <script type="text/javascript">
    window._prlI = window._prlI || {};
    _prlI.pageName = 'MyHome';
    _prlI.isDev = "False";
    _prlI.serverName = "w03";
    _prlI.categoryId = "e750675a458142bbbbcba424459e181f";
    </script>
    <script src="js/v3/ja-es.js.js"></script>
    <script>
    if (window._satellite) {
        _satellite.pageBottom();
    } else if (window.console && console.error) {
        console.error("JATM Satellite object is not present on bottom of the page");
    }
    </script>
</body>

</html>