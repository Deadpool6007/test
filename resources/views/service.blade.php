<!DOCTYPE HTML>
<html lang="en">
<head>
   <meta charset="UTF-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="keyword" content="asist, answer, solutions">
       <meta name="description" content="we asist you on your each feet">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Document</title>   
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel="stylesheet" href="css/help.css">
</head>        
<body>
    
     <div class="contactus">
        <div class="bgimg">
            <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
              <div class="container">
                  <a class="navbar-brand" href="#"><img src="images/asistley.png" ></a>
                  <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> -->
                    <!-- <span class="navbar-toggler-icon"></span> -->
                  </button>
              <div class="shiftnav">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                   <ul class="navbar-nav ml-auto">
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mis preguntas </a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Contacto</a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mi cuenta</a>
                      </li>
                    </ul>
                </div>
              </div>         <!--container-->
              <div class="mobile-item"> 
             <a class="nav-link" href="#">Mi cuenta</a>
           </div>
           </div>
           </nav>

           <div class="shift">
             <div class="backcolr">
                <div class="resize">
                  <div class="textcolr">
                        <div  style="vertical-align: inherit;"><div style="vertical-align: inherit;">¿En qué le podemos ayudar?</div></div> 
                  </div>  
                  <div class="search_form"> 
                        <input id="search-input" class="search__input" type="text" name="f" placeholder="Busque por palabras clave como: pago, factura, membresía...">
                        <img src="/images/icons8-search.gif" class="icon" >
                  </div>
                </div>
             </div>
           </div>        


       <div class="containermargin">
           <div class="container">
               <div class="row">
                  <div class="col-lg-10 col-md-10 col-sm-10 col-12">
                    <div class="nu_container"><h5><b>Su satisfacción es nuestra prioridad</b></h5>
                       <p>Estamos orgullosos de satisfacer las necesidades de nuestros clientes.<br>
                        Si por alguna razón no está satisfecho, póngase en contacto con nuestro equipo de Atención al cliente, disponible las 24 horas del día.</p>
                    </div>
                  </div>
               <div class="col-lg-2 col-md-2 col-sm-2 col-12">
                    <div>
                       <button class="efb"> Contacto</button>
                    </div>
               </div>
               </div>   
                 <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        <div class="mysidebar">
      
                           <div class="sidebar">
        
                                <ul class="sidebar__list">
                                     <li class="sidebar__link "><a href="help">Preguntas más frecuentes</a></li>
                                     <li class="sidebar__link "><a href="aboutjustans">Acerca de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="usingjustans">Uso de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="service">Servicios premium</a></li>
                                     <li class="sidebar__link "><a href="help">Prueba de membresía/Membresía</a></li>
                                     <li class="sidebar__link "><a href="paymentbilling">Pago y facturación</a></li>
                                     <li class="sidebar__link "><a href="accountsetting">Administrar mi cuenta</a></li>
                                </ul>
                           </div>
                        </div>
                      </div>
                     <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                            <div class="contactlist">
                            <h2 class="content"><b>Servicios premium</b></h2><br>
                            
                     
             <hr></hr>
                    <h6><b>Servicios de Expertos</b></h6>
                   <button class="accordion">Acepté una llamada telefónica del Experto. ¿Qué debo hacer ahora?</button>
                     <div class="panel">
                          <p>Ahora que ha aceptado una llamada telefónica, revise los siguientes consejos para asegurase de obtener el máximo provecho de su llamada.<br>

1.Proporcione al Experto su número de teléfono para que pueda llamarle en el cuadro de mensaje privado.<br>
2.El Experto le informará sobre cuándo estará disponible y concretará con usted cuál es el mejor momento para realizar la llamada. La mayoría de las veces usted recibirá la llamada en cuestión de minutos. Si usted no ha recibido la llamada puede enviar una respuesta al Experto haciéndole saber que está listo.<br>
3.Si el Experto que le está atendiendo dice que no está disponible para llamarle en ese momento, no hay de qué preocuparse. Encontraremos a otro Experto que lo llame, por lo general en 60 minutos o menos.
Esperamos que su llamada sea exitosa y que podamos ayudarle a responder más preguntas en el futuro.</p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">Acepté una sesión de Asistencia Remota Segura ¿Qué significa y cómo puedo empezar?</button>
                     <div class="panel">
                          <p>Ahora que ha aceptado la oferta de Asistencia remota segura, revise los consejos útiles para asegurarse de obtener el máximo provecho de su sesión.<br>

1.Busque un enlace que ha sido compartido con usted por parte del Experto y haga clic en él para empezar la sesión. Si usted tiene algún problema, responda al Experto para que éste le indique cómo continuar.<br>
2.Si usted todavía está a la esprera de obtener el enlace, escríbale al Experto para recordarle que está esperando. Si no recibe una respuesta en el momento oportuno, puede ponerse en contacto con nuestro servicio de atención al cliente.<br>
3.Recuerde, todas las sesiones de Asistencia Remota son seguras y privadas. Los Expertos pueden acceder a su equipo y solucionar su problema mientras usted observa. Una vez que la sesión ha terminado, el Experto ya no tiene acceso a su ordenador.
Esperamos que la sesión de Asistencia remota segura sea un éxito y que podamos ayudarle a responder las preguntas en el futuro. </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">He recibido una oferta de Servicio Premium. ¿Qué significa y cómo funciona?</button>
                     <div class="panel">
                          <p>Algunas veces una respuesta en línea no es suficiente. Por esto, usted puede recibir ayuda adicional de los Expertos en JustAnswer. El Experto puede ayudarle a obtener una respuesta más completa con una variedad de servicios como: llamada telefónica, videollamada, preparación de documentos, entre otros.

Los servicios premium son servicios adicionales que se salen del estándar de las respuestas que hasta ahora ha tenido en JustAnswer. Los servicios premium pueden ser llevados a cabo fuera de la página de JustAnswer (sea por teléfono, chat, correo electrónico, control remoto del ordenador, o cualquier otra vía de comunicación) pero siguen siendo un servicio con fines informativos generales. Para aceptar el Servicio Premium del Experto, simplemente haga clic en el botón "Aceptar la oferta". </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">Mi pago por Servicio Premium ha fallado. ¿Qué debo hacer?</button>
                     <div class="panel">
                          <p>Si su pago por Servicio Premium ha fallado, pero usted sigue interesado en obtener ayuda, por favor, póngase en contacto con su entidad de pago para comprobar si ha ocurrido uno de los siguientes errores:  <br>

1.La entidad de pago ha rechazado la transacción por motivos de seguridad.<br>
2.La cuenta no dispone de fondos suficientes. <br>
3.La tarjeta de crédito ha caducado. 
Una vez se haya resuelto el problema con su entidad de pago, por favor, póngase en contacto con nuestro servicio de Atención al cliente. </p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                 <button class="accordion">¿Por qué hay un cargo extra por el servicio premium?</button>
                     <div class="panel">
                          <p>El servicio premium es llevado a cabo fuera de la plataforma tradicional de JustAnswer. Por ello, el Experto puede ofrecerle una llamada telefónica, una videollamada o la preparación de un documento como puede ser la redacción de un contrato o un testamento. Debido a que el servicio premium es realizado fuera de la página y requiere más allá de una respuesta a su pregunta, el cargo extra se realiza para compensar al profesional por el tiempo adicional ayudándolo.


   
                        </p>
                        <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

             
</div>
</div>
</div>
</div>
</div>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>



                     <footer>
                       <div class="container">
                         <div class="row">
                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                            
                             <ul class="listround">
                               <li><b>JustAnswer</b></li>
                               <li><a href="">Inicio</a></li>
                               <li><a href="">Acerca de JustAnswer</a></li>
                               
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Clientes</b></li>
                               
                               <li><a href="">Iniciar sesión</a></li>
                               <li><a href="">Registrarse</a></li>
                               <li>Categorías</li>
                             
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Expertos</b></li>
                               
                               <li><a href="">Convertirse en Experto</a></li>
                              
              
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Atención al cliente</b></li>
                               <li><a href="">Ayuda</a></li>
                               <li><a href="">Contacto</a></li>

                             </ul>
                           </div>

                         </div>
                      </div>
      </footer>
     
      
<!--footer-section-->
<div class="f-section">
   <div class="links">
    <img src="images/norton_secured.png">
    <br>
    <span>© 2003-2021 Asistley LLC</span>

    <p>
      <a href="#">Aviso de privacidad</a> &nbsp; &nbsp;
      <a href="#">Contacto</a> &nbsp; &nbsp;
      <a href="#">Términos de servicio</a> &nbsp; &nbsp;
      <a href="#">Mapa del sitio</a>
    </p>
    </div>     <!--links-->
<div>       <!--f-section-->
 





<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>