<!DOCTYPE HTML>
<html lang="en">
<head>
   <meta charset="UTF-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="keyword" content="asist, answer, solutions">
       <meta name="description" content="we asist you on your each feet">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Document</title>   
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel="stylesheet" href="css/style.css">
</head>        
<body>
    
     <div class="contactus">
        <div class="bgimg">
            <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
              <div class="container">
                  <a class="navbar-brand" href="#"><img src="" ></a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
              <div class="shiftnav">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                   <ul class="navbar-nav ml-auto">
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mis preguntas </a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Contacto</a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mi cuenta</a>
                      </li>
                    </ul>
                </div>
              </div>         <!--container-->
           </div>
           </nav>

           <div class="shift">
             <div class="backcolr">
                <div class="resize">
                  <div class="textcolr">
                        <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">¿En qué le podemos ayudar?</font></font> 
                  </div>  
                  <div class="search_form"> 
                        <input id="search-input" class="search__input" type="text" name="f" placeholder="Busque por palabras clave como: pago, factura, membresía...">
                        <img src="/images/icons8-search.gif" class="icon" >
                  </div>
                </div>
             </div>
           </div>        


       <div class="containermargin">
           <div class="container">
               <div class="row">
                  <div class="col-lg-10 col-md-10 col-sm-10 col-12">
                    <div class="nu_container"><h5><b>Su satisfacción es nuestra prioridad</b></h5>
                       <p>Estamos orgullosos de satisfacer las necesidades de nuestros clientes.<br>
                        Si por alguna razón no está satisfecho, póngase en contacto con nuestro equipo de Atención al cliente, disponible las 24 horas del día.</p>
                    </div>
                  </div>
               <div class="col-lg-2 col-md-2 col-sm-2 col-12">
                    <div>
                       <button class="efb"> Contacto</button>
                    </div>
               </div>
               </div>   
                 <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        <div class="mysidebar">
      
                           <div class="sidebar">
        
                                <ul class="sidebar__list">
                                     <li class="sidebar__link "><a href="help">Preguntas más frecuentes</a></li>
                                     <li class="sidebar__link "><a href="aboutjustans">Acerca de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="usingjustans">Uso de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="service">Servicios premium</a></li>
                                     <li class="sidebar__link "><a href="help">Prueba de membresía/Membresía</a></li>
                                     <li class="sidebar__link "><a href="paymentbilling">Pago y facturación</a></li>
                                     <li class="sidebar__link "><a href="accountsetting">Administrar mi cuenta</a></li>
                                </ul>
                           </div>
                        </div>
                      </div>
                     <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                            <div class="contactlist">
                            <h2 class="content"><b>Pago y facturación</b></h2><br>
                            
                     
             <hr></hr>
                    <h6><b>Membresía</b></h6>
                   <button class="accordion">¿Cuándo se producen los cargos de la membresía?</button>
                     <div class="panel">
                          <p>La primera cuota mensual de membresía se cargará después de que termine el período de prueba (por lo general, después de 7 días). Posteriormente, la cuota mensual se cargará cada 30 días de manera automática, aunque el cargo puede tardar unos días en aparecer en su cuenta. </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                     <button class="accordion">¿Cómo puedo cancelar mi membresía?</button>
                     <div class="panel">
                          <p>Lamentamos que quiera cancelar su membresía. Para cancelar la membresía, vaya a la sección Membresía de Mi cuenta y haga clic en el botón Cancelar membresía. Recuerde que siempre puede obtener una segunda opinión o una respuesta nueva de otro Experto.  </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                     <hr></hr>
                    <h6><b>Pago por preguntas</b></h6>
                   <button class="accordion"> ¿Puedo ver el historial de pagos?</button>
                     <div class="panel">
                          <p>Usted puede ver sus preguntas recientes desde la sección de Mis preguntas.  Además, puede ver sus preguntas y el estado de sus pedidos en los últimos 90 días en la sección Historial de la configuración de su cuenta. 

Si usted no puede ver la sección de Mis preguntas en la parte superior de la página, intente iniciar sesión en su cuenta. </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                     <button class="accordion"> ¿Cuánto cuesta?</button>
                     <div class="panel">
                          <p>Se le cobrará por publicar la pregunta. Cualquier cargo adicional se verá reflejado en la página de pago. Si ya había publicado una pregunta, puede ver todos los cargos y el historial de sus pagos en la sección de Historial de Mi Cuenta.  </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                     <button class="accordion">¿Por qué debo pagar para publicar una pregunta?</button>
                     <div class="panel">
                          <p>El pago es necesario para reservar el tiempo del Experto. Para revisar el historial de sus pagos, visite la página de Historial.  </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                     <button class="accordion"> ¿Por qué se ha rechazado mi pago?</button>
                     <div class="panel">
                          <p>Lamentamos que esté teniendo problemas con el procesamiento de su pago. Existen varias razones por las que su pago puede ser rechazado: 

1.El número de la tarjeta de crédio, la fecha de caducidad o el código de seguridad son incorrectos. Intente volver a introducir los datos tal y como aparecen en la tarjeta de crédito. 
2.La tarjeta de crédito ha caducado. Asegúrese de que la información está actualizada y es correcta.
3.La cuenta no dispone de fondos suficientes. Póngase en contacto con su entidad bancaria para asegurarse de que dispone de fondos suficientes para cubrir el coste del servicio en su cuenta.
4.La entidad de pago ha rechazado la transacción por motivos de seguridad. Póngase en contacto con su entidad bancaria para averiguar si la transacción ha sido rechazada por motivos de seguridad y verificar que su tarjeta no se ha visto comprometida. 
Si todavía tiene problemas con su pago, por favor, póngase en contacto con nuestro servicio de Atención al cliente. 

 </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>


                <hr> </hr> 
                <h6><b>Pago no reconocido</b></h6>
                     <button class="accordion">He recibido un cargo en mi tarjeta de crédito de su compañía ¿Qué es JustAnswer?</button>
                     <div class="panel">
                          <p>JustAnswer es un sitio web dedicado a la solución de problemas de los usuarios, contamos con miles de Expertos dispuestos a responder las inquietudes en diferentes áreas cotidianas como lo son: legal, medicina, veterinaria, mecánica, las 24 horas del día. Es la forma más económica y conveniente de acceder a servicios de expertos.

Si usted necesita más información sobre JustAnswer, haga clic aquí. </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                     <hr></hr>
                  


                    
                <h6><b>Reembolsos</b></h6>
                     <button class="accordion">¿Cómo puedo solicitar un reembolso?</button>
                     <div class="panel">
                          <p>En JustAnswer, nuestra máxima prioridad es asegurarse de que nuestros clientes están satisfechos. Casi siempre encontramos una manera de ayudar a cada cliente, pero si no, póngase en contacto con nuestro equipo de Atención al cliente.</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  
                     <hr></hr>
                    <h6><b>Otro</b></h6>
                   <button class="accordion">¿Cómo puedo actualizar mi información de pago?</button>
                     <div class="panel">
                          <p>Usted puede actualizar su información de pago en cualquier momento haciendo clic en Mi cuenta en la parte superior de la página. Después debe hacer clic en Editar y usted podrá cambiar su método de pago.

Si usted no ha iniciado sesión en JustAnswer, haga clic aquí y siga las instrucciones anteriores. </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                 

             
</div>
</div>
</div>
</div>
</div>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>



                     <footer>
                       <div class="container">
                         <div class="row">
                           <div class="col-lg-3">
                            
                             <ul class="listround">
                               <li><b>JustAnswer</b></li>
                               <li><a href="">Inicio</a></li>
                               <li><a href="">Acerca de JustAnswer</a></li>
                               
                             </ul>
                           </div>

                           <div class="col-lg-3">
                             <ul class="listround">
                               <li><b>Clientes</b></li>
                               
                               <li><a href="">Iniciar sesión</a></li>
                               <li><a href="">Registrarse</a></li>
                               <li>Categorías</li>
                  
                         <ul>
                        <a href="">Preguntas de medicina</a></li>
                        
                          <a href="">Pregunte a un abogado</a></li>
                          <li><a href="">Preguntas de mecánica</a></li>
                          <li><a href="">Pregunte a un veterinario</a></li>
                          <li><a href="">Preguntas de informática</a></li>
                          <li><a href="">Preguntas de electrónica</a></li>
                          <li><a href="">Otras preguntas</a></li>
                        </ul>
                      </li>
                    
                               
                             </ul>
                           </div>

                           <div class="col-lg-3">
                             <ul class="listround">
                               <li><b>Expertos</b></li>
                               
                               <li><a href="">Convertirse en Experto</a></li>
                              
              
                             </ul>
                           </div>

                           <div class="col-lg-3">
                             <ul class="listround">
                               <li><b>Atención al cliente</b></li>
                               <li><a href="">Ayuda</a></li>
                               <li><a href="">Contacto</a></li>

                             </ul>
                           </div>

                         </div>
                      </div>
</footer>






<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>