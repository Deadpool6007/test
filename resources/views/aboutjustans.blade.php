<!-- about us answer -->

<!DOCTYPE HTML>
<html lang="en">
<head>
   <meta charset="UTF-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="keyword" content="asist, answer, solutions">
       <meta name="description" content="we asist you on your each feet">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Document</title>   
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel="stylesheet" href="css/help.css">
</head>        
<body>
    
     <div class="contactus">
        <div class="bgimg">
            <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
              <div class="container">
                  <a class="navbar-brand" href="#"><img src="images/asistley.png" ></a>
                  <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> -->
                    <!-- <span class="navbar-toggler-icon"></span> -->
                  </button>
              <div class="shiftnav">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                   <ul class="navbar-nav ml-auto">
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mis preguntas </a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Contacto</a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mi cuenta</a>
                      </li>
                    </ul>
                </div>
              </div>         <!--container-->
              <div class="mobile-item"> 
                <a class="nav-link" href="#">Mi cuenta</a>
             </div>
            </div>
           </nav>

           <div class="shift">
             <div class="backcolr">
                <div class="resize">
                  <div class="textcolr">
                        <div style="vertical-align: inherit;"><div style="vertical-align: inherit;">¿En qué le podemos ayudar?</div></div> 
                  </div>  
                  <div class="search_form"> 
                        <input id="search-input" class="search__input" type="text" name="f" placeholder="Busque por palabras clave como: pago, factura, membresía...">
                        <img src="/images/icons8-search.gif" class="icon" >
                  </div>
                </div>
             </div>
           </div>        


       <div class="containermargin">
           <div class="container">
               <div class="row">
                  <div class="col-lg-10 col-md-10 col-sm-10 sm-d-none col-12">
                    <div class="nu_container"><h5><b>Su satisfacción es nuestra prioridad</b></h5>
                       <p>Estamos orgullosos de satisfacer las necesidades de nuestros clientes.<br>
                        Si por alguna razón no está satisfecho, póngase en contacto con nuestro equipo de Atención al cliente, disponible las 24 horas del día.</p>
                    </div>
                  </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                    <div>
                       <button class="efb"> Contacto</button>
                    </div>
               </div>
               </div>   
                 <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        <div class="mysidebar">
      
                           <div class="sidebar">
        
                                <ul class="sidebar__list">
                                     <li class="sidebar__link "><a href="help">Preguntas más frecuentes</a></li>
                                     <li class="sidebar__link "><a href="aboutjustans">Acerca de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="usingjustans">Uso de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="service">Servicios premium</a></li>
                                     <li class="sidebar__link "><a href="help">Prueba de membresía/Membresía</a></li>
                                     <li class="sidebar__link "><a href="paymentbilling">Pago y facturación</a></li>
                                     <li class="sidebar__link "><a href="accountsetting">Administrar mi cuenta</a></li>
                               </ul>
                           </div>
                        </div>
                      </div>
                     <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                            <div class="contactlist">
                            <h2 class="content"><b>Acerca de JustAnswer</b></h2><br>
                            
                     
             <hr></hr>
                   <h6><b>Cómo funciona JustAnswer</b></h6>
                   <button class="accordion">¿Cuándo recibiré una respuesta?</button>
                     <div class="panel">
                          <p>La mayoría de las preguntas reciben una respuesta dentro de los primeros 30 minutos. Sin embargo, durante las horas pico los Expertos pueden tardar hasta 24 horas en responder a su pregunta.  

                           Cuando su pregunta tenga una respuesta, usted recibirá una notificación por correo electrónico. </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">¿Cuánto cuesta?</button>
                     <div class="panel">
                          <p>Se le cobrará por publicar la pregunta. Cualquier cargo adicional se verá reflejado en la página de pago. Si ya había publicado una pregunta, puede ver todos los cargos y el historial de sus pagos en la sección de Historial de Mi Cuenta.  </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">¿Cómo puedo publicar una pregunta?</button>
                     <div class="panel">
                          <p>Para publicar una pregunta en JustAnswer®. Normalmente recibirá una respuesta dentro de unos minutos.  El Experto puede hacer preguntas aclaratorias o solicitar más informácion para poder ofrecerle la respuesta correcta. Usted puede realizar tantas preguntas de seguimiento como necesite utilizando el botón Responder ubicado en la parte inferior de la página de la pregunta. </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">¿Es Justanswer gratis?</button>
                     <div class="panel">
                          <p>Nuestro objetivo es proporcionar ayuda rápida y asequible de Médicos, Abogados, Mecánicos y más Expertos. JustAnswer no es gratis, sin embargo, su coste es solo una fracción de la tarifa de un servicio profesional.</p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                     <button class="accordion">¿Qué es JustAnswer®?</button>
                     <div class="panel">
                          <p>JustAnswer es el mayor servicio de pago en línea que conecta a personas de todo el mundo con Expertos acreditados estableciendo una relación de tú a tú con médicos, abogados, mecánicos, veterinarios y más. El objetivo de JustAnswer es ayudar a las personas proporcionándoles la mejor plataforma online para que los usuarios puedan acceder a Expertos acreditados de forma rápida, cómoda y asequible. Desde el 2003 hemos ayudado a más de 9 millones de personas en los 196 países del mundo.</p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                <hr></hr>
                <h6><b>Los Expertos de JustAnswer</b></h6>
                <button class="accordion">Control de calidad </button>
                     <div class="panel">
                          <p>Los Expertos de JustAnswer pasan por un proceso de control de calidad y confirmación de las credenciales. </p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                     <hr></hr>
                <h6><b>Anonimato y seguridad</b></h6>
                <button class="accordion">Anónimo y Seguro </button>
                     <div class="panel">
                          <p>JustAnswer es un foro público y las preguntas y respuestas no son confidenciales. Los usuarios de la página permanecen anónimos siempre y cuando no ingresen ninguna información que pueda identificarles en el texto de la pregunta. Es recomendable no mencionar sus datos personales y no incluir su nombre y apellidos en el texto de la pregunta.  

                           Si hay información personal en su pregunta que a usted le gustaría retirar, por favor contacte con nosotros. Al enviar el correo electrónico, por favor incluya el enlace de la pregunta y la información que desea eliminar. </p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                     <hr></hr>
                <h6><b>Otro</b></h6>
                <button class="accordion">Categorías de JustAnswer </button>
                     <div class="panel">
                          <p>JustAnswer ofrece una amplia variedad de categorías, incluidas las siguientes: </p>

                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>


             
</div>
</div>
</div>
</div>
</div>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>



                     <footer>
                       <div class="container">
                         <div class="row">
                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                            
                             <ul class="listround">
                               <li><b>JustAnswer</b></li>
                               <li><a href="#">Inicio</a></li>
                               <li><a href="#">Acerca de JustAnswer</a></li>
                               
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Clientes</b></li>
                               
                               <li><a href="#">Iniciar sesión</a></li>
                               <li><a href="#">Registrarse</a></li>
                               <li><a href="#">Categorías</a></li>
                            </li>
                    
                               
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Expertos</b></li>
                               
                               <li><a href="#">Convertirse en Experto</a></li>
                              
              
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Atención al cliente</b></li>
                               <li><a href="#">Ayuda</a></li>
                               <li><a href="#">Contacto</a></li>

                             </ul>
                           </div>

                         </div>
                      </div>
</footer>


<!--footer-section-->
<div class="f-section">
    <div class="links">
    <img src="images/norton_secured.png">
    <br>
    <span>© 2003-2021 Asistley LLC</span>
     <p>
      <a href="#">Aviso de privacidad</a> &nbsp; &nbsp;
      <a href="#">Contacto</a> &nbsp; &nbsp;
      <a href="#">Términos de servicio</a> &nbsp; &nbsp;
      <a href="#">Mapa del sitio</a>
     </p>
    </div>     <!--links-->
<div>       <!--f-section-->





<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>