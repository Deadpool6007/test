<!DOCTYPE HTML>
<html lang="en">
<head>
   <meta charset="UTF-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="keyword" content="asist, answer, solutions">
       <meta name="description" content="we asist you on your each feet">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Document</title>   
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel="stylesheet" href="css/help.css">
</head>        
<body>
    
     <div class="contactus">
        <div class="bgimg">
            <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
              <div class="container">
                  <a class="navbar-brand" href="#"><img src="images/asistley.png" ></a>
                  <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> -->
                    <!-- <span class="navbar-toggler-icon"></span> -->
                  </button>
              <div class="shiftnav">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                   <ul class="navbar-nav ml-auto">
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mis preguntas </a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Contacto</a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mi cuenta</a>
                      </li>
                    </ul>
                </div>
              </div>         <!--container-->
              <div class="mobile-item"> 
                <a class="nav-link" href="#">Mi cuenta</a>
             </div>
           </div>
           </nav>

           <div class="shift">
             <div class="backcolr">
                <div class="resize">
                  <div class="textcolr">
                        <div style="vertical-align: inherit;"><div style="vertical-align: inherit;">¿En qué le podemos ayudar?</div></div> 
                  </div>  
                  <div class="search_form"> 
                        <input id="search-input" class="search__input" type="text" name="f" placeholder="Busque por palabras clave como: pago, factura, membresía...">
                        <img src="/images/icons8-search.gif" class="icon" >
                  </div>
                </div>
             </div>
           </div>        


       <div class="containermargin">
           <div class="container">
               <div class="row">
                  <div class="col-lg-10 col-md-10 col-sm-10 col-12">
                    <div class="nu_container"><h5><b>Su satisfacción es nuestra prioridad</b></h5>
                       <p>Estamos orgullosos de satisfacer las necesidades de nuestros clientes.<br>
                        Si por alguna razón no está satisfecho, póngase en contacto con nuestro equipo de Atención al cliente, disponible las 24 horas del día.</p>
                    </div>
                  </div>
               <div class="col-lg-2 col-md-2 col-sm-2 col-12">
                    <div>
                       <button class="efb"> Contacto</button>
                    </div>
               </div>
               </div>   
                 <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        <div class="mysidebar">
      
                           <div class="sidebar">
        
                                <ul class="sidebar__list">
                                     <li class="sidebar__link "><a href="help">Preguntas más frecuentes</a></li>
                                     <li class="sidebar__link "><a href="aboutjustans">Acerca de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="usingjustans">Uso de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="service">Servicios premium</a></li>
                                     <li class="sidebar__link "><a href="help">Prueba de membresía/Membresía</a></li>
                                     <li class="sidebar__link "><a href="">Pago y facturación</a></li>
                                     <li class="sidebar__link "><a href="accountsetting">Administrar mi cuenta</a></li>
                                </ul>
                           </div>
                        </div>
                      </div>
                     <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                            <div class="contactlist">
                            <h2 class="content"><b>Administrar mi cuenta</b></h2><br>
                            
                     
             <hr></hr>
                    <h6><b>Cerrar la cuenta</b></h6>
                   <button class="accordion">¿Cómo puedo cerrar mi cuenta?</button>
                     <div class="panel">
                          <p>Si desea cerrar su cuenta de usuario con JustAnswer, por favor contacte con nuestro servicio de Atención al cliente.</p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                     <hr></hr>
                    <h6><b>Dejar de recibir correos electrónicos</b></h6>
                   <button class="accordion">¿Cómo puedo darme de baja de los correos de publicidad?</button>
                     <div class="panel">
                          <p>JustAnswer periódicamente envía correos electrónicos a los clientes para ayudarles con las preguntas frecuentes y además para hacerles saber de nuestras ofertas. Para dejar de recibir estas comunicaciones, acceda a la sección de Mi cuenta desde la parte superior de la página. Poseriormente, haga clic en "Editar" y desactive la casilla que hay debajo de su dirección de correo electrónico que dice: "Por favor, contacte conmigo cuando haya notificaciones por parte de los Expertos o cuando hay descuentos y ofertas disponibles." Haga clic en "Guardar" para guardar los cambios.

Usted también puede cancelar la recepción de las comunicaciones, haciendo clic en el enlace "Darse de baja" que encontrará en la parte inferior de las comunicaciones que recibe de JustAnswer.</p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>


                <hr> </hr> 
                <h6><b>Actualizar la información</b></h6>
                     <button class="accordion">¿Cómo puedo cambiar mi configuración?</button>
                     <div class="panel">
                          <p>Usted puede cambiar la configuración de su cuenta desde la sección de Mi cuenta ubicada en la parte superior de la página. Usted tiene la posibilidad de cambiar su correo electrónico, su contraseña, su número de teléfono y la información de pago. No olvide de guardar su configuración antes de salir. 

Nota: el nombre de usuario no se puede cambiar desde la página de configuración de la cuenta. Para cambiar su nombre de usuario, por favor, póngase en contacto con el servicio de Atención al cliente. </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">¿Cómo puedo cambiar mi contraseña?</button>
                     <div class="panel">
                          <p>Para cambiar la contraseña, haga clic en Mi cuenta en la parte superior de la página. Si usted no ve la opción de "Mi cuenta" es porque usted no ha iniciado sesión todavía. Por lo tanto haga clic en Iniciar sesión para acceder a su cuenta. Dentro de "Mi cuenta", seleccione Editar y haga clic en Cambiar contraseña. Se le pedirá que introduzca la contraseña actual y la contraseña nueva (dos veces). No olvide Guardar contaseña para guardar todos los cambios. Si usted no recuerda su contraseña, haga clic aquí y luego en ¿Ha olvidadado su contraseña? para restablecer la contraseña.  </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">¿Cómo puedo cambiar mi correo electrónico?</button>
                     <div class="panel">
                          <p>Inicie sesión en su cuenta y haga clic en Mi cuenta en la parte superior derecha de la página.  Haga clic en la sección Editar para cambiar la dirección de correo electrónico. No olvide hacer clic en Guardar para mantener los cambios realizados.</p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>


                     <hr> </hr> 
                <h6><b>Otro</b></h6>
                     <button class="accordion">He olvidado mi nombre de usuario ¿Qué debo hacer?</button>
                     <div class="panel">
                          <p>Si usted ha olvidado su nombre de usuario, puede utilizar la dirección de correo electrónico de su cuenta para iniciar sesión en JustAnswer. Si aún tiene dificultades para ingresar a su cuenta, debe contactar con nuestro servicio de Atención al cliente. Estaremos encantados de ayudarle.</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">¿Por qué no he recibido un correo electrónico con mi contraseña temporal todavía?</button>
                     <div class="panel">
                          <p>Normalmente usted debe recibir el correo con su contraseña en unos minutos. Si usted ha solicitado una nueva contraseña y no ha recibo un correo electrónico todavía, le recomendamos que compruebe su carpeta de correo no deseado. En algunas ocasiones los correos electrónicos son eviados erróneamente a esta carpeta y los usuarios no son conscientes. Si usted tiene una cuenta de Gmail, le recomendamos que compruebe las carpetas de Promociones y Prioridad. Si aun así, no localiza la contraseña, por favor póngase en contacto con nosotros y atenderemos su consulta. </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">¿Qué pasa si olvido mi contraseña?</button>
                     <div class="panel">
                          <p>Siempre puede reestablecer su contraseña. Para ello solo tiene que clicar en "¿Ha olvidado su contraseña?" después de hacer clic en "Iniciar sesión".

Usted deberá recibir un correo electrónico con la nueva contraseña en unos minutos. Si la solicitud de nueva contraseña no le ha llegado todavía, le recomendamos que compruebe la carpeta de correo no deseado. En algunas ocasiones, los correos electrónicos son enviados a la carpeta errónea y los usuarios no son conscientes de ello. Si el proveedor de su correo electrónico es Google Mail (Gmail), le recomendamos que también compruebe las carpetas de Promociones y Prioridad. 

Si no puede solicitar una nueva contraseña, contacte con atención al cliente.</p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                 

             
</div>
</div>
</div>
</div>
</div>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>



                     <footer>
                       <div class="container">
                         <div class="row">
                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                            
                             <ul class="listround">
                               <li><b>JustAnswer</b></li>
                               <li><a href="">Inicio</a></li>
                               <li><a href="">Acerca de JustAnswer</a></li>
                               
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Clientes</b></li>
                               
                               <li><a href="">Iniciar sesión</a></li>
                               <li><a href="">Registrarse</a></li>
                               <li><a href="#">Categorías</a></li>
                              </li>
                    
                               
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Expertos</b></li>
                               
                               <li><a href="">Convertirse en Experto</a></li>
                              
              
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Atención al cliente</b></li>
                               <li><a href="">Ayuda</a></li>
                               <li><a href="">Contacto</a></li>

                             </ul>
                           </div>

                         </div>
                      </div>
</footer>


<!--footer-section-->
<div class="f-section">
    <div class="links">
    <img src="images/norton_secured.png">
    <br>
    <span>© 2003-2021 Asistley LLC</span>
     <p>
      <a href="#">Aviso de privacidad</a> &nbsp; &nbsp;
      <a href="#">Contacto</a> &nbsp; &nbsp;
      <a href="#">Términos de servicio</a> &nbsp; &nbsp;
      <a href="#">Mapa del sitio</a>
     </p>
    </div>     <!--links-->
<div>       <!--f-section-->



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>