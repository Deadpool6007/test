<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
     <link rel="stylesheet" href="css/expertreg.css">
</head>
<body>
   <div class="container">
      <div class="reg-header pt-lg-2 pt-md-2 p-0 pt-1">
          <img src="images/assistly.png" class="img-fluid d-inline  logo">
           
           <p>Conviértete en un Experto
               <!-- <div class="time"> -->
                <span class="content"><img src="images/right-tick.png" class="right-tick"><span>Guardado a las 10:16</span><img src="images/ques-tick.png" ></span>
               <!-- </div> -->
           </p>
       </div>    <!--reg-header-->
    </div>     <!--contianer-->

     <div class="box-section">
          <div class="contianer">
              <form method="post" action="expertregiste">
                 @csrf
                <label for="fname" required>Categoría <sup>*</sup></label><br>
                <select class="allcolor">
                   <option>Legal</option>
                  <option>General</option>
                  <option>Impuestos</option>
                  <option>Informática, electrónica y electrodomésticos</option>
                  <option>Legal</option>
                  <option>Mecánica</option>
                  <option>Medicina</option>
                  <option>Reparaciones del hogar</option>
                  <option>Tarot y astrología</option>
                  <option>Valoración de antigüedades</option>
                  <option>Veterinaria</option>
                </select><br>
                <!-- 2nd label -->
                <label for="fname" required>Categoría principal<sup>*</sup></label><br>
                <select class="allcolor">
                  <option>Leyes de Argentina</option>
                  <option>Leyes de Bolivia</option>
                  <option>Leyes de Chile</option>
                  <option>Leyes de Colombia</option>
                  <option>Leyes de Costa Rica</option>
                  <option>Leyes de Cuba</option>
                  <option>Leyes de Ecuador</option>
                  <option>Leyes de El Salvador</option>
                  <option>Leyes de España</option>
                  <option>Leyes de España - Daños y Perjuicios</option>
                  <option>Leyes de España - Derecho Civil</option>
                  <option>Leyes de España - Derecho Empresarial</option>
                  <option>Leyes de España - Derecho inmobiliario</option>
                  <option>Leyes de España - Derecho Internacional</option>
                  <option>Leyes de España - Derecho Laboral</option>
                  <option>Leyes de España - Derecho Penal</option>
                  <option>Leyes de España - Herencias</option>
                  <option>Leyes de España - Ley de Tráfico</option>
                  <option>Leyes de Estados Unidos</option>
                  <option>Leyes de Guatemala</option>
                  <option>Leyes de Honduras</option>
                  <option>Leyes de Inmigración</option>
                  <option>Leyes de México</option>
                  <option>Leyes de Nicaragua</option>
                  <option>Leyes de Panamá</option>
                  <option>Leyes de Paraguay</option>
                  <option>Leyes de Perú</option>
                  <option>Leyes de Puerto Rico</option>
                  <option>Leyes de República Dominicana</option>
                  <option>Leyes de Uruguay</option>
                  <option>Leyes de Venezuela</option>
                  <option>Leyes Europeas</option>
                </select><br>

                <!-- 3rd label -->
                <label for="fname" required>Categoría principal (opcional)<span data-optional-label=" (opcional)"></span></label><br>
                <select class="allcolor">
                  <option>Leyes de Argentina</option>
                  <option>Leyes de Bolivia</option>
                  <option>Leyes de Chile</option>
                  <option>Leyes de Colombia</option>
                  <option>Leyes de Costa Rica</option>
                  <option>Leyes de Cuba</option>
                  <option>Leyes de Ecuador</option>
                  <option>Leyes de El Salvador</option>
                  <option>Leyes de España</option>
                  <option>Leyes de España - Daños y Perjuicios</option>
                  <option>Leyes de España - Derecho Civil</option>
                  <option>Leyes de España - Derecho Empresarial</option>
                  <option>Leyes de España - Derecho inmobiliario</option>
                  <option>Leyes de España - Derecho Internacional</option>
                  <option>Leyes de España - Derecho Laboral</option>
                  <option>Leyes de España - Derecho Penal</option>
                  <option>Leyes de España - Herencias</option>
                  <option>Leyes de España - Ley de Tráfico</option>
                  <option>Leyes de Estados Unidos</option>
                  <option>Leyes de Guatemala</option>
                  <option>Leyes de Honduras</option>
                  <option>Leyes de Inmigración</option>
                  <option>Leyes de México</option>
                  <option>Leyes de Nicaragua</option>
                  <option>Leyes de Panamá</option>
                  <option>Leyes de Paraguay</option>
                  <option>Leyes de Perú</option>
                  <option>Leyes de Puerto Rico</option>
                  <option>Leyes de República Dominicana</option>
                  <option>Leyes de Uruguay</option>
                  <option>Leyes de Venezuela</option>
                  <option>Leyes Europeas</option>
                </select><br>
                <div class="name-field">
                   <label class="input__title required" for="form-input-first-name">Nombre <sup>*</sup></label><br>
                   <input type="text" class="numbro allcolor" placeholder="Introduce tu nombre" name="email"><br>
                   <label class="input__title required" for="form-input-first-name">Apellidos <sup>*</sup></label><br>
                   <input type="text" class="numbro allcolor" placeholder="Introduce tus apellidos" name="password"><br>
                   <label class="input__title required" for="form-input-first-name">Email  <sup>*</sup></label><br>
                   <input type="email" class="numbro allcolor" placeholder="Tu email"><br>
                    
                        
                   <label>Teléfono <sup>*</sup></label>
                   <!-- <button>+<input type="tel" id="quantity" name="quantity" min="1" max="5"></button> -->
                   <div class="mob" class="arraow">
                   <input id="form-input-phone-country-code" class="country-code allcolor" value="+34" maxlength="5" autocomplete="new-phone">
                </div>        <!--name-field-->
                   <label>Fecha de nacimiento<sup>*</sup></label><br>
                   <input type="text" class="allcolor numbro" placeholder="DD/MM/YYYY"><br>
                  
                   <!-- country -->
                   <!-- 3rd label -->
                <label for="fname" required>País</label><sup>*</sup><br>
                <select class="allcolor">
                  <option>Selecciona</option>
                  <option>Leyes de España - Daños y Perjuicios</option>
                  <option>Leyes de España - Derecho Civil</option>
                  <option>Leyes de España - Derecho Empresarial</option>
                  <option>Leyes de España - Derecho inmobiliario</option>
                  <option>Leyes de España - Derecho Internacional</option>
                  <option>Leyes de Panamá</option>
                  <option>Leyes de Paraguay</option>
                  <option>Leyes de Perú</option>
                  <option>Leyes de Puerto Rico</option>
                  <option>Leyes de República Dominicana</option>
                  <option>Leyes de Uruguay</option>
                  <option>Leyes de Venezuela</option>
                  <option>Leyes Europeas</option>
                </select><br>

                <!--  -->
                  <!-- 3rd label -->
                  <label for="fname" required>Categoría principal (opcional)<span data-optional-label=" (opcional)"></span><br>
                <select class="allcolor">
                  <option>Selecciona</option>
                  <option>Leyes de España - Daños y Perjuicios</option>
                  <option>Leyes de España - Derecho Civil</option>
                  <option>Leyes de España - Derecho Empresarial</option>
                  <option>Leyes de España - Derecho inmobiliario</option>
                  <option>Leyes de España - Derecho Internacional</option>
                  <option>Leyes de Panamá</option>
                  <option>Leyes de Paraguay</option>
                  <option>Leyes de Perú</option>
                  <option>Leyes de Puerto Rico</option>
                  <option>Leyes de República Dominicana</option>
                  <option>Leyes de Uruguay</option>
                  <option>Leyes de Venezuela</option>
                  <option>Leyes Europeas</option>
                </select>
             
             
                <h4 class="margi">Educación</h4>
                <p>Por favor, introduce tu historial educativo.</p>
                
                   <label class="input__title required" for="form-input-first-name">Título  <sup>*</sup></label><br>
                   <input class="quali" type="text" class="numbro allcolor" placeholder="Nombre del título"><br>

                   <label class="input__title required" for="form-input-first-name">Año de finalización  <sup>*</sup></label><br>
                   <input class="year" placeholder="YYYY" type="text" class="numbro allcolor" maxlength tabindex inputmode="numeric"><br>
                    
                   <label class="input__title required" for="form-input-first-name">Centro educativo   <sup>*</sup></label><br>
                   <input class="quali" type="text" class="numbro allcolor" placeholder="Nombre del centro educativo"><br>

                   <h4 class="margi">Historial laboral</h4>

                   <label class="input__title required" for="form-input-first-name">Nombre de la empresa  <sup>*</sup></label><br>
                   <input class="quali" type="text" class="numbro allcolor" placeholder="Empresa"><br><br>

                   <div class="containermargin">
                          <div class="container">
                                 <div class="row">
                                  <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                  <label class="input__title required " for="form-input-first-name" name="email">Desde  <sup>*</sup></label>
                                  </div>
                                  <div class="col-lg-8 col-md-8 col-sm-8 col-12">
                                   <label class="input__title required " for="form-input-first-name" name="password">Hasta <sup>*</sup></label>
                                  </div>
                                  <div class="row">
                                  <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                     <input class="new" placeholder="MM/YYYY" type="text" class="numbro allcolor" maxlength tabindex inputmode="numeric">
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                     <input class="new" placeholder="MM/YYYY" type="text" class="numbro allcolor" maxlength tabindex inputmode="numeric">
                                  </div>
                                  <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                                     <input type="checkbox" class="allcolor"  name="checkbox" value="value"><font size="2">Actualmente trabajo aquí</font>
                                 </div>
                                </div>
                           </div> 
</div>
                           <label class="input__title required" for="form-input-first-name">Contacto en la empresa  <sup>*</sup></label><br>
                   <input class="quali" type="text" class="numbro allcolor" placeholder="Contacto"> <br>

                   <label>Teléfono de la empresa(opcional)  </label>
                   <!-- <button>+<input type="tel" id="quantity" name="quantity" min="1" max="5"></button> -->
                   <div class="tofo" class="arraow">
                   <input id="form-input-phone-country-code" class="tofo" value="+34" maxlength="5" autocomplete="new-phone">
                </div>
                <div class="image-upload" >
                    <div class="margi">
      <label for="file-input">
        <img id="previewImg" class="attachcolor" src="\images\icons8-attach-100.png">
      </label>

      <input id="file-input" type="file" onchange="previewFile(this);" style="display: none;" />
    </div><label  for="customFile" class="attachcolor">Subir C.V. (opcional)</label>
                <p>Formatos aceptados: doc, docx, rtf, odt, pdf y txt. Tamaño máximo 5MB.</p>
                </div>
</div>

<label class="input__title required" for="form-input-first-name">Perfil de LinkedIn (opcional)
  </label><br>
                   <input class="quali" type="text" class="numbro allcolor" placeholder="Dirección URL"> 

                   <h4 class="margi">Acreditaciones profesionales</h4>
                <p>Por favor, introduce todas tus acreditaciones o títulos profesionales.</p>

                <label class="input__title required" for="form-input-first-name">Tipo de acreditación o título</label><br>
                   <input class="quali" type="text" class="numbro" placeholder="Tipo de acreditación">

                   <label class="input__title required" for="form-input-first-name">Número de licencia o colegiación</label>
                   <input class="quali" type="text" class="numbro" placeholder="Número de licencia o colegiación profesional">

                   <label class="input__title required" for="form-input-first-name">Entidad que emite la licencia</label>
                   <input class="quali" type="text" class="numbro" placeholder="Entidad que emite la licencia">

                   <label>Teléfono de la entidad emisora  </label>
                   <!-- <button>+<input type="tel" id="quantity" name="quantity" min="1" max="5"></button> -->
                   <div class="tofo" class="arraow">
                   <input id="form-input-phone-country-code" class="tofo" value="+34" maxlength="5" autocomplete="new-phone" placeholder="Teléfono de la entidad emisora">
                </div><br>

                <input type="checkbox"  name="checkbox" value="value"><font size="2">I accept the Expert Agreement and Terms of Service , and confirm I am 18 years or older.</font><br>
                <button class="buttoncolor"> Send</button>
            </div>                   
                               

                </form>
          </div>     <!--container-->
     </div>   <!--box-section-->

     <!-- info section -->
     <script>
        function previewFile(input){
            var file = $("input[type=file]").get(0).files[0];

            if(file){
              var reader = new FileReader();

              reader.onload = function(){
                  $("#previewImg").attr("src", reader.result);
              }

              reader.readAsDataURL(file);
            }
        }
    </script>

     


   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
   <script>
       $("input").intlTelInput({
          utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.3/js/utils.js"
        });
   </script>
   
</body>
</html>