<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keyword" content="asist, answer, solutions">
    <meta name="description" content="we asist you on your each feet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/ExpertDashboard.css">
</head>


<body>
    <nav>
        <div class="top-menu"><a href="expertlog">cerrar sesión</a>&nbsp;|&nbsp;
            <a href="expertdash">Mi cuenta</a>&nbsp;|&nbsp;
            <a href="contactus">Contacto</a>&nbsp;|&nbsp;
        </div>
    </nav>


                  <section>
    <div class="userdashboard">
        <div class="container">
            <div class="top-section">
                <img src="images/logo.png" class="logo__link" width="203" height="70" >
                <div class="Pregunte">Pregunte sobre Derecho y obtenga respuestas</div>
            </div>
            <!--top-section-->
        </div>
        <!--container-->

        <div class="user-info">
            <div class="container">
                <h3>Mi cuenta</h3>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                        <div class="box">
                            <b>Buscar en archivos de respuestas</b>
                            <input type="text">
                            <select>
                                <option>Cualquier categoría</option>
                                <option>Cualquier categoría</option>
                                <option>Cualquier categoría</option>
                                <option>Cualquier categoría</option>
                                <option>Cualquier categoría</option>
                                <option>Cualquier categoría</option>
                                <option>Cualquier categoría</option>
                            </select>
                            <a href="#"><button class="btn">Buscar</button></a>
                        </div>
                        <!--box-->
                        <div class="box1">
                            <b>Datos de la cuenta</b>
                            <ul>

<li><a href="expertdash"><b>Perfil de la cuenta</b></a></li>


<li><a href="assignuser">ver asignar usuario</a></li>
</ul>
                        </div>
                    </div>
                    <!---col-3-->

                    <!---col-3-->
                    <!-- col-9 -->
                    <div class="col-lg-9 col-md-9 col-12" style="overflow-x: auto;">
                    <form action="expertdash" class="form-container">
                        <table class="table-data">
                            <h4>Perfil de la cuenta</h4>
                            <p><b>Datos de la cuenta</b></p>
                            <tr>
                                <td>Nombre de usuario</td>
                                <td class="t-content">{{$user->username}}</td>
                            </tr>
                            <tr>
                                <td><b>Dirección de correo electrónico</b></td>
                                <!-- <td class="t-content">Nombre o</td> -->
                                <td class="t-content"> {{$user->email}}<a href="#" onclick="openForm()" >Cambiar correo electrónico</a></td>
                               <div class="form-popup" id="myForm">
                             

                 <!-- <label for="email"><b>Introducir una nueva dirección</b></label>
                    <input type="text"  name="email" required>

            <label for="psw"><b>Password</b></label>
            <input type="text"  name="email" required>
                <button type="button" class="btn cancel" onclick="closeForm()">ajustarse</button> -->
                            
                            <tr>
                                <td><b>Contraseña</b></td>
                                <td class="t-content"><a href="#"> Editar contraseña</a></td>
                            </tr>
                            <tr>
                                <td>
                                    <p><b>Configuración de la cuenta</b></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="rad-text">Mantener iniciada la sesión</td>
                                <td class="t-content">
                                    <div class="r-btn">
                                        <span class ="btn-ro"><input type="radio" value="si"  name ="NO" />Si</span>
                                    </div>
                                    <div class="r-btn">
                                    <span class ="btn-ro">
                                        <input type="radio" value="no"  name ="NO" class="radio-btn" />No
                                    </span>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td class="rad-text">Acepto recibir comunicaciones de JustAnswer</td>
                                <td class="t-content">
                                    <div class="r-btn">
                                    <span class ="btn-ro"><input type="radio" value="si" name ="NO">Si</span>
                                    </div>
                                    <div class="r-btn">
                                       <span class="btn-ro"> <input type="radio" value="no" class="radio-btn" name ="NO">No</span>
                                    </div>

                                </td>
                            </tr>

                            <tr>
                                <td>Zona horaria</td>
                                <td class="t-content">
                                    <select>
                                        <option>Cualquier categoría</option>
                                        <option>Cualquier categoría</option>
                                        <option>Cualquier categoría</option>
                                        <option>Cualquier categoría</option>
                                        <option>Cualquier categoría</option>
                                        <option>Cualquier categoría</option>
                                        <option>Cualquier categoría</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p><b>Información de contacto</b></p>
                                </td>
                            <tr>
                            <tr>
                                <td>Nombre:</td>
                                <td class="t-content"><input type="text" class="t-input"></td>
                            </tr>
                            <tr>
                                <td>Apellido:</td>
                                <td class="t-content"><input type="text" class="t-input"></td>
                            </tr>
                            <tr>
                                <td>Dirección 1:</td>
                                <td class="t-content"><input type="text" class="t-input"></td>
                            </tr>
                            <tr>
                                <td>Ciudad:</td>
                                <td class="t-content"><input type="text" class="t-input"></td>
                            </tr>
                            <tr>
                                <td>Estado (O bien, país:)</td>
                                <td class="t-content">
                                    <select>
                                        <option></option>
                                        <option>Cualquier categoría</option>
                                        <option>Cualquier categoría</option>
                                        <option>Cualquier categoría</option>
                                        <option>Cualquier categoría</option>
                                        <option>Cualquier categoría</option>
                                        <option>Cualquier categoría</option>
                                        <option>Cualquier categoría</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Código postal:</td>
                                <td class="t-content"><input type="text" class="t-input"></td>
                            </tr>
                            <tr>
                                <td>Teléfono:</td>
                                <td class="t-content"><input type="text" class="t-input"></td>
                            </tr>
                           
                    </div>
                    </table>
                    <button class="btn1">Guardar configuración</button>



                   
                </div>
</section>
<hr style= "margin-top:30ssspx">
 <footer class="footer-part">
      <div style="text-align:center">
      <a href =" becomeexpert">Convertirse en un experto</a>&nbsp;|&nbsp;
      <a href =" #">Términos de servicio </a>&nbsp;|&nbsp;
      <a href =" #"> Privacidad</a>&nbsp;|&nbsp;
      <a href =" about">Acerca de nosotros </a>
</div>
</footer>


<script>
function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
</script>




                <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
                    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
                    crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
                    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                    crossorigin="anonymous"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
                    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                    crossorigin="anonymous"></script>




</body>

</html>