<!DOCTYPE HTML>
<html lang="en">
<head>
   <meta charset="UTF-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="keyword" content="asist, answer, solutions">
       <meta name="description" content="we asist you on your each feet">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Document</title>   
       <link rel="stylesheet" href="css/help.css">
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
 </head>        
<body>
    
     <div class="contactus">
        <div class="bgimg">
            <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
              <div class="container">
                  <a class="navbar-brand" href="#"><img src="images/asistley.png" class="img-fluid"></a>
                  <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> -->
                    <!-- <span class="navbar-toggler-icon"></span> -->
                  </button>
              <div class="shiftnav">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                   <ul class="navbar-nav ml-auto">
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mis preguntas </a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Contacto</a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mi cuenta</a>
                      </li>
                    </ul>
                </div>
              </div>         <!--container-->
              <div class="mobile-item"> 
             <a class="nav-link" href="#">Mi cuenta</a>
           </div>
             </div>
           </nav>
          
               
           <div class="shift">
             <div class="backcolr">
                <div class="resize">
                  <div class="textcolr">
                        <div style="vertical-align: inherit;"><div style="vertical-align: inherit;">
                        ¿En qué le podemos ayudar?</div></div> 
                  </div>  
                  <div class="search_form"> 
                        <input id="search-input" class="search__input" type="text" name="f" placeholder="Busque por palabras clave como: pago, factura, membresía...">
                        <img src="/images/icons8-search.gif" class="icon" >
                  </div>
                </div>
             </div>
           </div>        


       <div class="containermargin">
           <div class="container">
               <div class="row">
                  <div class="col-lg-10 col-md-10 col-sm-10 col-12">
                    <div class="nu_container"><h5><b>Su satisfacción es nuestra prioridad</b></h5>
                       <p>Estamos orgullosos de satisfacer las necesidades de nuestros clientes.<br>
                        Si por alguna razón no está satisfecho, póngase en contacto con nuestro equipo de Atención al cliente, disponible las 24 horas del día.</p>
                    </div>
                  </div>
               <div class="col-lg-2 col-md-2 col-sm-2 col-12">
                    <div>
                       <button class="efb"> Contacto</button>
                    </div>
               </div>
               </div>   
                 <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        <div class="mysidebar">
      
                           <div class="sidebar">
        
                                <ul class="sidebar__list">
                                     <li class="sidebar__link "><a href="help">Preguntas más frecuentes</a></li>
                                     <li class="sidebar__link "><a href="aboutjustans">Acerca de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="usingjustans">Uso de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="service">Servicios premium</a></li>
                                     <li class="sidebar__link "><a href="help">Prueba de membresía/Membresía</a></li>
                                     <li class="sidebar__link "><a href="paymentbilling">Pago y facturación</a></li>
                                     <li class="sidebar__link "><a href="accountsetting">Administrar mi cuenta</a></li>
                                </ul>
                           </div>
                        </div>
                      </div>
                     <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                            <div class="contactlist">
                            <h2 class="content"><b>Preguntas más frecuentes</b></h2><br>
                            <h6>Encuentre respuestas a las preguntas más frecuentes.</h6>
                     
             <hr></hr>
                   <button class="accordion">¿Cuándo recibiré una respuesta?</button>
                     <div class="panel">
                          <p>La mayoría de las preguntas reciben una respuesta dentro de los primeros 30 minutos. Sin embargo, durante las horas pico los Expertos pueden tardar hasta 24 horas en responder a su pregunta.  

                           Cuando su pregunta tenga una respuesta, usted recibirá una notificación por correo electrónico.</p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">¿Qué es la prueba de la membresía de JustAnswer?</button>
                     <div class="panel">
                          <p>Se le cobrará por publicar la pregunta. Cualquier cargo adicional se verá reflejado en la página de pago. Si ya había publicado una pregunta, puede ver todos los cargos y el historial de sus pagos en la sección de Historial de Mi Cuenta. </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">¿Qué puedo hacer si no estoy satisfecho con la respuesta que he recibido?</button>
                     <div class="panel">
                          <p>Lamentamos que la respuesta no haya sido satisfactoria. Intentaremos hacer todo lo posible para que usted reciba la respuesta que necesita.

                          Puede solicitar una respuesta de otro Experto sin coste adicional. El enlace para solicitar una respuesta de otro Experto se encuentra debajo de su pregunta. </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                  <button class="accordion">¿Qué está incluido en la membresía de JustAnswer?</button>
                     <div class="panel">
                          <p>Por un solo pago mensual, usted puede enviar preguntas a Médicos, Abogados, Mecánicos y otros profesionales.  La mayoría de los planes incluyen un período de prueba de 7 días. Ahorre tiempo y dinero cada mes obteniendo:

                             Preguntas y respuestas gratis
                             Respuestas para amigos y familiares
                             Médicos, Abogados, Mecánicos, Veterinarios, Psicólogos y 1000 más disponibles 24 horas.
                             Solución de sus problemas antes de que éstos sean más costosos </p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                 <button class="accordion">¿Cómo puedo cancelar mi prueba?</button>
                     <div class="panel">
                          <p>Lamentamos que quiera cancelar su prueba de membresía. Para cancelar la prueba, vaya a la sección Membresía de Mi cuenta y haga clic en el botón Cancelar membresía. Recuerde que siempre puede obtener una segunda opinión o una respuesta nueva de otro Experto.
   
                        </p>
                        <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
<button class="accordion">¿Cómo puedo cancelar mi membresía?</button>
<div class="panel">
  <p>Lamentamos que quiera cancelar su membresía. Para cancelar la membresía, vaya a la sección Membresía de Mi cuenta y haga clic en el botón Cancelar membresía. Recuerde que siempre puede obtener una segunda opinión o una respuesta nueva de otro Experto.  </p>
  <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
</div>
<button class="accordion">¿Cómo puedo cambiar mi contraseña?</button>
<div class="panel">
  <p>Para cambiar la contraseña, haga clic en Mi cuenta en la parte superior de la página. Si usted no ve la opción de "Mi cuenta" es porque usted no ha iniciado sesión todavía. Por lo tanto haga clic en Iniciar sesión para acceder a su cuenta. Dentro de "Mi cuenta", seleccione Editar y haga clic en Cambiar contraseña. Se le pedirá que introduzca la contraseña actual y la contraseña nueva (dos veces). No olvide Guardar contaseña para guardar todos los cambios. Si usted no recuerda su contraseña, haga clic aquí y luego en ¿Ha olvidadado su contraseña? para restablecer la contraseña.  </p>
  <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
</div>

             
</div>
</div>
</div>
</div>
</div>
                    
                    <footer>
                       <div class="container">
                         <div class="row">
                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                            
                             <ul class="listround">
                               <li><b>JustAnswer</b></li>
                               <li><a href="">Inicio</a></li>
                               <li><a href="">Acerca de JustAnswer</a></li>
                             </ul>
                           </div>
                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Clientes</b></li>
                               
                               <li><a href="#">Iniciar sesión</a></li>
                               <li><a href="#">Registrarse</a></li>
                               <li><a href="#">Categorías</a></li></li>
             
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6 ">
                             <ul class="listround">
                               <li><b>Expertos</b></li>
                               
                               <li><a href="">Convertirse en Experto</a></li>
                              
              
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Atención al cliente</b></li>
                               <li><a href="">Ayuda</a></li>
                               <li><a href="">Contacto</a></li>

                             </ul>
                           </div>

                         </div>
                      </div>
</footer>


<!--footer-section-->
<div class="f-section">
   <div class="links">
    <img src="images/norton_secured.png">
    <br>
    <span>© 2003-2021 Asistley LLC</span>

    <p>
      <a href="#">Aviso de privacidad</a> &nbsp; &nbsp;
      <a href="#">Contacto</a> &nbsp; &nbsp;
      <a href="#">Términos de servicio</a> &nbsp; &nbsp;
      <a href="#">Mapa del sitio</a>
    </p>
    </div>     <!--links-->
<div>       <!--f-section-->
 


<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>





<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>