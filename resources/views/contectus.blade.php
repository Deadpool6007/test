<!DOCTYPE HTML>
<html lang="en">
<head>
   <meta charset="UTF-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="keyword" content="asist, answer, solutions">
       <meta name="description" content="we asist you on your each feet">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Document</title>   
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel="stylesheet" href="css/help.css">
</head>        
<body>
    
     <div class="contactus">
        <div class="bgimg">
            <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
              <div class="container">
                  <a class="navbar-brand" href="#"><img src="images/asistley.png" ></a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
              <div class="shiftnav">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                   <ul class="navbar-nav ml-auto">
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mis preguntas </a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Contacto</a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mi cuenta</a>
                      </li>
                    </ul>
                </div>
              </div>         <!--container-->
           </div>
           </nav>

           <div class="shift">
             <div class="backcolr">
                <div class="resize">
                  <div class="textcolr">
                        <div style="vertical-align: inherit;"><div style="vertical-align: inherit;">¿En qué le podemos ayudar?</font></font> 
                  </div>  
                  <div class="search_form"> 
                        <input id="search-input" class="search__input" type="text" name="f" placeholder="Busque por palabras clave como: pago, factura, membresía...">
                        <img src="/images/icons8-search.gif" class="icon" >
                  </div>
                </div>
             </div>
           </div>        


       <div class="containermargin">
           <div class="container">
               <div class="row">
                  <div class="col-lg-10 col-md-10">
                    <div class="nu_container"><h5><b>Su satisfacción es nuestra prioridad</b></h5>
                       <p>Estamos orgullosos de satisfacer las necesidades de nuestros clientes.<br>
                        Si por alguna razón no está satisfecho, póngase en contacto con nuestro equipo de Atención al cliente, disponible las 24 horas del día.</p>
                    </div>
                  </div>
               <div class="col-lg-2 col-md-2 col-sm-2 col-12">
                    <div>
                      <button class="efb"> Contacto</button>
                    </div>
               </div>
               </div>   
                 <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        <div class="mysidebar">
                           <div class="sidebar">
                              <ul class="sidebar__list">
                                     <li class="sidebar__link "><a href="help">Preguntas más frecuentes</a></li>
                                     <li class="sidebar__link "><a href="aboutjustans">Acerca de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="usingjustans">Uso de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="service">Servicios premium</a></li>
                                     <li class="sidebar__link "><a href="help">Prueba de membresía/Membresía</a></li>
                                     <li class="sidebar__link "><a href="paymentbilling">Pago y facturación</a></li>
                                     <li class="sidebar__link "><a href="accountsetting">Administrar mi cuenta</a></li>
                                </ul>
                           </div>
                        </div>
                      </div>
                     <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                            <div class="contactlist">
                                <h4><b>Contacto</b></h4>
                                   <div class="main">
                                      <img src="/images/icons8-phone-50.png" class="conimgresize">
                                          <h4 class="head"><b>Teléfono</b></h4>
                                   </div>    <!--main-->
                            <div class="row"> 
                     <div class="col-lg-7 col-md-7 col-sm-7 col-12">
                           <div class="element_margin">
                                   <p>España</p>
                                   <p>Estados Unidos/Canadá</p>
                                   <p>Argentina</p>
                                   <p>Chile</p>
                                   <p>México</p>
    
                           </div>  <!--element-margin-->
                       </div>   <!--col-lg-7-->
                      <div class="col-lg-5 col-md-7 col-sm-5 col-12">
                            <p>900 998 440</p>
                            <p>800 939 6216</p>
                            <p>800 666 3381</p>
                            <p>800 914 757</p>
                            <p>800 681 6944</p>         
                     </div>        <!--col-lg-5-->
                    </div>      <!--row-->
             <hr></hr>
                <div class="main">
                    <img src="/images/icons8-secured-letter.gif" class="conimgresize">
                       <h4 class="head"><b>Correo electrónico</b></h4>
                </div>    <!--main-->
                    <div class="italictext"><p><i>Tiempo medio de respuesta: 24 horas</i></p></div>
                    <div class="email">
                           <p><b>Envíe un correo electrónico al servicio de Atención al cliente</b></p>
                           <p>Por favor, complete el formulario y nuestro servicio de Atención al cliente se pondrá en contacto con usted tan pronto como posible, normalmente dentro de 24 horas. Su correo electrónico será utilizado solo para la búsqueda de su cuenta y comunicación.</p>
                    </div>      <!--email-->
                    <!-- <div> -->
<div class="containermargin">
<div class="container">
     <div class="row">
       <div class="col-lg-4 col-md-4 col-sm-4 col-12">
  <form id="email">
  <div class="email"><p>Nombre <em class="asterik">*</em>
</p></div>         <!--email-->
<div class="email1">
  <p>Correo electrónico <em class="asterik">*</em></p>
</div>      <!--email1-->
<div class="email2">
    <p>Mensaje <em class="asterik ">*</em></p>
</div>    <!--email-2-->
</div>    <!--col-lg-4-->
<div class="col-lg-8 col-md-8 col-sm-8 col-12">
  <!-- <input  type="hidden" name="subject"> -->
  
  <div class="margi">
  <input type="text" class="txtb" name="name" required="true">
</div>   <!--margi-->
<div class="textmargi">
  <input type="email" class="txtb" name="email" required="true">
</div> <!--textmargi-->
<div class="margi2">
  <textarea required="true" class="margi2" rows="8" cols="50"></textarea>
</div>    <!--margi2-->
<button class="buttoncolor"> Enviar</button>
</div>   <!--col-lg-8-->
</div>        <!---row-->
</div>         <!--container-->
</div>
</div>
</div>
</div>
</div>



                     <footer>
                       <div class="container">
                         <div class="row">
                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                            
                             <ul class="listround">
                               <li><b>JustAnswer</b></li>
                               <li><a href="#">Inicio</a></li>
                               <li><a href="#">Acerca de JustAnswer</a></li>
                               
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Clientes</b></li>
                               
                               <li><a href="#">Iniciar sesión</a></li>
                               <li><a href="#">Registrarse</a></li>
                               <li>Categorías</li>
                    
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Expertos</b></li>
                               
                               <li><a href="#">Convertirse en Experto</a></li>
                              
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Atención al cliente</b></li>
                               <li><a href="help">Ayuda</a></li>
                               <li><a href="contactus">Contacto</a></li>
                             </ul>
                           </div>
                         </div>
                      </div>
</footer>


<!--footer-section-->
<div class="f-section">
   <div class="links">
    <img src="images/norton_secured.png">
    <br>
    <span>© 2003-2021 Asistley LLC</span>

    <p>
      <a href="#">Aviso de privacidad</a> &nbsp; &nbsp;
      <a href="#">Contacto</a> &nbsp; &nbsp;
      <a href="#">Términos de servicio</a> &nbsp; &nbsp;
      <a href="#">Mapa del sitio</a>
    </p>
    </div>     <!--links-->
<div>       <!--f-section-->



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>