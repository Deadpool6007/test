<!DOCTYPE HTML>
<html lang="en">
<head>
   <meta charset="UTF-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="keyword" content="asist, answer, solutions">
       <meta name="description" content="we asist you on your each feet">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Document</title>   
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel="stylesheet" href="css/help.css">
</head>        
<body>
    
     <div class="contactus">
        <div class="bgimg">
            <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
              <div class="container">
                  <a class="navbar-brand" href="#"><img src="images/asistley.png" ></a>
                  <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> -->
                    <!-- <span class="navbar-toggler-icon"></span> -->
                  </button>
              <div class="shiftnav">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                   <ul class="navbar-nav ml-auto">
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mis preguntas </a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Contacto</a>
                      </li>
                      <li class="nav-item">
                         <a class="nav-link" href="#">Mi cuenta</a>
                      </li>
                    </ul>
                </div>
              </div>         <!--container-->
              <div class="mobile-item"> 
             <a class="nav-link" href="#">Mi cuenta</a>
           </div>
           </div>
           </nav>

           <div class="shift">
             <div class="backcolr">
                <div class="resize">
                  <div class="textcolr">
                        <divdiv style="vertical-align: inherit;"><div style="vertical-align: inherit;">¿En qué le podemos ayudar?</div></div> 
                  </div>  
                  <div class="search_form"> 
                        <input id="search-input" class="search__input search-tab" type="text" name="f" placeholder="Busque por palabras clave como: pago, factura, membresía...">
                        <img src="/images/icons8-search.gif" class="icon" >
                  </div>
                </div>
             </div>
           </div>        


       <div class="containermargin">
           <div class="container">
               <div class="row">
                  <div class="col-lg-10 col-md-10 col-sm-10 col-12">
                    <div class="nu_container"><h5><b>Su satisfacción es nuestra prioridad</b></h5>
                       <p>Estamos orgullosos de satisfacer las necesidades de nuestros clientes.<br>
                        Si por alguna razón no está satisfecho, póngase en contacto con nuestro equipo de Atención al cliente, disponible las 24 horas del día.</p>
                    </div>
                  </div>
               <div class="col-lg-2 col-md-2 col-sm-2 col-12">
                    <div>
                       <button class="efb"> Contacto</button>
                    </div>
               </div>
               </div>   
                 <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        <div class="mysidebar">
      
                           <div class="sidebar">
        
                                <ul class="sidebar__list">
                                     <li class="sidebar__link "><a href="help">Preguntas más frecuentes</a></li>
                                     <li class="sidebar__link "><a href="aboutjustans">Acerca de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="usingjustans">Uso de JustAnswer</a></li>
                                     <li class="sidebar__link "><a href="service">Servicios premium</a></li>
                                     <li class="sidebar__link "><a href="help">Prueba de membresía/Membresía</a></li>
                                     <li class="sidebar__link "><a href="paymentbilling">Pago y facturación</a></li>
                                     <li class="sidebar__link "><a href="accountsetting">Administrar mi cuenta</a></li>
                                </ul>
                           </div>
                        </div>
                      </div>
                     <div class="col-lg-9 col-md-9 col-sm-9 col-12">
                            <div class="contactlist">
                            <h2 class="content"><b>Uso de JustAnswer</b></h2><br>
                            
                     
             <hr></hr>
                   <h6><b>Obtener respuestas</b></h6>
                   <button class="accordion">He publicado una pregunta y todavía no hay respuesta</button>
                     <div class="panel">
                          <p>Normalmente usted recibirá una respuesta dentro de 30 minutos al publicar la pregunta. Sin embargo, durante las horas pico las respuestas pueden tardar hasta 24 horas.  Puede comprobar si tiene una respuesta aquí. </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Cómo me doy de alta para recibir notificaciones sobre mi respuesta?</button>
                     <div class="panel">
                          <p>Cuando su respuesta esté lista, le enviaremos una notificación por correo electrónico. Si además desea recibir notificaciones por mensajes de texto, vaya a la sección Editar de la página de Mi cuenta, introduzca su número de teléfono y marque la casilla correspondiente para recibir las notificaciones por teléfono. Asegúrese de guardar su número de teléfono para recibir una notificación cuando su pregunta reciba la respuesta del Experto. Por favor, tenga en cuenta que el mensaje de texto no incuirá la respuesta del Experto y solo le notificará que la respuesta le está esperando.

</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Cómo puedo saber si mi pregunta ya tiene una respuesta de Experto?</button>
                     <div class="panel">
                          <p>La manera más fácil de comprobar si su pregunta tiene una respuesta es abrir la sección de Mis preguntas. Además, usted recibirá un correo electrónico y un mensaje de texto cuando un Experto responda a su pregunta. Si usted necesita actualizar su dirección de correo electrónico o su número de teléfono, visite la sección Mi cuenta. </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Cómo puedo ver la respuesta del Experto?</button>
                     <div class="panel">
                          <p>Cuando su respuesta esté lista, le enviaremos un correo electrónico y un mensaje de texto. También puede consultar la página de Mis preguntas para ver si tiene una respuesta. Si no ha iniciado sesión en su cuenta, haga clic en Iniciar sesión en la parte superior de la página para acceder a su cuenta.
</p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>




                     <hr></hr>
                   <h6><b>Hacer preguntas</b></h6>
                   <button class="accordion">¿Cuándo recibiré una respuesta?</button>
                     <div class="panel">
                          <p>La mayoría de las preguntas reciben una respuesta dentro de los primeros 30 minutos. Sin embargo, durante las horas pico los Expertos pueden tardar hasta 24 horas en responder a su pregunta.  

Cuando su pregunta tenga una respuesta, usted recibirá una notificación por correo electrónico. </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Cómo puedo publicar una pregunta?</button>
                     <div class="panel">
                          <p>Para publicar una pregunta en JustAnswer®, vaya a http://www.justanswer.es e introduzca su pregunta. Normalmente recibirá una respuesta dentro de unos minutos.  El Experto puede hacer preguntas aclaratorias o solicitar más informácion para poder ofrecerle la respuesta correcta. Usted puede realizar tantas preguntas de seguimiento como necesite utilizando el botón Responder ubicado en la parte inferior de la página de la pregunta.

</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Cómo puedo responder o editar mi pregunta?</button>
                     <div class="panel">
                          <p>Si usted desea agregar más detalles o aclarar algo, vaya al recuadro de la parte inferior de la página de la pregunta, introduzca la nueva información y haga clic en Enviar.

</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Qué debo hacer si mi pregunta se ha publicado en la categoría incorrecta?</button>
                     <div class="panel">
                          <p>Si su pregunta ha sido enviada en una categoría incorrecta, le agradeceremos que se ponga en contacto con nosotros, estaremos encantados de ubicarla en la categoría correcta.
</p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>



                     <hr></hr>
                   <h6><b>Otras preguntas</b></h6>
                   <button class="accordion">¿Cómo puedo cancelar mi pregunta?</button>
                     <div class="panel">
                          <p>Si usted ya no necesita una respuesta del Experto y le gustaría cancelar su pregunta, póngase en contacto con nuestro Departamento de atención al cliente. </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Qué puedo hacer si no estoy satisfecho con la respuesta que he recibido?</button>
                     <div class="panel">
                          <p>Lamentamos que la respuesta no haya sido satisfactoria. Intentaremos hacer todo lo posible para que usted reciba la respuesta que necesita.

                            Puede solicitar una respuesta de otro Experto sin coste adicional. El enlace para solicitar una respuesta de otro Experto se encuentra debajo de su pregunta.

</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>


                     <hr></hr>
                   <h6><b>Valorar las preguntas/expertos</b></h6>
                   <button class="accordion">¿Cómo valoro una respuesta?</button>
                     <div class="panel">
                          <p>Para valorar el servicio recibido por parte del Experto, usted solo tiene que seleccionar una de las 5 estrellas que están ubicadas en la parte superior derecha de la página. Valorar significa recompensar al Experto por su trabajo. El Experto no recibirá compensación económica hasta que usted no valore su respuesta.  Si las estrellas están en gris o no puede hacer clic en las estrellas para calificar su experiencia,  notifique a su Experto para que pueda ajustar su pregunta para la valoración.</p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Qué puedo hacer si he valorado la respuesta positivamente por error?</button>
                     <div class="panel">
                          <p>Puede continuar la conversación incluso después de valorar la respuesta.

                             Si valoró la respuesta positivamente pero está insatisfecho con la respuesta, nuestro servicio de Atención al cliente puede ayudarle a encontrar un nuevo Experto para responder a su pregunta. 

                            </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                     <button class="accordion">¿Tengo que valorar la respuesta?</button>
                     <div class="panel">
                          <p>Una vez el Experto le facilite la respuesta a su pregunta, usted tiene la oportunidad de valorarla. Si usted está satisfecho con la respuesta que ha recibido, asegúrese de darle una valoración positiva. De esta forma, el Experto será valorado por su trabajo y recibirá su beneficio.
Si usted no está satisfecho con la respuesta, agregue más información y haga clic en Enviar para que el Experto reciba su opinión al respecto de la respuesta y pueda mejorarla. Si usted prefiere que otro Experto responda su pregunta, póngase en contacto con nosotros para que le busquemos otro Experto.

</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>



                     <hr></hr>
                   <h6><b>Servicios de Expertos</b></h6>
                   <button class="accordion">Acepté una llamada telefónica del Experto. ¿Qué debo hacer ahora?</button>
                     <div class="panel">
                          <p>Ahora que ha aceptado una llamada telefónica, revise los siguientes consejos para asegurase de obtener el máximo provecho de su llamada.

1.Proporcione al Experto su número de teléfono para que pueda llamarle en el cuadro de mensaje privado.
2.El Experto le informará sobre cuándo estará disponible y concretará con usted cuál es el mejor momento para realizar la llamada. La mayoría de las veces usted recibirá la llamada en cuestión de minutos. Si usted no ha recibido la llamada puede enviar una respuesta al Experto haciéndole saber que está listo.
3.Si el Experto que le está atendiendo dice que no está disponible para llamarle en ese momento, no hay de qué preocuparse. Encontraremos a otro Experto que lo llame, por lo general en 60 minutos o menos.
Esperamos que su llamada sea exitosa y que podamos ayudarle a responder más preguntas en el futuro.</p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">Acepté una sesión de Asistencia Remota Segura ¿Qué significa y cómo puedo empezar?</button>
                     <div class="panel">
                          <p>Ahora que ha aceptado la oferta de Asistencia remota segura, revise los consejos útiles para asegurarse de obtener el máximo provecho de su sesión.

1.Busque un enlace que ha sido compartido con usted por parte del Experto y haga clic en él para empezar la sesión. Si usted tiene algún problema, responda al Experto para que éste le indique cómo continuar.
2.Si usted todavía está a la esprera de obtener el enlace, escríbale al Experto para recordarle que está esperando. Si no recibe una respuesta en el momento oportuno, puede ponerse en contacto con nuestro servicio de atención al cliente.
3.Recuerde, todas las sesiones de Asistencia Remota son seguras y privadas. Los Expertos pueden acceder a su equipo y solucionar su problema mientras usted observa. Una vez que la sesión ha terminado, el Experto ya no tiene acceso a su ordenador.
Esperamos que la sesión de Asistencia remota segura sea un éxito y que podamos ayudarle a responder las preguntas en el futuro. 

                            </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                     <button class="accordion">He recibido una oferta de Servicio Premium. ¿Qué significa y cómo funciona?</button>
                     <div class="panel">
                          <p>Algunas veces una respuesta en línea no es suficiente. Por esto, usted puede recibir ayuda adicional de los Expertos en JustAnswer. El Experto puede ayudarle a obtener una respuesta más completa con una variedad de servicios como: llamada telefónica, videollamada, preparación de documentos, entre otros.

Los servicios premium son servicios adicionales que se salen del estándar de las respuestas que hasta ahora ha tenido en JustAnswer. Los servicios premium pueden ser llevados a cabo fuera de la página de JustAnswer (sea por teléfono, chat, correo electrónico, control remoto del ordenador, o cualquier otra vía de comunicación) pero siguen siendo un servicio con fines informativos generales. Para aceptar el Servicio Premium del Experto, simplemente haga clic en el botón "Aceptar la oferta".

</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                     
                     <button class="accordion">Mi pago por Servicio Premium ha fallado. ¿Qué debo hacer?</button>
                     <div class="panel">
                          <p>Si su pago por Servicio Premium ha fallado, pero usted sigue interesado en obtener ayuda, por favor, póngase en contacto con su entidad de pago para comprobar si ha ocurrido uno de los siguientes errores:  

1.La entidad de pago ha rechazado la transacción por motivos de seguridad.
2.La cuenta no dispone de fondos suficientes. 
3.La tarjeta de crédito ha caducado. 
Una vez se haya resuelto el problema con su entidad de pago, por favor, póngase en contacto con nuestro servicio de Atención al cliente. 

</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>
                     
                     <button class="accordion">¿Por qué hay un cargo extra por el servicio premium?</button>
                     <div class="panel">
                          <p>El servicio premium es llevado a cabo fuera de la plataforma tradicional de JustAnswer. Por ello, el Experto puede ofrecerle una llamada telefónica, una videollamada o la preparación de un documento como puede ser la redacción de un contrato o un testamento. Debido a que el servicio premium es realizado fuera de la página y requiere más allá de una respuesta a su pregunta, el cargo extra se realiza para compensar al profesional por el tiempo adicional ayudándolo.



</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                     



                     <hr></hr>
                   <h6><b>Otro</b></h6>
                   <button class="accordion">El restablecimiento de contraseña que me enviaron no funciona. ¿Qué debo hacer?</button>
                     <div class="panel">
                          <p>Compruebe que utiliza la contraseña más reciente enviada a usted.  Si necesita ayuda, póngase en contacto con nuestro Departamento de atención al cliente y estaremos encantados de ayudarle. </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">He recibido un mensaje diciendo que alguien ha creado una cuenta con mi correo electrónico. ¿Qué debo hacer?</button>
                     <div class="panel">
                          <p>Es posible que usted haya creado una cuenta con nosotros anteriormente. Por favor,  póngase en contacto con nuestro servicio de Atención al cliente para que podamos encontrar la pregunta publicada con su correo electrónico.

</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Cómo puedo enviarle una foto o archivo al Experto?</button>
                     <div class="panel">
                          <p>Hay varias maneras de enviar una foto o un archivo al Experto. Usted puede añadir un archivo en el momento de publicar la pregunta. Si usted ya había publicado su pregunta y quiere añadir una foto o archivo para compartir con el Experto, haga clic en el botón "Añadir archivo" o " Añadir imagen" ubicados encima del recuadro de la respuesta. Se le abrirá una ventana de su ordenador donde podrá seleccionar el archivo o foto que desea agregar. Encuentre el archivo que desea cargar en su ordenador y haga clic en "Abrir". El nombre del archivo o foto seleccionado aparecerá en la página, justo debajo del recuadro de la respuesta. Posteriormente introduzca el texto de su respuesta y haga clic en "Enviar" para enviar el archivo al Experto.

El archivo debe tener un tamaño inferior a 5MB y un formato de: GIF, PNG, JPEG, ZIP, RAR, PDF, DOC, XLS, PPT, DOCX, XLSX, SWF, FLV, WMV.  Si su archivo es superior a 5MB, por favor intente cambiar el tamaño o envie su archivo al servicio de Atención al cliente.</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Cómo puedo revisar mis preguntas recientes?</button>
                     <div class="panel">
                          <p>Usted puede acceder a sus preguntas haciendo clic en Mis preguntas en la parte superior de la página. Si usted no ve Mis preguntas en la parte superior de la página es porque usted no ha iniciado sesión correctamente. Simplemente inicie sesión y siga las instrucciones anteriormente señaladas.


</p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                     <button class="accordion">¿Cómo puedo transferir una foto de mi teléfono a mi ordenador?</button>
                     <div class="panel">
                          <p>Siga las siguientes instrucciones que mejor se adapten a su situación:

1.Utilice correo electrónico para enviar fotos desde su teléfono a su ordenador (recomendado)
2.Conecte su teléfono directamente a su ordenador
Utilice correo electrónico para enviar fotos desde su teléfono a su ordenador (recomendado)
Desde iPhone
1. Primero, asegúrese de que su iPhone está conectado a Internet a través de wifi o su red celular. 
2. Abra la cámara y escoja la foto que desea enviar. 
3. En la esquina inferior derecha, presione el botón "Compartir" (se ve como una caja con una flecha apuntando hacia arriba), luego escoja "Correo electrónico".
4. Un nuevo mensaje de correo electrónico se abrirá con la foto adjunta. Introduzca su dirección de correo electrónico en el campo "Para", y luego presione "Enviar".
5. Usando su ordenador, abra el correo electrónico que usted mismo se ha enviado y guarde la foto en la carpeta de imágenes. 
6. Una vez que usted tenga su foto, haga clic aquí para ver cómo descargar su foto en la pregunta.

Desde Android

1. Primero, asegúrese de que su teléfono Android está conectado a Internet a través de wifi o su red celular.
2. Abra la aplicación de correo electrónico de su preferencia.
3. Haga clic en "Crear correo electrónico". 
4. Localice el botón "Adjuntar" que se ve como un clip de papelería. Seleccione "Imágenes" cuando le pregunte qué desea adjuntar.
5. Usted deberá seleccionar una aplicación para acceder a las imágenes. Seleccione "Galería" si usted no está seguro de cuál es la aplicación por defecto para abrir las imágenes.
6. Busque y seleccione la foto.
7. Introduzca su dirección de correo electrónico en el mensaje y luego presione "Enviar".
8. Revise su correo electrónico desde un ordenador para obtener su foto.
9. Una vez que usted tenga su foto, haga clic aquí para ver cómo agregar su foto en la pregunta.
Si usted necesita instrucciones para un teléfono diferente, o si tiene alguna pregunta, envíe un email a nuestro servicio de Atención al cliente.

Conecte su teléfono directamente a su ordenador
Desde iPhone
1. Conecte su iPhone al ordenador usando un cable USB.
2. Espere a que el icono del ordenador aparezca en su teléfono, de esta forma sabrá que está conectado y las fotos se pueden descargar. El sistema debería conectarse automáticamente, si no, deberá seleccionar "Importar fotos y vídeos". 
3. Espere hasta que las fotos se descarguen en su ordenador. 
4. Una vez que usted tenga su foto, haga clic aquí para ver cómo agregar su foto en la pregunta.

Desde Android
1. Conecte su teléfono Android al ordenador usando un cable USB. 
2. Una vez que su teléfono esté conectado, aparecerá una ventana automáticamente. Seleccione "Abrir dispositivo para ver archivos usando windows explorer". Asegúrese de que su teléfono esté desbloqueado para ver los archivos.
3. Luego haga clic en "Teléfono", doble clic en la carpeta "DCIM", y luego en "Cámara" para ver las imágenes.
4. Seleccione la(s) foto(s) de su preferencia, presione en el teclado "Ctrl+C" para copiar una vez estén seleccionadas.
5. Vaya a "Escritorio" y pegue las fotos presionando en el teclado "Ctrl+V".
6. Una vez que usted tenga su foto, haga clic aquí para ver cómo agregar su foto en la pregunta. 
Si usted necesita instrucciones para un teléfono diferente, o si tiene alguna pregunta, envíe un email a nuestro servicio de Atención al cliente. </p>
                           <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Es Justanswer gratis?</button>
                     <div class="panel">
                          <p>Nuestro objetivo es proporcionar ayuda rápida y asequible de Médicos, Abogados, Mecánicos y más Expertos. JustAnswer no es gratis, sin embargo, su coste es solo una fracción de la tarifa de un servicio profesional.

</p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Por qué no puedo iniciar sesión?</button>
                     <div class="panel">
                          <p>Si no puede iniciar sesión, compruebe que su nombre de usuario o contraseña son correctos. Su nombre de usuario es la dirección de correo electrónico que proporcionó al hacer su primera pregunta. Si no puede recordar su contraseña, haga clic aquí para restablecerla.

Si aún sigue teniendo problemas, puede contactar con nuestro servicio de Atención al cliente. Es posible que usted tenga que agregar JustAnswer a la lista de sitios seguros en su navegador. 

 </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Por qué no puedo responder al Experto?</button>
                     <div class="panel">
                          <p>Esto generalmente significa que no ha iniciado sesión. Para ver si ha accedido correctamente, mire en la esquina superior de la página. Si ve que la opción de Mi cuenta no está, es porque usted debe iniciar sesión. Una vez iniciada la sesión, usted podrá volver a su pregunta y responder al Experto desde esa página. Si por el contrario usted está con la sesión iniciada y aun así no puede responder al Experto, puede ser que la pregunta haya sido cerrada por inactividad. Si su pregunta se cierra porque no ha habido ninguna respuesta, por favor envíenos un correo electrónico para atender su solicitud.
</p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                     <button class="accordion">¿Puedo eliminar mi pregunta o eliminar información personal de mi pregunta?</button>
                     <div class="panel">
                          <p>JustAnswer no puede borrar la pregunta de la página web, pero podemos retirar toda la información personal que usted ha enviado en la misma. Si hay información personal en su pregunta que a usted le gustaría retirar, por favor contacte con nosotros. Al enviar el correo electrónico, por favor incluya el enlace de la pregunta y la información que desea eliminar.

JustAnswer es un foro público y las preguntas y respuestas no son confidenciales. Los usuarios de la página permanecen anónimos siempre y cuando no ingresen ninguna información que pueda identificarles en el texto de la pregunta. </p>
                          <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>

                  <button class="accordion">¿Qué son las preguntas de seguimiento?</button>
                     <div class="panel">
                          <p>Las preguntas aclaratorias que desee formular al Experto son gratuitas siempre y cuando éstas sean realizadas en la pregunta original.

Para hacer una pregunta de seguimiento, vaya a la sección de Mis preguntas, abra la pregunta que necesita y haga clic en "Responder al Experto". No abra una nueva pregunta para hacer preguntas relacionadas con la pregunta original, ya que dará lugar a cargos adicionales en su cuenta en concepto de nueva pregunta.
</p>
                             <div class="ques"><p>¿Le resultó útil este artículo?</p><button class="accorbutton">Si</button><button class="accorbutton">No</button></div>
                     </div>


                 

             
</div>
</div>
</div>
</div>
</div>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>



                     <footer>
                       <div class="container">
                         <div class="row">
                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                            
                             <ul class="listround">
                               <li><b>JustAnswer</b></li>
                               <li><a href="">Inicio</a></li>
                               <li><a href="">Acerca de JustAnswer</a></li>
                               
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Clientes</b></li>
                               
                               <li><a href="">Iniciar sesión</a></li>
                               <li><a href="">Registrarse</a></li>
                               <li>Categorías</li>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Expertos</b></li>
                               
                               <li><a href="">Convertirse en Experto</a></li>
                              
              
                             </ul>
                           </div>

                           <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                             <ul class="listround">
                               <li><b>Atención al cliente</b></li>
                               <li><a href="">Ayuda</a></li>
                               <li><a href="">Contacto</a></li>

                             </ul>
                           </div>

                         </div>
                      </div>
      </footer>


<!--footer-section-->
<div class="f-section">
   <div class="links">
    <img src="images/norton_secured.png">
    <br>
    <span>© 2003-2021 Asistley LLC</span>

    <p>
      <a href="#">Aviso de privacidad</a> &nbsp; &nbsp;
      <a href="#">Contacto</a> &nbsp; &nbsp;
      <a href="#">Términos de servicio</a> &nbsp; &nbsp;
      <a href="#">Mapa del sitio</a>
    </p>
    </div>     <!--links-->
<div>       <!--f-section-->



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>