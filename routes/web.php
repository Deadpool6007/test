<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\PayController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ExpertController;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('user.home1');
});
Route::get('login',[UserController::class,'login']);
Route::post('register',[UserController::class,'register']);
Route::get('resetpassword',[UserController::class,'resetpassword']);
Route::post('resetpass',[UserController::class,'resetpass']);
Route::post('userupdate',[UserController::class,'update']);
Route::get('userdashboard',[UserController::class,'userdashboard']);
Route::get('askque',[UserController::class,'askquestion']);
Route::get('chathistory',[UserController::class,'chathistory']);
Route::post('postquestion',[UserController::class,'postquestion']);
Route::get('chatExpert/{id}',[UserController::class,'chartExpert']);

Route::post('chatExpert/postques/{id}',[UserController::class,'postque']);
Route::get('userlogout',[UserController::class,'logout'] );
//Route::view('expertreg','demo');
//postques
//payment
Route::get('payment',[PaymentController::class,'index'] );
Route::post('charge', [PaymentController::class,'charge']);
Route::get('paymentsuccess', [PaymentController::class,'payment_success']);
Route::get('paymenterror',  [PaymentController::class,'payment_error']);

Route::view('demo','demo');
Route::get('demo1', [UserController::class,'register']);

Route::get('pay',[PayController::class,'index'] );
Route::post('pay2', [PayController::class,'charge']);
Route::get('paymentsucc',[PayController::class,'payment_success']);
Route::get('paymenterr',[PayController::class,'payment_error']);

//admin

//adminlogin
Route::get('adminlogin',[AdminController::class,'login'] );
Route::post('adminin',[AdminController::class,'adminlogin'] );
Route::get('Addcategory',[AdminController::class,'Addcategory'] );
//Route::view('Addprofile','admin.Addprofile');
Route::get('admindash',[AdminController::class,'dashboard'] );
Route::get('assignexpert',[AdminController::class,'assignexpert'] );
Route::post('addcat',[AdminController::class,'addcat'] );

// Route::view('assignexpert','admin.Assignexperet');
Route::get('expertview',[AdminController::class,'expertview'] );
Route::get('viewprofile/{id}',[AdminController::class,'viewprofile'] );
Route::post('assign',[AdminController::class,'assign']);
// Route::view('expertview','admin.Expertview');
Route::view('paymentreport','admin.Paymentreport');
// Route::view('viewuser','admin.Viewuser');
Route::get('viewuser',[AdminController::class,'viewuser'] );
Route::get('paymentreport',[AdminController::class,'paymentreport'] );
Route::get('userdelete/{id}',[AdminController::class,'userdelete'] );
Route::get('deleteexpert/{id}',[AdminController::class,'deleteexpert'] );
Route::get('Addcategory',[AdminController::class,'Addcategory'] );
Route::get('Adminlogout',[AdminController::class,'logout'] );
//deleteexpert

//expert url
Route::get('expertlogin',[ExpertController::class,'expertlogin']);
Route::post('expertin',[ExpertController::class,'login']);
Route::post('expertregiste',[ExpertController::class,'register']);
Route::get('expertdash',[ExpertController::class,'expertdash']);
Route::post('expertupd',[ExpertController::class,'expertupdate']);

Route::get('expertlog',[ExpertController::class,'logout']);
Route::get('expertreg',[ExpertController::class,'expertreg']);
Route::get('chartexp',[ExpertController::class,'chartexpert']);
Route::get('assignuser',[ExpertController::class,'assignuser']);
Route::get('chartexp/{id}',[ExpertController::class,'chathistory']);
Route::post('chartexp/postans/{id}',[ExpertController::class,'postans']);
//Route::get('expertdash',[ExpertController::class,'expertdash']);



// Route::view('expertdash','expert.ExpertDashboard');
//Route::view('expertchat','expert.chartExpert');
// 
//Route::view('usertable','expert.Usertable');
//only static pages

Route::view('contactus','contectus');
Route::view('help','help');
Route::view('aboutjustans','aboutjustans');
Route::view('service','service');
Route::view('accountsetting','accountsetting');
Route::view('paymentbilling','paymentbilling');
Route::view('usingjustans','usingjustans');
Route::view('new','new');